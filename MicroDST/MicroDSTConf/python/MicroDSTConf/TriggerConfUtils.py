###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__all__ = ('configureHltReportDecoding', 'configureHltDecoding')

from Configurables import DataOnDemandSvc, ApplicationMgr


def fixTrunk(trunk):
    """
    Make sure trunk ends with '/'.
    Usage:
    >>> name = '/Event/Hello/World'
    >>> fixTrunk(name)
    '/Event/Hello/World/'
    >>> name = '/Event/Hello/World/'
    >>> fixTrunk(name)
    '/Event/Hello/World/'
    """

    trunk += '/'
    return trunk.replace('//', '/')


def trunkName(trunk):
    return trunk.replace('/', '')


def configureHltReportDecoding(trunk):
    """
    Create HltDecReports and HltSelReports from DAQ/RawEvent banks.
    Fetches DAQ/RawEvent from trunk+DAQ/RawEvent, places reports in trunk + Hlt/DecReports and trunk + Hlt/SelReports respectively. Action is on-demand via the DataOnDemandSvs.
    """

    locationRoot = fixTrunk(trunk)

    name = trunkName(trunk)

    rawEventLoc = locationRoot + "DAQ/RawEvent"
    #decReportLoc = locationRoot + "Hlt/DecReports"
    #selReportLoc = locationRoot + "Hlt/SelReports"

    ApplicationMgr().ExtSvc += [DataOnDemandSvc()]
    from DAQSys.Decoders import DecoderDB
    from DAQSys.DecoderClass import decodersForBank
    for bank in ["Sel", "Dec", "Vertex", "Track"]:
        for d in decodersForBank(DecoderDB, "Hlt" + bank + "Reports"):
            d.overrideInputs(rawEventLoc)
            for key, location in d.Outputs.items():
                # This is needed only for the VertexReports decoder
                if isinstance(location, list):
                    d.overrideOutputs({
                        key: [locationRoot + loc for loc in location]
                    })
                else:
                    d.overrideOutputs({key: locationRoot + location})

    #DataOnDemandSvc().AlgMap[selReportLoc] = selReportsDecoder
    #DataOnDemandSvc().AlgMap[decReportLoc] = decReportsDecoder


def configureHltDecoding(trunk):

    configureHltReportDecoding(trunk)
