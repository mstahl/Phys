###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import CopyParticle2MCRelations
from Configurables import CopyRecHeader
from Configurables import CopyProtoParticles
from Configurables import CopyPrimaryVertices
from Configurables import CopyParticles
from Configurables import CopyParticle2PVRelations
from Configurables import CopyODIN
from Configurables import CopyMCParticles
from Configurables import CopyMCHeader
from Configurables import CopyHltSummary
from Configurables import CopyHltDecReports
from Configurables import CopyFlavourTag
