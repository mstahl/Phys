/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYPARTICLES_H
#define COPYPARTICLES_H 1

// Include files
// from MicroDST
#include "MicroDST/BindType2ClonerDef.h"
#include "MicroDST/KeyedContainerClonerAlg.h"
#include <MicroDST/ICloneParticle.h>
// from LHCb
#include <Event/Particle.h>
/** @class CopyParticles CopyParticles.h
 *
 * MicroDSTAlgorithm to clone LHCb::Particles and related objects
 * from one TES location to a parallel one.
 * It inherits the std::string properties InputLocation and OutputPrefix from
 * MicroDSTCommon. The LHCb::Particles are taken from the TES location
 * defined by InputLocation, and are cloned and put in TES location
 * "/Event" + OutputPrefix + InputLocation. If InputLocation already contains
 * a leading "/Event" it is removed.
 * The actual cloning of individual LHCb::Particles is performed by the
 * ICloneParticle, the implementation of which is set by the property
 * ClonerType (default ParticleCloner)
 * @see ICloneParticle
 * @see ParticleCloner
 *
 * <b>Example</b>: Clone particles from "/Event/Phys/DC06selBd2Jpsi2MuMu_Kst2KPi/Particles" to
 * "/Event/MyLocation/Phys/DC06selBd2Jpsi2MuMu_Kst2KPi/Particles" using a ParticleCloner
 *  @code
 *  MySelection = GaudiSequencer("SomeSelectionSequence")
 *  copyParticles = CopyParticles()
 *  copyParticles.OutputPrefix = "MyLocation"
 *  copyParticles.InputLocation = "Phys/DC06selBd2Jpsi2MuMu_Kst2KPi/Particles"
 *  #copyParticles.addTool(ParticleCloner, name='ClonerType')
 *  #copyParticles.ParticleCloner.addTool(VertexCloner, name='IClonerVertex')
 *  #copyParticles.ParticleCloner.addTool(ProtoParticleCloner, name='ICloneProtoParticle')
 *  MySelection.Members += [copyParticles]
 *  @endcode
 *
 *  @author Juan PALACIOS juan.palacios@nikhef.nl
 *  @date   2007-10-16
 */
//=============================================================================
template <>
struct BindType2Cloner<LHCb::Particle> {
  typedef LHCb::Particle Type;
  typedef ICloneParticle Cloner;
};
//=============================================================================
template <>
struct Defaults<LHCb::Particle> {
  const static std::string clonerType;
};
const std::string Defaults<LHCb::Particle>::clonerType = "ParticleCloner";
//=============================================================================
template <>
struct Location<LHCb::Particle> {
  const static std::string Default;
};
const std::string Location<LHCb::Particle>::Default = LHCb::ParticleLocation::Production;
//=============================================================================
typedef MicroDST::KeyedContainerClonerAlg<LHCb::Particle> CopyParticles;
DECLARE_COMPONENT_WITH_ID( CopyParticles, "CopyParticles" )
#endif // COPYPRIMARYVERTICES_H
