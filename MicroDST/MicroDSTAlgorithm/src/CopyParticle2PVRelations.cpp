/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CopyParticle2PVRelations.h"

DECLARE_COMPONENT_WITH_ID( CopyParticle2PVRelations, "CopyParticle2PVRelations" )

const std::string Defaults<Particle2Vertex::Table>::clonerType = "VertexBaseFromRecVertexCloner";
const std::string Location<Particle2Vertex::Table>::Default    = "NO DEFAULT LOCATION";
