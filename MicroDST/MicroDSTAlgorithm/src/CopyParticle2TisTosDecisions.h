/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYPARTICLE2TISTOSDECISIONS_H
#define COPYPARTICLE2TISTOSDECISIONS_H 1

// Include files
// from Gaudi
#include "MicroDST/MicroDSTAlgorithm.h"

struct ITriggerTisTos;

namespace LHCb {
  class HltDecReports;
}

/** @class CopyParticle2TisTosDecisions CopyParticle2TisTosDecisions.h
 *
 *
 *  @author Juan Palacios
 *  @date   2010-09-28
 */
class CopyParticle2TisTosDecisions : public MicroDSTAlgorithm {

public:
  /// Standard constructor
  CopyParticle2TisTosDecisions( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  void executeLocation( const std::string& particleLocation );

private:
  ITriggerTisTos*      m_iTisTos    = nullptr;
  LHCb::HltDecReports* m_decReports = nullptr;
  std::string          m_decReportsLocation;
};
#endif // COPYPARTICLE2TISTOSDECISIONS_H
