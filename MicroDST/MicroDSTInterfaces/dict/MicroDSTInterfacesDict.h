/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_MICRODSTINTERFACESDICT_H
#define DICT_MICRODSTINTERFACESDICT_H 1

/** @file MicroDSTInterfacesDict.h MicroDSTInterfacesDict.h dict/MicroDSTInterfacesDict.h
 *
 *
 *  @author Juan Palacios
 *  @date   2007-12-10
 */

#include "MicroDST/ICloneMCParticle.h"
#include "MicroDST/ICloneMCVertex.h"
#include "MicroDST/ICloneParticle.h"
#include "MicroDST/ICloneProtoParticle.h"
#include "MicroDST/ICloneRecVertex.h"
#include "MicroDST/ICloneTrack.h"
#include "MicroDST/ICloneVertex.h"

#endif // DICT_MICRODSTINTERFACESDICT_H
