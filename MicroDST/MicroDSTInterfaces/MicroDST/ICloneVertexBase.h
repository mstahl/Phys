/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_ICLONEVERTEXBASE_H
#define MICRODST_ICLONEVERTEXBASE_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class VertexBase;
}

/** @class ICloneVertexBase MicroDST/ICloneVertexBase.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2009-07-29
 */
class GAUDI_API ICloneVertexBase : virtual public MicroDST::ICloner<LHCb::VertexBase> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneVertexBase, 2, 0 );

  /// Destructor
  virtual ~ICloneVertexBase() {}
};

#endif // MICRODST_ICLONEVERTEXBASE_H
