/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_ICLONEMCPARTICLE_H
#define MICRODST_ICLONEMCPARTICLE_H 1
// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations

namespace LHCb {
  class MCParticle;
}

/** @class ICloneMCParticle MicroDST/ICloneMCParticle.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class GAUDI_API ICloneMCParticle : virtual public MicroDST::ICloner<LHCb::MCParticle> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneMCParticle, 2, 0 );

  /// Destructor
  virtual ~ICloneMCParticle() {}
};

#endif // MICRODST_ICLONEMCPARTICLE_H
