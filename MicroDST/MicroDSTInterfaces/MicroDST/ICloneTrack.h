/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_ICLONETRACK_H
#define MICRODST_ICLONETRACK_H 1
// from MicroDST
#include "Event/Track.h"
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {}

/** @class ICloneTrack MicroDST/ICloneTrack.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class GAUDI_API ICloneTrack : virtual public MicroDST::ICloner<LHCb::Track> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneTrack, 2, 0 );

  /// Destructor
  virtual ~ICloneTrack() {}
};

#endif // MICRODST_ICLONETRACK_H
