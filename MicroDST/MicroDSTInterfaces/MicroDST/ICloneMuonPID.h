/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_ICLONEMUONPID_H
#define MICRODST_ICLONEMUONPID_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class MuonPID;
}

/** @class ICloneMuonPID MicroDST/ICloneMuonPID.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-12
 */
class GAUDI_API ICloneMuonPID : virtual public MicroDST::ICloner<LHCb::MuonPID> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneMuonPID, 1, 0 );

  /// Destructor
  virtual ~ICloneMuonPID() {}
};

#endif // MICRODST_ICLONEMUONPID_H
