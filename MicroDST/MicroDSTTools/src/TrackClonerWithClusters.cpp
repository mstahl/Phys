/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackClonerWithClusters.h"

TrackClonerWithClusters::TrackClonerWithClusters( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : base_class( type, name, parent ), m_mcPcloner( NULL ) {
  declareProperty( "ICloneMCParticle", m_mcpClonerName = "MCParticleCloner" );
  declareProperty( "CloneAncestors", m_cloneAncestors = true );
  declareProperty( "CloneMCLinks", m_cloneMCLinks = false );
  declareProperty( "VPClusters", m_vpClusLoc = LHCb::VPClusterLocation::Default );
  declareProperty( "UTClusters", m_utClusLoc = LHCb::UTClusterLocation::UTClusters );
  declareProperty( "FTClusters", m_ftClusLoc = LHCb::FTClusterLocation::Default );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================

StatusCode TrackClonerWithClusters::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  // MC stuff
  if ( m_cloneMCLinks ) { debug() << "Will clone MC Links" << endmsg; }

  return sc;
}

//=============================================================================

// Method that handles various Gaudi "software events"
void TrackClonerWithClusters::handle( const Incident& /* incident */ ) {
  // Only one Incident type, so skip type check
  clonedTrackList().clear();
}

//=============================================================================

LHCb::Track* TrackClonerWithClusters::operator()( const LHCb::Track* track ) { return this->clone( track ); }

//=============================================================================

LHCb::Track* TrackClonerWithClusters::clone( const LHCb::Track* track ) {
  if ( !track ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Track pointer is NULL !" << endmsg;
    return NULL;
  }

  if ( !track->parent() ) {
    this->Warning( "Cannot clone a Track with no parent!" ).ignore();
    return NULL;
  }

  // Is this location in the veto list ?
  if ( isVetoed( track ) ) { return const_cast<LHCb::Track*>( track ); }

  LHCb::Track* cloneTrack = cloneKeyedContainerItem<BasicTrackCloner>( track );

  // Did the cloning work ?
  if ( cloneTrack ) {

    // If so clone ancestors if required
    cloneTrack->clearAncestors();
    if ( m_cloneAncestors ) {
      for ( const SmartRef<LHCb::Track>& Tk : track->ancestors() ) {
        const LHCb::Track* cloneAnTk = this->clone( Tk );
        if ( cloneAnTk ) { cloneTrack->addToAncestors( cloneAnTk ); }
      }
    }

    cloneClusters( cloneTrack );

    // Clone MC links ?
    if ( m_cloneMCLinks ) { cloneMCLinks( track, cloneTrack ); }
  }

  return cloneTrack;
}

void TrackClonerWithClusters::cloneClusters( const LHCb::Track* track ) {
  const auto allIDs = track->lhcbIDs();
  const auto vClus  = getIfExists<LHCb::VPClusters>( m_vpClusLoc );
  const auto ttClus = getIfExists<LHCb::UTClusters>( m_utClusLoc );
  const auto itClus = getIfExists<LHCb::FTClusters>( m_ftClusLoc );

  if ( !vClus ) { Warning( "Failed to load '" + m_vpClusLoc + "'" ).ignore(); }
  if ( !ttClus ) { Warning( "Failed to load '" + m_utClusLoc + "'" ).ignore(); }
  if ( !itClus ) { Warning( "Failed to load '" + m_ftClusLoc + "'" ).ignore(); }

  for ( const auto& id : allIDs ) {
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Cloning " << id << endmsg; }

    // Clone by type
    if ( id.isVP() && vClus ) {
      const auto* cl = vClus->object( id.vpID() );
      if ( cl ) {
        const LHCb::VPCluster* cloneCl = cloneKeyedContainerItem<BasicVPClusterCloner>( cl );
        if ( !cloneCl ) {
          Warning( "Failed to clone a VP cluster. Activate debug for details" ).ignore();
          if ( msgLevel( MSG::DEBUG ) ) debug() << "VP cluster not cloned : " << id << endmsg;
        }
      } else {
        Warning( "Failed to locate a VP cluster. Activate debug for details" ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown VP cluster : " << id << endmsg;
      }
    } else if ( id.isUT() && ttClus ) {
      const auto* cl = ttClus->object( id.utID() );
      if ( cl ) {
        const LHCb::UTCluster* cloneCl = cloneKeyedContainerItem<BasicUTClusterCloner>( cl );
        if ( !cloneCl ) {
          Warning( "Failed to clone a UT cluster. Activate debug for details" ).ignore();
          if ( msgLevel( MSG::DEBUG ) ) debug() << "UT cluster not cloned : " << id << endmsg;
        }
      } else {
        Warning( "Failed to locate a UT cluster. Activate debug for details" ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown UT cluster : " << id << endmsg;
      }
    } else if ( id.isFT() && itClus ) {
      /* PK-R3C FTCluster doesn't have a clone() method
      const auto* cl = itClus->object( id.ftID() );
      if ( cl ) {
        const LHCb::FTCluster* cloneCl = cloneKeyedContainerItem<BasicFTClusterCloner>( cl );
        if ( !cloneCl ) {
          Warning( "Failed to clone a FT cluster. Activate debug for details" ).ignore();
          if ( msgLevel( MSG::DEBUG ) ) debug() << "FT cluster not cloned : " << id << endmsg;
        }
      } else {
        Warning( "Failed to locate a FT cluster. Activate debug for details" ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown FT cluster : " << id << endmsg;
      }
      */
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Ignoring FT cluster" << endmsg; }
    } else if ( id.isMuon() ) {
      // We can't handle MUON clusters
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Ignoring MUON cluster" << endmsg; }
      continue;
    } else {
      Warning( "Unknown LHCbID. Activate debug for details" ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown LHCbID type : " << id << endmsg;
    }
  }
}

//=============================================================================

void TrackClonerWithClusters::cloneMCLinks( const LHCb::Track* track, const LHCb::Track* cloneTrack ) {

  // Linker typedefs
  typedef LinkerWithKey<LHCb::MCParticle, LHCb::Track> Linker;
  typedef LinkerTool<LHCb::Track, LHCb::MCParticle>    Asct;
  typedef Asct::DirectType                             Table;

  // has this clone already been done
  if ( std::find( clonedTrackList().begin(), clonedTrackList().end(), cloneTrack ) == clonedTrackList().end() ) {

    // location in TES of original tracks
    const std::string tkLoc = objectLocation( track->parent() );

    // try and load the linker tool
    Asct         linker = Asct( evtSvc(), tkLoc );
    const Table* table  = linker.direct();

    // If we found a table, try and use it
    if ( table ) {
      // location in TES of cloned tracks
      const std::string cloneLoc = objectLocation( cloneTrack->parent() );

      // Create a new linker for the cloned tracks
      Linker clonedLinks( evtSvc(), msgSvc(), cloneLoc );

      // Loop over relations for original track
      for ( const auto& Rentry : table->relations( track ) ) {
        // get cloned MCParticle
        const LHCb::MCParticle* clonedMCP = mcPCloner()( Rentry.to() );
        if ( clonedMCP ) {
          // if cloning worked, fill relation in linker with original weight
          clonedLinks.link( cloneTrack, clonedMCP, Rentry.weight() );
        }
      }
    }

    // save in the list
    clonedTrackList().push_back( cloneTrack );
  }
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( TrackClonerWithClusters )

//=============================================================================
