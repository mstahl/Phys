/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_TRACKCLONERWITHCLUSTERS_H
#define MICRODST_TRACKCLONERWITHCLUSTERS_H 1

// base class
#include "ObjectClonerBase.h"

// From MicroDST
#include "MicroDST/ICloneMCParticle.h"
#include "MicroDST/ICloneTrack.h"

// linker stuff
#include "Linker/LinkerTool.h"
#include "Linker/LinkerWithKey.h"

// from Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// from LHCb
#include "Event/FTCluster.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "Event/VPCluster.h"

/** @class TrackClonerWithClusters TrackClonerWithClusters.h src/TrackClonerWithClusters.h
 *
 *  Clone an LHCb::Track and its associated LHCb::VPCluster and LHCb::STCluster objects.
 */
class TrackClonerWithClusters : public extends<ObjectClonerBase, ICloneTrack, IIncidentListener> {

public:
  /// Standard constructor
  TrackClonerWithClusters( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

public:
  LHCb::Track* operator()( const LHCb::Track* track ) override;

private:
  typedef MicroDST::BasicCopy<LHCb::Track>                     BasicTrackCloner;
  typedef MicroDST::BasicItemCloner<LHCb::VPCluster>           BasicVPClusterCloner;
  typedef MicroDST::BasicItemCloner<LHCb::UTCluster>           BasicUTClusterCloner;
  typedef MicroDST::BasicItemCloner<LHCb::FTCluster>           BasicFTClusterCloner;
  typedef std::vector<const LHCb::Track*>                      TrackList;
  typedef KeyedContainer<LHCb::VPCluster, Containers::HashMap> VPClusters;
  typedef KeyedContainer<LHCb::FTCluster, Containers::HashMap> FTClusters;

private:
  /// Clone a track
  LHCb::Track* clone( const LHCb::Track* track );

  /// Clone MC Links for the given track and its clone
  void cloneMCLinks( const LHCb::Track* track, const LHCb::Track* cloneTrack );

  /// Clone track clusters for the given track clone
  void cloneClusters( const LHCb::Track* track );

  /// Static list of cloned tracks
  TrackList& clonedTrackList() {
    static TrackList list;
    return list;
  }

  /// Access on demand the MCParticle cloner
  ICloneMCParticle& mcPCloner() {
    if ( !m_mcPcloner ) { m_mcPcloner = tool<ICloneMCParticle>( m_mcpClonerName, this->parent() ); }
    return *m_mcPcloner;
  }

private:
  /// Type of MCParticle cloner
  std::string m_mcpClonerName;
  /// MCParticle Cloner
  ICloneMCParticle* m_mcPcloner;

  bool m_cloneAncestors; ///< Flag to turn on cloning of ancestors
  bool m_cloneMCLinks;   ///< Flag to turn on cloning of links to MCParticles

  std::string m_vpClusLoc; ///< VP clusters location
  std::string m_utClusLoc; ///< UT clusters location
  std::string m_ftClusLoc; ///< FT clusters location
};

#endif // MICRODST_TRACKCLONERWITHCLUSTERS_H
