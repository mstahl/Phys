/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ProtoParticleCloner.h"
#include "Relations/RelationWeighted1D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ProtoParticleCloner
//
// 2008-04-01 : Juan PALACIOS
//-----------------------------------------------------------------------------

namespace {
  using PP2MCPTable = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;
} // namespace

//=============================================================================

ProtoParticleCloner::ProtoParticleCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareProperty( "ICloneTrack", m_trackClonerName = "TrackCloner" );
  declareProperty( "ICloneCaloHypo", m_caloHypoClonerName = "CaloHypoCloner" );
  declareProperty( "PP2MCPRelations", m_pp2mcp_relations = {} );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================

StatusCode ProtoParticleCloner::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_trackCloner = tool<ICloneTrack>( m_trackClonerName, this->parent() );
  if ( !m_caloHypoClonerName.empty() && m_caloHypoClonerName != "NONE" ) {
    m_caloHypoCloner = tool<ICloneCaloHypo>( m_caloHypoClonerName, this->parent() );
  }

  return sc;
}

//=============================================================================

LHCb::ProtoParticle* ProtoParticleCloner::operator()( const LHCb::ProtoParticle* protoParticle ) {
  return this->clone( protoParticle );
}

//=============================================================================

void ProtoParticleCloner::cloneRelations( const LHCb::ProtoParticle* pp_original,
                                          const LHCb::ProtoParticle* pp_clone ) {
  for ( const auto& relLoc : m_pp2mcp_relations ) {
    const auto* rels_original = this->template get<PP2MCPTable>( relLoc );

    // Retrieve the cloned relations table if it exists, else create it
    const auto cloneLocation = outputTESLocation( MicroDST::objectLocation( rels_original ) );
    auto*      rels_clone    = this->template getIfExists<PP2MCPTable>( cloneLocation );
    if ( rels_clone == nullptr ) {
      rels_clone = new PP2MCPTable;
      this->put( rels_clone, cloneLocation );
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Created clone PP2MCP linker table at " << cloneLocation << endmsg; }
    }

    const PP2MCPTable::Range rels = rels_original->relations( pp_original );
    for ( const auto& rel : rels ) {
      // Returns StatusCode::FAILURE if the relation already exists, which is fine
      rels_clone->relate( pp_clone, rel.to(), rel.weight() ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Adding clone PP2MCP relation from PP " << pp_clone->index() << " to MCP " << rel.to()->index()
                << endmsg;
      }
    }
  }
}

//=============================================================================

LHCb::ProtoParticle* ProtoParticleCloner::clone( const LHCb::ProtoParticle* protoParticle,
                                                 const LHCb::Particle*      parent ) {
  if ( !protoParticle ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "ProtoParticle pointer is NULL !" << endmsg;
    return nullptr;
  }

  if ( !protoParticle->parent() ) {
    this->Warning( "Cannot clone a ProtoParticle with no parent!" ).ignore();
    return nullptr;
  }

  // Is this object in the veto list ?
  const bool vetoed = isVetoed( protoParticle );

  // Is this object in the always clone list ?
  const bool alClone = alwaysClone( protoParticle );

  // Return now if vetoed and no cloning
  if ( vetoed && !alClone ) { return const_cast<LHCb::ProtoParticle*>( protoParticle ); }

  LHCb::ProtoParticle* protoParticleClone = cloneKeyedContainerItem<BasicProtoParticleCloner>( protoParticle );

  if ( protoParticleClone ) {

    // Track
    LHCb::Track* clonedTrack = nullptr;
    if ( m_trackCloner ) { clonedTrack = ( *m_trackCloner )( protoParticle->track() ); }
    protoParticleClone->setTrack( clonedTrack );

    // Rich PID
    if ( !isVetoed( protoParticle->richPID() ) ) {
      LHCb::RichPID* clonedRichPID = cloneKeyedContainerItem<RichPIDCloner>( protoParticle->richPID() );
      if ( clonedRichPID ) {
        // set the main track
        clonedRichPID->setTrack( clonedTrack );
      }
      protoParticleClone->setRichPID( clonedRichPID );
    }

    // MUON PID
    if ( !isVetoed( protoParticle->muonPID() ) ) {
      LHCb::MuonPID* clonedMuonPID = cloneKeyedContainerItem<MuonPIDCloner>( protoParticle->muonPID() );
      if ( clonedMuonPID ) {
        // Set the main track
        clonedMuonPID->setIDTrack( clonedTrack );
        // Clone and set the Muon Track
        LHCb::Track* clonedMuonTrack = nullptr;
        if ( m_trackCloner ) { clonedMuonTrack = ( *m_trackCloner )( protoParticle->muonPID()->muonTrack() ); }
        clonedMuonPID->setMuonTrack( clonedMuonTrack );
      }
      protoParticleClone->setMuonPID( clonedMuonPID );
    }

    // CALO
    protoParticleClone->clearCalo();
    const auto& caloHypos = protoParticle->calo();
    if ( !caloHypos.empty() ) {
      for ( auto hypo : caloHypos ) {
        if ( hypo ) {
          if ( !isVetoed( hypo ) ) {
            if ( m_caloHypoCloner ) {
              // Use full cloning tool
              auto hypoClone = m_caloHypoCloner->clone( hypo, parent );
              // save
              if ( hypoClone ) { protoParticleClone->addToCalo( hypoClone ); }
            } else {
              // Use basic Cloner
              auto hypoClone = cloneKeyedContainerItem<CaloHypoCloner>( hypo );
              if ( hypoClone ) {
                // For basic Cloner, set hypo, cluster and digit smartref vectors to empty
                // as the basic cloning keeps them pointing to the originals
                hypoClone->clearHypos();
                hypoClone->clearDigits();
                hypoClone->clearClusters();
                // save the clone
                protoParticleClone->addToCalo( hypoClone );
              }
            }
          } else {
            // save the original
            protoParticleClone->addToCalo( hypo );
          }
        }
      }
    }

    this->cloneRelations( protoParticle, protoParticleClone );
  }

  return ( vetoed ? const_cast<LHCb::ProtoParticle*>( protoParticle ) : protoParticleClone );
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( ProtoParticleCloner )

//=============================================================================
