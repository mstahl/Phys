2017-08-28 Phys v23r6
=====================

Release for 2017 production
---------------------------

This version is released on the 2017-patches branch.
It is based on Gaudi v28r2, LHCb v42r6, Lbcom v20r6 and Rec v21r6 and uses LCG_88 with ROOT 6.08.06.

- Add TrackClonerWithClusters tool, modify CaloDigitCloner to support existing CaloAdc containers.
  Required in Moore and Tesla, respectively.
  - See merge request !168
- Add a new functor for accessing related info.
  - See merge request !158
- LoKiPhys: add new functor ANNPID to provide access to arbitrary ANNPID tuning.
  - See merge request !155
