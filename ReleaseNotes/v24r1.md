2017-10-18 Phys v24r1
=====================

This version is released on the master branch.
It is based on Gaudi v29r0, LHCb v43r1, Lbcom v21r1 and Rec v22r1 and uses LCG_91 with ROOT 6.10.06.

- Remove unnecessary include of Property.h.
  - See merge request !208
- Fix call to CaloHypo::setPosition.
  - See merge request !207
- Add RestoreCaloRecoChain (see MR !204).
  - See merge request !205
- Fix untested StatusCodes uncovered by gaudi/Gaudi!386.
  - See merge request !201
- Add missing CaloClusterCloner import.
  - See merge request !199
- Add include LoKi/Streamers.h.
  - See merge request !169
- Fix clang warnings.
  - See merge request !166
- Merge MR!158 from 2017-patches to master.
  - See merge request !162
- Fully qualify enum values.
  - See merge request !161
