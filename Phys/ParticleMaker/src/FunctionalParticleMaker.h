/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "GaudiAlg/Transformer.h"

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IProtoParticleFilter.h"
#include "Kernel/ParticleProperty.h"
#include "TrackInterfaces/ITrackSelector.h"

/** @class FunctionalParticleMaker FunctionalParticleMaker.h
 *
 * @brief LHCb::Particle creation from LHCb::ProtoParticle objects.
 *
 * TODO
 *
 * Example usage:
 *
 * @code {.py}
 * # TODO
 * @endcode
 *
 */
class FunctionalParticleMaker : public Gaudi::Functional::Transformer<LHCb::Particles( LHCb::ProtoParticles const& )> {

public:
  FunctionalParticleMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  LHCb::Particles operator()( LHCb::ProtoParticles const& protos ) const override;

private:
  /// Return the State that should be used for defining the particle's kinematics
  const LHCb::State* usedState( const LHCb::Track* track ) const;

  /// Fill the Particle's members using the protoparticle and particle properties
  bool fill_particle( const LHCb::ProtoParticle* proto, const LHCb::ParticleProperty* property,
                      LHCb::Particle* particle ) const;

  /// Print Warnings if the RICH or Muon PID objects are mising from the protoparticle
  void check_pid( const LHCb::ProtoParticle* proto ) const;

  LHCb::IParticlePropertySvc* m_particlePropertySvc = nullptr;

  /// Tool for filling the particle's kinematics from a given state
  ToolHandle<IParticle2State> m_particle_from_state_tool = {this, "Particle2StateTool", "Particle2State"};
  /// Tool to filter tracks associated to input protoparticles
  ToolHandle<ITrackSelector> m_track_selector = {this, "TrackSelector", "LoKi::Hybrid::TrackSelector"};
  /// Tool to filter protoparticles
  ToolHandle<IProtoParticleFilter> m_proto_selector = {this, "ProtoParticleFilter",
                                                       "LoKi::Hybrid::ProtoParticleFilter"};

  /// Particle property information for matter particles to be created
  const LHCb::ParticleProperty* m_particle_prop = nullptr;
  /// Particle property information for antimatter particles to be created
  const LHCb::ParticleProperty* m_antiparticle_prop = nullptr;

  /// Confidence level assigned to each Particle
  const double m_CL = 50 * Gaudi::Units::perCent;

  Gaudi::Property<std::string> m_particleid{this, "ParticleID", "UNDEFINED",
                                            "Particle species hypothesis to apply to each created object."};
  Gaudi::Property<bool>        m_check_pid{
      this, "CheckPID", true,
      "If true, print Warnings if the Particle ID information on the ProtoParticle is missing."};

  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{this, "Nb created particles"};
  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{this, "Nb created anti-particles"};
  /// Number of protoparticles passing the protoparticle filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_proto_filter{this, "# passed ProtoParticle filter"};
  /// Number of tracks passing the track filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_track_filter{this, "# passed Track filter"};
};
