/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DIELECTRONMAKER_H
#define DIELECTRONMAKER_H 1

// Include files
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "ChargedParticleMakerBase.h"
#include "Kernel/IBremAdder.h"
#include "Kernel/IParticleTransporter.h"
#include "Kernel/IProtoParticleFilter.h"
#include "TrackInterfaces/ITrackSelector.h"

/** @class DiElectronMaker DiElectronMaker.h
 *
 *  maker algorithm to produce dielectron with correct bremsstrahlung treatment (killing overlap)
 *
 *  @author Olivier Deschamps
 *  @date   2011-02-25
 */
class DiElectronMaker : public ChargedParticleMakerBase {

public:
  /// Standard constructor
  DiElectronMaker( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~DiElectronMaker(); ///< Destructor
  StatusCode makeParticles( LHCb::Particle::Vector& particles ) override;
  StatusCode initialize() override; ///< Algorithm initialization

private:
  const ITrackSelector*     trSel() const { return m_trSel; }
  double                    ConfLevel( LHCb::Particle* electron );
  double                    ePID( LHCb::Particle* electron );
  double                    veloCnv( LHCb::Particle* p1, LHCb::Particle* p2 );
  double                    veloCh( LHCb::Particle* p1 );
  std::pair<double, double> getY( const LHCb::ProtoParticle* proto, double zcalo );
  Gaudi::XYZPoint           getPoCA( LHCb::Particle* particle, const Gaudi::XYZPoint PVpos );
  void                      clear( LHCb::Particle::Vector& vector );
  StatusCode combinepair( LHCb::Particle* ele1, LHCb::Particle* ele2, LHCb::Particle& mother, LHCb::Vertex& vertex );

private:
  const ITrackSelector*              m_trSel;
  IProtoParticleFilter*              m_pFilter;
  LHCb::Calo::Interfaces::IElectron* m_caloElectron;
  DeCalorimeter*                     m_calo;

  std::string                   m_toolType;
  std::string                   m_toolName;
  std::vector<std::string>      m_eleinputs;
  bool                          m_oppSign;
  bool                          m_symgamma;
  const LHCb::ParticleProperty* m_ePps;
  const LHCb::ParticleProperty* m_gPps;

  // cuts
  double                m_ecl;
  double                m_gcl;
  double                m_mmax;
  double                m_mmin;
  double                m_eOpMax;
  double                m_eOpMin;
  double                m_aFactor;
  double                m_ptmin;
  double                m_eptmin;
  int                   m_method;
  double                m_deltaY;
  double                m_deltaYmax;
  double                m_ecalE;
  double                m_vc;
  double                m_eidmax;
  double                m_eidmin;
  double                m_gid;
  double                m_zcalo;
  bool                  m_addBrem;
  bool                  m_combpair;
  double                m_maxdistpair;
  IParticleTransporter* m_transporter;
  std::string           m_transporterName;

  // counters
  mutable Gaudi::Accumulators::Counter<> m_partCombinerFailCounter{this, "ParticleCombiner failure"};
  mutable Gaudi::Accumulators::Counter<> m_partCombiner2ndFailCounter{this, "ParticleCombiner 2nd try failure"};
  mutable Gaudi::Accumulators::Counter<> m_pairCombinerFailCounter{this, "PairCombiner failure"};
  mutable Gaudi::Accumulators::Counter<> m_pairCombinerSuccCounter{this, "PairCombiner success"};
  mutable Gaudi::Accumulators::Counter<> m_partCombiner2ndSuccCounter{this, "ParticleCombiner 2nd try success"};
  mutable Gaudi::Accumulators::Counter<> m_partCombinerSuccCounter{this, "ParticleCombiner success"};
  mutable Gaudi::Accumulators::Counter<> m_transEle1FailCounter{this, "Transporter ele1 failure"};
  mutable Gaudi::Accumulators::Counter<> m_transEle2FailCounter{this, "Transporter ele2 failure"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbSelectedElectronsCounter{this, "Selected electrons"};
  mutable Gaudi::Accumulators::BinomialCounter<>            m_applyingBremcorrCounter{this,
                                                                           "Applying Brem-correction to dielectron"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nInputProtoP{this, "Input protoP"};
  mutable Gaudi::Accumulators::Counter<>                    m_nBremStrahlungRemoval{this, "BremStrahlung removal"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nTotInputE{this, "Total input electrons"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nCreatedEE{this, "Created -> ee"};
};

#endif // DIELECTRONMAKER_H
