/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/Transformer.h"

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IProtoParticleFilter.h"
#include "Kernel/ParticleProperty.h"
#include "TrackInterfaces/ITrackSelector.h"

#include <array>

namespace Pid {
  enum class Key { e, mu, pi, K, p };
  inline constexpr auto keys = {Key::e, Key::mu, Key::pi, Key::K, Key::p};

  int pidCode( Key pid ) {
    switch ( pid ) {
    case Key::pi:
      return 211;
    case Key::K:
      return 321;
    case Key::mu:
      return 13;
    case Key::e:
      return 11;
    case Key::p:
      return 2212;
    }
    throw std::runtime_error( "impossible key to find PID code" );
  }

  const std::string toString( Key k ) {
    switch ( k ) {
    case Key::e:
      return "electron";
    case Key::mu:
      return "mu";
    case Key::pi:
      return "pi";
    case Key::K:
      return "K";
    case Key::p:
      return "proton";
    }
    throw std::runtime_error( "impossible key to convert PID to string" );
  }
  std::ostream& operator<<( std::ostream& os, Key k ) { return os << toString( k ); }

  template <typename ValueType>
  class Array {
    std::array<ValueType, size( keys )> m_prob = {};

  public:
    ValueType& operator()( Key k ) { return m_prob[static_cast<int>( k )]; }
    ValueType  operator()( Key k ) const { return m_prob[static_cast<int>( k )]; }
  };

  template <typename ValueType>
  Key bestPid( Array<ValueType> const& array ) {
    return std::max( keys, [&array]( Key lhs, Key rhs ) { return array( lhs ) < array( rhs ); } );
  }
} // namespace Pid

/** @class ParticleMakerForParticleFlow
 *
 * @brief LHCb::Particle creation from LHCb::ProtoParticle objects. Uses best ProbNN for particle ID
 *
 */

class ParticleMakerForParticleFlow
    : public Gaudi::Functional::Transformer<LHCb::Particles( LHCb::ProtoParticles const& )> {

public:
  ParticleMakerForParticleFlow( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  LHCb::Particles operator()( LHCb::ProtoParticles const& protos ) const override;

private:
  /// Return the State that should be used for defining the particle's kinematics
  const LHCb::State* usedState( const LHCb::Track* track ) const;

  /// Fill the Particle's members using the protoparticle and particle properties
  bool fill_particle( const LHCb::ProtoParticle* proto, const LHCb::ParticleProperty* property,
                      LHCb::Particle& particle ) const;

  /// Print Warnings if the RICH or Muon PID objects are mising from the protoparticle
  void check_pid( const LHCb::ProtoParticle* proto ) const;

  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{this, "ParticleProperty", "LHCb::ParticlePropertySvc",
                                                                  "Particle property service"};

  /// Tool for filling the particle's kinematics from a given state
  ToolHandle<IParticle2State> m_particle_from_state_tool = {this, "Particle2StateTool", "Particle2State"};
  /// Tool to filter tracks associated to input protoparticles
  ToolHandle<ITrackSelector> m_track_selector = {this, "TrackSelector", "LoKi::Hybrid::TrackSelector"};
  /// Tool to filter protoparticles
  ToolHandle<IProtoParticleFilter> m_proto_selector = {this, "ProtoParticleFilter",
                                                       "LoKi::Hybrid::ProtoParticleFilter"};

  /// Apply high momentum trackcut if cut>0
  Gaudi::Property<float> m_c_over_e_cut{this, "c_over_e_cut", 0., "Cut on qOverP()/sqrt( sta.errQOverP2()"};

  /// Particle property information for matter particles to be created
  Pid::Array<const LHCb::ParticleProperty*> m_particle_prop;
  /// Particle property information for antimatter particles to be created
  Pid::Array<const LHCb::ParticleProperty*> m_antiparticle_prop;

  Gaudi::Property<bool> m_check_pid{
      this, "CheckPID", true,
      "If true, print Warnings if the Particle ID information on the ProtoParticle is missing."};

  /// Number of input protoparticles
  mutable Gaudi::Accumulators::StatCounter<> m_ninput_proto{this, "01: # input ProtoParticle"};
  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{this, "04: # created particles"};
  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{this, "05: # created anti-particles"};
  /// Number of protoparticles passing the protoparticle filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_proto_filter{this, "03: # passed ProtoParticle filter"};
  /// Number of tracks passing the track filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_track_filter{this, "02: # passed Track filter"};
  /// Number of tracks passing the qOverP()/sqrt( sta.errQOverP2() cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_c_over_e_filter{this, "06: # passed c/e filter"};

  /// Counters by pid
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_npions{this, "06: #pions"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nkaons{this, "07: #kaons"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nmuons{this, "08: #muons"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nelectrons{this, "09: #electrons"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nprotons{this, "10: #protons"};

  /// Counter of high p tracks
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nHighP{this, "11: #High P tracks"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_failed_to_fill{this, "Failed to fill Particle from State"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_no_muonpid{
      this, "ProtoParticle has MUON information but NULL MuonPID SmartRef"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_failed_RichPid{
      this, "ProtoParticle has RICH information but NULL RichPID SmartRef!"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_track_state{
      this, "Found no state at ClosestToBeam or at FirstMeasurement for track. Using first state instead"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_failed_to_fill_particle{this,
                                                                                  "Failed to fill Particle, rejecting"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_failed_proto_no_track{
      this, "Charged ProtoParticle with no Track found. Ignoring."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_failed_track_no_states{
      this, "Track has empty states. This is likely to be bug"};
};

DECLARE_COMPONENT( ParticleMakerForParticleFlow )

ParticleMakerForParticleFlow::ParticleMakerForParticleFlow( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"InputProtoParticles", LHCb::ProtoParticleLocation::Charged}},
                   {KeyValue{"Output", "Phys/ParticleMakerForParticleFlow/Particles"}} ) {}

StatusCode ParticleMakerForParticleFlow::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) { return sc; }

  for ( auto k : Pid::keys ) {
    const auto* tmp = m_particlePropertySvc->find( LHCb::ParticleID( pidCode( k ) ) );
    if ( !tmp ) { return Error( "Could not find ParticleProperty for " + toString( k ) ); }
    m_particle_prop( k )     = tmp;
    m_antiparticle_prop( k ) = m_particle_prop( k )->antiParticle();
  }

  return sc;
}

LHCb::Particles ParticleMakerForParticleFlow::operator()( LHCb::ProtoParticles const& protos ) const {
  LHCb::Particles particles;
  int             nparticles     = 0;
  int             nantiparticles = 0;

  Pid::Array<int> pidCount;

  m_ninput_proto += protos.size();

  uint nHighP = 0;

  for ( const auto* proto : protos ) {
    const auto* track = proto->track();
    // Sanity checks
    if ( track == nullptr ) {
      ++m_failed_proto_no_track;
      continue;
    }
    if ( track->states().empty() ) {
      ++m_failed_track_no_states;
      continue;
    }

    const auto selected_track = m_track_selector->accept( *track );
    m_npassed_track_filter += selected_track;
    if ( !selected_track ) { continue; }

    const auto selected_proto = m_proto_selector->isSatisfied( proto );
    m_npassed_proto_filter += selected_proto;
    if ( !selected_proto ) { continue; }

    auto c_over_e = std::numeric_limits<double>::max();
    if ( m_c_over_e_cut > 0 ) {
      auto   sta = track->firstState();
      double c( fabs( sta.qOverP() ) ), e( sqrt( sta.errQOverP2() ) );
      c_over_e = c / e;
      if ( msgLevel( MSG::DEBUG ) && c_over_e < m_c_over_e_cut )
        info() << "Particle: p: " << ( *track ).p() << " 1/p: " << 1. / ( *track ).p() << " 1st State: " << &sta
               << " 1/p: " << c << " e: " << e << " c/e: " << c_over_e << " c_over_e_cut: " << m_c_over_e_cut << endmsg;
    }
    const auto selected_c_over_e = ( ( c_over_e < m_c_over_e_cut ) ? false : true );
    m_npassed_c_over_e_filter += selected_c_over_e;
    if ( !selected_c_over_e ) {
      ++nHighP;
      continue;
    }

    // When filling PID, run additional checks and selections
    if ( m_check_pid.value() ) { check_pid( proto ); }

    // Find the PID corresponding to the best ProbNN hypothesis
    Pid::Array<float> prob;
    prob( Pid::Key::pi ) = proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNpi, 0 );
    prob( Pid::Key::K )  = proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNk, 0 );
    prob( Pid::Key::mu ) = proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNmu, 0 );
    prob( Pid::Key::e )  = proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNe, 0 );
    prob( Pid::Key::p )  = proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNp, 0 );

    auto bestID = bestPid( prob );

    auto* best_part_prop     = m_particle_prop( bestID );
    auto* best_antipart_prop = m_antiparticle_prop( bestID );

    // Get the (anti)particle property corresponding to the charge we have
    const LHCb::ParticleProperty* prop =
        ( proto->charge() == best_part_prop->charge()
              ? best_part_prop
              : proto->charge() == best_antipart_prop->charge() ? best_antipart_prop : nullptr );

    // Fill the particle information
    auto particle = std::make_unique<LHCb::Particle>();
    bool filled   = fill_particle( proto, prop, *particle );
    if ( !filled ) {
      ++m_failed_to_fill_particle;
      continue;
    }

    // Record what we successfully fill
    if ( prop == best_part_prop ) {
      ++nparticles;
    } else if ( prop == best_antipart_prop ) {
      ++nantiparticles;
    }

    ++pidCount( bestID );

    particles.add( particle.release() );
  }
  m_nHighP += nHighP;

  m_nparticles += nparticles;
  m_nantiparticles += nantiparticles;

  m_npions += pidCount( Pid::Key::pi );
  m_nkaons += pidCount( Pid::Key::K );
  m_nmuons += pidCount( Pid::Key::mu );
  m_nelectrons += pidCount( Pid::Key::e );
  m_nprotons += pidCount( Pid::Key::p );

  return particles;
}

const LHCb::State* ParticleMakerForParticleFlow::usedState( const LHCb::Track* track ) const {
  const LHCb::State* state = nullptr;
  // Default: closest to the beam for track types with a Velo part.
  if ( !( track->checkType( LHCb::Track::Types::Downstream ) || track->checkType( LHCb::Track::Types::Ttrack ) ) ) {
    state = track->stateAt( LHCb::State::Location::ClosestToBeam );
  }
  // If not available: first measurement. Default for Downstream and T tracks
  if ( !state ) { state = track->stateAt( LHCb::State::Location::FirstMeasurement ); }
  // Backup
  if ( !state ) {
    ++m_no_track_state;
    state = &track->firstState();
  }
  return state;
}

void ParticleMakerForParticleFlow::check_pid( const LHCb::ProtoParticle* proto ) const {
  // RICH links
  if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::RichPIDStatus ) ) {
    const LHCb::RichPID* rpid = proto->richPID();
    if ( !rpid ) ++m_failed_RichPid;
  }
  // Muon links
  if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus ) ) {
    const LHCb::MuonPID* mpid = proto->muonPID();
    if ( !mpid ) ++m_no_muonpid;
  }
}

bool ParticleMakerForParticleFlow::fill_particle( const LHCb::ProtoParticle*    proto,
                                                  const LHCb::ParticleProperty* property,
                                                  LHCb::Particle&               particle ) const {
  particle.setMeasuredMass( property->mass() );
  particle.setMeasuredMassErr( 0 );

  particle.setParticleID( property->particleID() );
  particle.setProto( proto );

  // Take the default confidence level
  particle.setConfLevel( 50 * Gaudi::Units::perCent );

  // Find the appropriate state and use it to define this object's kinematics
  const LHCb::State* state = usedState( proto->track() );
  return m_particle_from_state_tool->state2Particle( *state, particle )
      .orElse( [&] { ++m_failed_to_fill; } )
      .isSuccess();
}
