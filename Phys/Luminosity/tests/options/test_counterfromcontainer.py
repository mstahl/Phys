###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the creation of LHCb::HltLumiSummary objects.

These objects are created by measuring container sizes, and then by merging
multiple summaries in to one. The merged summary is then encoded as a raw bank
and inserted in to the raw event, and then the bank is decoded and compared
with the original merged summary.
"""
from __future__ import print_function

# Workaround for ROOT-10769
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy

from Configurables import (
    ApplicationMgr,
    HltLumiSummaryDecoder,
    HltLumiWriter,
    LHCbApp,
    LumiCounterFromContainer__RecVertex_v1,
    LumiCounterFromContainer__Track_v1,
    LumiCounterMerger,
    UnpackRecVertex,
    UnpackTrack,
    bankKiller,
)
import GaudiPython as GP
from PRConfig import TestFileDB

RAW_EVENT_LOC = "/Event/DAQ/RawEvent"
# Counter names, which must correspond to entries in the LumiCounters enum
TRACK_COUNTER_NAME = "Velo"
TRACK_COUNTER_KEY = getattr(cppyy.gbl.LHCb.LumiCounters, TRACK_COUNTER_NAME)
VERTEX_COUNTER_NAME = "PV3D"
VERTEX_COUNTER_KEY = getattr(cppyy.gbl.LHCb.LumiCounters, VERTEX_COUNTER_NAME)

# Unpack pRec/Track/Best
track_unpacker = UnpackTrack()
# Unpack pRec/Vertex/Primary
vertex_unpacker = UnpackRecVertex()

# Create the track summary
track_counter = LumiCounterFromContainer__Track_v1("CountTracks")
track_counter.CounterName = TRACK_COUNTER_NAME
track_counter.InputContainer = track_unpacker.getDefaultProperty("OutputName")
track_counter.OutputSummary = str(track_counter.InputContainer) + "Summary"

# Create the vertex summary
vertex_counter = LumiCounterFromContainer__RecVertex_v1("CountPrimaryVertices")
vertex_counter.CounterName = VERTEX_COUNTER_NAME
vertex_counter.InputContainer = vertex_unpacker.getDefaultProperty(
    "OutputName")
vertex_counter.OutputSummary = str(vertex_counter.InputContainer) + "Summary"

# Create the merged summary
merger = LumiCounterMerger("CountMerged")
merger.InputSummaries = [
    str(track_counter.OutputSummary),
    str(vertex_counter.OutputSummary)
]
merger.OutputSummary = "/Event/LumiSummary"

bank_killer = bankKiller("LumiBankKiller")
bank_killer.RawEventLocations = [RAW_EVENT_LOC]
bank_killer.BankTypes = ["HltLumiSummary"]

encoder = HltLumiWriter("LumiEncoder")
encoder.InputBank = merger.OutputSummary
# TODO check that we have a merged raw event
encoder.RawEventLocation = RAW_EVENT_LOC

decoder = HltLumiSummaryDecoder("LumiDecoder")
decoder.RawEventLocations = RAW_EVENT_LOC
decoder.OutputContainerName = "{}Decoded".format(merger.OutputSummary)

# Define the sequence
ApplicationMgr().TopAlg = [
    track_unpacker,
    vertex_unpacker,
    track_counter,
    vertex_counter,
    merger,
    bank_killer,
    encoder,
    decoder,
]

app = LHCbApp()
app.EvtMax = 100

# Pick a file that has the reconstruction available
f = TestFileDB.test_file_db[
    "2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed"]
f.setqualifiers(configurable=app)
f.run(configurable=app, withDB=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

event = 0
while event < app.EvtMax:
    tracks = TES[str(track_counter.InputContainer)]
    vertices = TES[str(vertex_counter.InputContainer)]
    if not (tracks and vertices):
        event += 1
        appMgr.run(1)
        continue

    # Check the track summary
    track_summary = TES[str(track_counter.OutputSummary)]
    assert track_summary.hasInfo(TRACK_COUNTER_KEY), "Incomplete summary"
    assert track_summary.info(TRACK_COUNTER_KEY,
                              -1) == len(tracks), "Length mismatch"

    # Check the vertex summary
    vertex_summary = TES[str(vertex_counter.OutputSummary)]
    assert vertex_summary.hasInfo(VERTEX_COUNTER_KEY), "Incomplete summary"
    assert vertex_summary.info(VERTEX_COUNTER_KEY,
                               -1) == len(vertices), "Length mismatch"

    # Check the merged summary
    merged_summary = TES[str(merger.OutputSummary)]
    assert merged_summary.hasInfo(TRACK_COUNTER_KEY), "Incomplete summary"
    assert merged_summary.hasInfo(VERTEX_COUNTER_KEY), "Incomplete summary"
    assert merged_summary.info(TRACK_COUNTER_KEY,
                               -1) == len(tracks), "Length mismatch"
    assert merged_summary.info(VERTEX_COUNTER_KEY,
                               -1) == len(vertices), "Length mismatch"

    # Compare the decoded summary with the original merges summary
    decoded_summary = TES[str(decoder.OutputContainerName)]
    assert decoded_summary.info(TRACK_COUNTER_KEY, -1) == merged_summary.info(
        TRACK_COUNTER_KEY, -1), "Length mismatch"
    assert decoded_summary.info(VERTEX_COUNTER_KEY, -1) == merged_summary.info(
        VERTEX_COUNTER_KEY, -1), "Length mismatch"

    event += 1
    appMgr.run(1)
