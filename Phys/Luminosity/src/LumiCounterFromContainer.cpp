/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cstdint>

#include "GaudiAlg/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track_v1.h"
#include "PrKernel/PrSelection.h"

/** @class LumiCounterFromContainer<T>
 *
 * @brief Create a HltLumiSummary object filled with the size of a single container.
 *
 * Takes a single container as input, and outputs an HltLumiSummary object,
 * which is essentially a map, with a single key-value pair. The key is taken
 * as the CounterName property, and the value is taken as the length of the
 * input container.
 *
 * The template parameter T is the input container type.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 *
 */
template <typename ContainerType>
class LumiCounterFromContainer : public Gaudi::Functional::Transformer<LHCb::HltLumiSummary( const ContainerType& )> {
public:
  using base_class = Gaudi::Functional::Transformer<LHCb::HltLumiSummary( const ContainerType& )>;

  LumiCounterFromContainer( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, {"InputContainer", ""}, {"OutputSummary", ""} ) {}

  StatusCode initialize() override {
    StatusCode sc = base_class::initialize();
    if ( !sc ) { return sc; }

    m_counterKey = LHCb::LumiCounters::counterKeyToType( m_counterName.value() );
    if ( m_counterKey == LHCb::LumiCounters::Unknown ) {
      this->error() << "LumiCounter not found with name: " << m_counterName << endmsg;
      sc = StatusCode::FAILURE;
    } else {
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << m_counterName << " key value: " << m_counterKey << endmsg;
      }
    }

    return sc;
  }

  LHCb::HltLumiSummary operator()( const ContainerType& container ) const override {
    auto nobjects = container.size();
    m_nobjects += nobjects;
    LHCb::HltLumiSummary summary{};
    summary.addInfo( m_counterKey, nobjects );

    return summary;
  }

private:
  /// Luminosity counter name, chosen from LHCb::LumiCounters::s_counterKeyTypMap
  Gaudi::Property<std::string> m_counterName{this, "CounterName", "", "Luminosity counter name"};

  /// Records container sizes
  mutable Gaudi::Accumulators::StatCounter<std::uint64_t> m_nobjects{this, "Nb objects"};

  /// The CounterName property mapped to an int by LHCb::LumiCounters::counterKeyToType
  int m_counterKey = -1;
};

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Event::v1::Track::Container>,
                           "LumiCounterFromContainer__Track_v1" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Pr::Velo::Tracks>, "LumiCounterFromContainer__PrVeloTracks" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::RecVertex::Container>,
                           "LumiCounterFromContainer__RecVertex_v1" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Event::v2::RecVertices>,
                           "LumiCounterFromContainer__RecVertex_v2" )
