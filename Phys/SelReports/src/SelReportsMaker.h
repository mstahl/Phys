/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SELREPORTSMAKER_H
#define SELREPORTSMAKER_H 1

// Include files
#include <memory>
#include <numeric>
#include <string>
#include <vector>

// from Gaudi
#include "Event/HltObjectSummary.h"
#include "Event/RelatedInfoMap.h"
#include "Event/Track.h"
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiKernel/VectorMap.h"

#include "HltDAQ/IReportConvert.h"
#include "HltDAQ/ReportConvertTool.h"
#include "Kernel/IANNSvc.h"

namespace LHCb {
  class CaloCluster;
  class Particle;
  class RecVertex;
  class HltSelReports;
  class HltDecReports;
} // namespace LHCb

/** @class TrackSelReportsMaker TrackSelReportsMaker.h
 *  Algorithm to translate HltSummary  into HltSelResults and associated HltObjectSummaries
 */

namespace SelReports::types {
  using Track_t                   = LHCb::Track;
  using Vertex_t                  = LHCb::RecVertex;
  using DecReports_t              = LHCb::HltDecReports;
  using Output_t                  = std::tuple<LHCb::HltSelReports, HltObjectSummary::Container>;
  using In_t                      = Gaudi::Functional::details::vector_of_const_<DataObject*>;
  using MergingMultiTransformer_t = Gaudi::Functional::MergingMultiTransformer<Output_t( In_t const& )>;
} // namespace SelReports::types

class SelReportsMaker : public SelReports::types::MergingMultiTransformer_t {
public:
  using base = SelReports::types::MergingMultiTransformer_t;

  enum OutputInfoLevel {
    kMinInfoLevel      = 0, ///< lhcbIDs only
    kStandardInfoLevel = 1, ///< lhcbIDs + standard numerical info(object specific)
    kExtraInfoLevel    = 2, ///< lhcbIDs + extra numerical info (no standard info!)
    kMaxInfoLevel      = 3  ///< lhcbIDs + standard + extra numerical info
  };

  enum GlobalSelectionIDs { kHlt1GlobalID = 1, kHlt2GlobalID = 2 };

  /// Standard constructor
  SelReportsMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode                  initialize() override; ///< Algorithm initialization
  SelReports::types::Output_t operator()( SelReports::types::In_t const& in ) const override; ///< Algorithm execution

protected:
  IANNSvc* hltANNSvc() const { return m_hltANNSvc; }

private:
  ToolHandle<IReportConvert>                            m_conv{this, "ConvTool", "ReportConvertTool"};
  DataObjectReadHandle<SelReports::types::DecReports_t> m_dec_reps{this, "DecReports", ""};
  Gaudi::Property<std::vector<std::string>>             m_selection_names{
      this, "SelectionNames", {}, "Specify the line selection names."};
  Gaudi::Property<std::vector<Gaudi::StringKey>> m_inputcandidatetypes{
      this, "CandidateTypes", {}, "Map of selection name to candidate type."};
  Gaudi::Property<int> m_hlt_stage{this, "HltStage", 1, "Hlt1 or 2?"};

  /// for producing numerical info to be saved on the object
  LHCb::HltObjectSummary::Info infoToSave( const LHCb::Track& object, unsigned int const presentInfoLevel,
                                           bool const turbo ) const;
  LHCb::HltObjectSummary::Info infoToSave( const LHCb::RecVertex& object, unsigned int const presentInfoLevel,
                                           bool const turbo ) const;

  /// for splitting strings
  std::vector<std::string>& split( const std::string&, char, std::vector<std::string>& );
  std::vector<std::string>  split( const std::string&, char );

  /// for converting objects in to summaries

  const LHCb::HltObjectSummary* store_( const LHCb::Track& object, HltObjectSummary::Container& objectSummaries,
                                        unsigned int const presentInfoLevel, bool const turbo ) const;
  const LHCb::HltObjectSummary* store_( const LHCb::RecVertex& object, HltObjectSummary::Container& objectSummaries,
                                        unsigned int const presentInfoLevel, bool const turbo ) const;

  template <typename T>
  const LHCb::HltObjectSummary* store( const ContainedObject* obj, HltObjectSummary::Container& objectSummaries,
                                       unsigned int const presentInfoLevel, bool const turbo ) const {
    auto const* tobj = dynamic_cast<const T*>( obj );
    if ( !tobj ) return nullptr;
    auto i = std::find_if( std::begin( objectSummaries ), std::end( objectSummaries ),
                           [&]( const LHCb::HltObjectSummary* hos ) {
                             return hos->summarizedObjectCLID() == tobj->clID() && hos->summarizedObject() == tobj;
                           } );
    return ( ( i != std::end( objectSummaries ) ) && !turbo )
               ? *i
               : store_( *tobj, objectSummaries, presentInfoLevel, turbo );
  }

  int rank( const LHCb::Track& object, unsigned int const presentInfoLevel, bool const turbo ) const;
  int rank( const LHCb::RecVertex& object, unsigned int const presentInfoLevel, bool const turbo ) const;

  /// rank LHCbIDs for selection rank
  int rankLHCbIDs( const std::vector<LHCb::LHCbID>& lhcbIDs ) const;

  /// for trimming output size and disabling output all together (if 0)
  unsigned int maximumNumberOfCandidatesToStore( const std::string& selectionName, bool const debugMode ) const;

  /// set present output parameters
  unsigned int setPresentInfoLevel( const std::string& selectionName, bool const debugMode ) const;

  /// enable Turbo level output
  Gaudi::Property<bool> m_enableTurbo{this, "EnableTurbo", true};

  /// HltANNSvc for making selection names to int selection ID
  IANNSvc* m_hltANNSvc;

  // RelatedInfo
  std::map<std::string, std::vector<std::string>> m_RelInfoLocationsMap;

  // from info id to its name
  GaudiUtils::VectorMap<int, std::string> m_infoIntToName;

  // get trigger selection names //  -- amalgamate into vector<struct>
  struct selectionInfo {
    Gaudi::StringKey id;
    Gaudi::StringKey type;
    int              intId;
    int              maxCand;
    int              maxCandDebug;
  };
  std::vector<selectionInfo> m_selectionInfo;

  /// for setting per selection properties
  typedef std::map<std::string, int>                 SelectionSetting;
  typedef SimpleProperty<std::map<std::string, int>> SelectionSettingProp;

  /// Where is the RecSummary location
  string m_RecSummaryLoc;

  /// debug event period (global, can't be change per selection)  0=never 1=always e.g. 100=every 100th event
  UnsignedIntegerProperty m_debugPeriod;

  /// default max number of candidates for Decision selections (can be overruled by per selection setting)
  UnsignedIntegerProperty m_maxCandidatesDecision;
  UnsignedIntegerProperty m_maxCandidatesDecisionDebug;
  UnsignedIntegerProperty m_maxCandidatesNonDecision;
  UnsignedIntegerProperty m_maxCandidatesNonDecisionDebug;

  /// per selection max number of candidates
  SelectionSettingProp m_maxCandidates;
  SelectionSettingProp m_maxCandidatesDebug;

  /// default output info level for Decision selections (can be overruled by per selection setting)
  UnsignedIntegerProperty m_infoLevelDecision;
  UnsignedIntegerProperty m_infoLevelDecisionDebug;
  UnsignedIntegerProperty m_infoLevelNonDecision;
  UnsignedIntegerProperty m_infoLevelNonDecisionDebug;

  /// per selection info level
  SelectionSettingProp m_infoLevel;
  SelectionSettingProp m_infoLevelDebug;

  /// whether selections killed by postscale are allowed to save candidates
  bool m_SuppressPostscale;
  bool m_SuppressPostscaleDebug;
};

#endif // SelReportsMAKER_H
