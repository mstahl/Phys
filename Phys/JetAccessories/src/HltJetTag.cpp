/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "GaudiAlg/Transformer.h"

// Event.
#include "Event/Particle.h"

// LoKi.
#include "LoKi/ParticleCuts.h"
#include "LoKi/PhysExtract.h"

using namespace std;
using namespace LHCb;

/*
Receives lists of jets and return only the jets that are tagged by the list of tags.
The tagging condition is DeltaR<ConeSizeTag (default: 0.5)
*/

class HltJetTag
    : public Gaudi::Functional::Transformer<LHCb::Particles( const LHCb::Particles&, const LHCb::Particles& )> {

public:
  /// Constructor.
  HltJetTag( const string& name, ISvcLocator* svc );

  // Main method
  LHCb::Particles operator()( const LHCb::Particles&, const LHCb::Particles& ) const override;

private:
  Gaudi::Property<float> m_ConeSizeTag{this, "ConeSizeTag", 0.5, "Cone size to tag jets."};
};

DECLARE_COMPONENT( HltJetTag )

/// Constructor.
HltJetTag::HltJetTag( const string& name, ISvcLocator* svc )
    : Transformer( name, svc, {KeyValue{"Jets", ""}, KeyValue{"Tags", ""}}, {"Output", "Phys/TaggedJets/Particles"} ) {}

LHCb::Particles HltJetTag::operator()( const LHCb::Particles& Jets, const LHCb::Particles& Tags ) const {

  static int count = 0;
  ++count;
  debug() << "=============== New event: " << count << "  ================" << endmsg;

  LHCb::Particles prts;

  for ( auto& jet : Jets ) {
    auto jetphi = jet->momentum().Phi();
    auto jeteta = jet->momentum().Eta();
    for ( auto& tag : Tags ) {
      auto tagphi = tag->momentum().Phi();
      auto tageta = tag->momentum().Eta();
      auto dphi   = tagphi - jetphi;
      auto deta   = tageta - jeteta;
      while ( dphi > M_PI ) dphi -= 2 * M_PI;
      while ( dphi <= -M_PI ) dphi += 2 * M_PI;
      if ( sqrt( dphi * dphi + deta * deta ) < m_ConeSizeTag ) {
        prts.insert( new LHCb::Particle( *jet ) );
        break;
      }
    }
  }

  debug() << "Number of output particles: " << prts.size() << endmsg;

  return prts;
}
