/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL.
#include <unordered_map>
#include <unordered_set>

#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Transformer.h"

// Jets.
#include "Kernel/IJetMaker.h"
#include "Kernel/JetEnums.h"

// Event.
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"

// LoKi.
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/PhysExtract.h"
#include "LoKi/VertexCuts.h"

// Root.
#include "Math/VectorUtil.h"

// Track to vertex relations
#include "Kernel/IRelatedPVFinder.h"
#include "Kernel/Particle2Vertex.h"
#include "Relations/Relation1D.h"

using namespace std;
using namespace LHCb;

/*
Run3 Jet building (JB) algorithm class for use in the HLT and offline.
2019-12-10 : Algorithm based from Run 2 code

Goal: Use list of particles and clusters them into jets using the
FastJet package. Optionally, it can then correct the jet energy
(JEC), calculate additional information about the jet, and tag jets with Particles.

1) FastJet Tool is created and configured with the defined parameters.
2) List of particles is created to call the Jet clustering. In this step,
particles can be filtered out.
3) List of jet particles is created from the reconstructed jets.
The jet reconstruction can be performed for each primary vertex or indenpendent
of the primary vertex
In Run2, the standard was to reconstruct inclusively, since the Pile-up impact was small.
If built per PV, then particles are associated to PVs if they contain one or more
tracks. Particles without tracks are associated to all PVs. The, jets are built
using the input for each PV and the PV location is set as the reference point of the jet.
Note that when building per PV, the neutrals are double counted when considering
jets from all PVs.

TODO: Include Jet Energy Calibration. In Run2, JEC was saved in histograms saved
in root files located in ParamFiles package.
TODO: Add a JetID filter , probably as a separate transformer.
*/

using P2PVTable = unique_ptr<LHCb::Relation1D<LHCb::RecVertex*, LHCb::Particle*>>;

class HltJetBuilderRun3
    : public Gaudi::Functional::Transformer<LHCb::Particles( const LHCb::Particles&, const LHCb::RecVertices& )> {

public:
  HltJetBuilderRun3( const std::string& name, ISvcLocator* svc ); // Constructor
  //  StatusCode      initialize() override;                     // Initialize
  LHCb::Particles operator()( const LHCb::Particles&, const LHCb::RecVertices& ) const override; // Main method

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_nbInput{this, "Nb of Inputs"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter{this, "Nb of Reconstructed Jets"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter10{this, "Nb of Reconstructed Jets with pT>=10GeV"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nbJetCounter20{this, "Nb of Reconstructed Jets with pT>=20GeV"};

  ///< Add information to jet
  void addInfo( Particle* prt ) const;

  Gaudi::Property<bool> m_jetVtx = {this, "JetsByVtx", true,
                                    "If true, build jets for each primary vertex, otherwise "
                                    "build inclusively."};

  ///< Relate a particles with bestPVs
  P2PVTable relateP2Vtx( std::vector<const LHCb::Particle*> Particles, const LHCb::RecVertices& PrimaryVertices ) const;

  /// Tool to RelatedPVFinder
  ToolHandle<IRelatedPVFinder> m_prtVtxTool = {
      this, "RelatedPVFinder",
      "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_/P2PVWithIPChi2"};

  // Input/output property members (Inputs and Output handled by base class).
  std::vector<std::string> m_inBans; ///< Input locations of Particles to ban.
  std::vector<std::string> m_inTags; ///< Input locations of Particle to tag.

  /// Tool to FastJet
  ToolHandle<IJetMaker> m_fj = {this, "FastJet", "LoKi::FastJetMaker"};

  /// MsgSvc counters
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_noBestPV{this,
                                                                       "Failed to relate a particle with its bestPV."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_FJfailed{this, "FastJet failed."};
};

/*-----------------------------------------------------------------------------
Implementation file for class : HltJetBuilderRun3
-----------------------------------------------------------------------------*/

// Declare the algorithm factory.
DECLARE_COMPONENT( HltJetBuilderRun3 )

//=============================================================================
// Constructor.
//=============================================================================
HltJetBuilderRun3::HltJetBuilderRun3( const std::string& name, ISvcLocator* svc )
    : Transformer( name, svc, {KeyValue{"Input", ""}, KeyValue{"PVLocation", "Rec/Vertex/Primary"}},
                   KeyValue{"Output", "Phys/Jets/Particles"} ) {}

//=============================================================================
// Execute.
//=============================================================================
LHCb::Particles HltJetBuilderRun3::operator()( const LHCb::Particles&   Particles,
                                               const LHCb::RecVertices& PrimaryVertices ) const {
  m_nbInput += Particles.size();
  auto c10 = m_nbJetCounter10.buffer();
  auto c20 = m_nbJetCounter20.buffer();

  // Output jets
  LHCb::Particles recojets;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "New event: #Particles: " << Particles.size() << " PVs: " << PrimaryVertices.size() << " PV list ----> ";
    for ( auto vtx : PrimaryVertices ) debug() << vtx << " ";
    debug() << "<----" << endmsg;
  }

  // Split Particles, separating photons, merged and resolved pi0s
  std::vector<const LHCb::Particle*> tmpparttracks;
  std::vector<const LHCb::Particle*> tmpneutrals;
  tmpparttracks.reserve( 0.7 * Particles.size() );
  tmpneutrals.reserve( 0.5 * Particles.size() );
  int pid;
  for ( auto const* prt : Particles ) {
    pid = prt->particleID().pid();
    ( pid == 22 || pid == 111 ) ? tmpneutrals.push_back( prt ) : tmpparttracks.push_back( prt );
  }

  // Relate particles with tracks (or their composites) to best PV
  auto vtx2prtRel = relateP2Vtx( tmpparttracks, PrimaryVertices );

  if ( m_jetVtx.value() ) { // Make jets by PVs
    if ( vtx2prtRel ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Making jets by PVs" << endmsg;
      for ( auto vtx : PrimaryVertices ) {
        auto tmppartRange =
            vtx2prtRel->relations( vtx ); // LHCb::Relation1D<LHCb::RecVertex *, LHCb::Particle *>::Range
        std::vector<const LHCb::Particle*> tmppart;
        tmppart.reserve( tmppartRange.size() + tmpneutrals.size() );
        for ( const auto prt : tmppartRange ) tmppart.push_back( prt.to() );
        // Extend including neutral (They go to every PV)
        for ( const auto prt : tmpneutrals ) tmppart.push_back( prt );

        // Make the jets
        std::vector<LHCb::Particle*> tmprecojets;
        tmprecojets.reserve( 40 );
        if ( m_fj->makeJets( tmppart.begin(), tmppart.end(), tmprecojets ).isFailure() ) ++m_msg_FJfailed;

        if ( msgLevel( MSG::DEBUG ) ) debug() << "Vtx: " << vtx << " #jets: " << tmprecojets.size() << endmsg;
        recojets.reserve( tmprecojets.size() );
        for ( auto* jet : tmprecojets ) {
          recojets.insert( jet );
          jet->addInfo( LHCb::JetIDInfo::vtx_x, vtx->position().X() );
          jet->addInfo( LHCb::JetIDInfo::vtx_y, vtx->position().Y() );
          jet->addInfo( LHCb::JetIDInfo::vtx_z, vtx->position().Z() );
        }
      }
    } else
      return recojets;
  } else { // Make jets inclusively
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Making jets inclusively" << endmsg;

    std::vector<LHCb::Particle*> tmppart{Particles.begin(), Particles.end()};

    // Make the jets
    std::vector<LHCb::Particle*> tmprecojets;
    tmprecojets.reserve( 40 );
    if ( m_fj->makeJets( tmppart.begin(), tmppart.end(), tmprecojets ).isFailure() ) ++m_msg_FJfailed;

    recojets.reserve( tmprecojets.size() );
    for ( auto* jet : tmprecojets ) {
      recojets.insert( jet );
      auto vtx = m_prtVtxTool->relatedPV( jet, PrimaryVertices );
      jet->addInfo( LHCb::JetIDInfo::vtx_x, vtx->position().X() );
      jet->addInfo( LHCb::JetIDInfo::vtx_y, vtx->position().Y() );
      jet->addInfo( LHCb::JetIDInfo::vtx_z, vtx->position().Z() );
    }
  }

  for ( auto* jet : recojets ) {
    addInfo( jet );
    const auto pt = jet->momentum().Pt();
    if ( pt >= 10000 ) { c10 += 1; }
    if ( pt >= 20000 ) { c20 += 1; }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    // Print jet info
    for ( auto* jet : recojets ) {
      auto jetBestPV = m_prtVtxTool->relatedPV( jet, PrimaryVertices );
      debug() << "P: " << jet->momentum().P() << " Pt: " << jet->momentum().Pt()
              << " Ntracks: " << jet->info( LHCb::JetIDInfo::Ntracks, 0 )
              << " MTF: " << jet->info( LHCb::JetIDInfo::MTF, 0 ) << " MNF: " << jet->info( LHCb::JetIDInfo::MNF, 0 )
              << " MPT: " << jet->info( LHCb::JetIDInfo::MPT, 0 ) << " CPF: " << jet->info( LHCb::JetIDInfo::CPF, 0 )
              << " JetWidth: " << jet->info( LHCb::JetIDInfo::JetWidth, 0 )
              << " JetWidthNorm: " << jet->info( LHCb::JetIDInfo::JetWidthNorm, 0 )
              << " #Jet daughters: " << jet->daughters().size() << " Jet BestPV: " << jetBestPV << endmsg;
    }
  }

  m_nbJetCounter += recojets.size();
  return recojets;
}

// Add additional information.
void HltJetBuilderRun3::addInfo( Particle* prt ) const {
  double cpx( 0 ), cpy( 0 ), cptMax( 0 ), nptMax( 0 ), width( 0 ), norm( 0 ), trks( 0 ), pt( prt->momentum().Pt() );
  Particle::ConstVector dtrs;
  LoKi::Extract::getParticles( prt, back_inserter( dtrs ), LoKi::Cuts::HASPROTO && LoKi::Cuts::ISBASIC );
  for ( auto dtr : dtrs ) {
    const Gaudi::LorentzVector& vec = dtr->momentum();
    if ( dtr->charge() != 0 ) {
      if ( vec.Pt() > cptMax ) cptMax = vec.Pt();
      cpx += vec.Px();
      cpy += vec.Py();
      ++trks;
    } else if ( vec.Pt() > nptMax )
      nptMax = vec.Pt();
    width += ROOT::Math::VectorUtil::DeltaR( vec, prt->momentum() ) * vec.Pt();
    norm += vec.Pt();
  }
  prt->addInfo( LHCb::JetIDInfo::Ntracks, trks );
  prt->addInfo( LHCb::JetIDInfo::MTF, cptMax / pt );
  prt->addInfo( LHCb::JetIDInfo::MNF, nptMax / pt );
  prt->addInfo( LHCb::JetIDInfo::MPT, cptMax );
  prt->addInfo( LHCb::JetIDInfo::CPF, sqrt( cpx * cpx + cpy * cpy ) / pt );
  prt->addInfo( LHCb::JetIDInfo::JetWidth, width );
  prt->addInfo( LHCb::JetIDInfo::JetWidthNorm, width / norm ); // Added to Phys/Phys/JetAccessories/Kernel/JetEnums.h.
                                                               // Remove this or the one above after tests
}

//  Relate particles with bestPVs
P2PVTable HltJetBuilderRun3::relateP2Vtx( std::vector<const LHCb::Particle*> Particles,
                                          const LHCb::RecVertices&           PrimaryVertices ) const {
  if ( !m_prtVtxTool ) { return nullptr; }
  if ( PrimaryVertices.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "No PrimaryVertices in this event" << endmsg;
    return nullptr;
  }
  auto vtx2prtRel = std::make_unique<LHCb::Relation1D<LHCb::RecVertex*, LHCb::Particle*>>();
  auto vtx        = LHCb::VertexBase::ConstVector( PrimaryVertices.begin(), PrimaryVertices.end() );
  for ( auto prt : Particles ) {
    auto bestPV = m_prtVtxTool->relatedPV( prt, vtx );
    if ( !bestPV ) {
      warning() << "Could not find bestPV for particle " << prt << " Particle ID: " << prt->particleID().pid()
                << endmsg;
      continue;
    }
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "====> Particle: " << prt << " Particle ID: " << prt->particleID().pid() << " Best PV: " << bestPV
              << " Chi2/nDofF: " << bestPV->chi2PerDoF() << " PV position: " << bestPV->position() << endmsg;
    // auto sc = vtx2prtRel->relate( dynamic_cast<const LHCb::RecVertex*>( bestPV ), prt );
    auto sc = vtx2prtRel->relate( static_cast<const LHCb::RecVertex*>( bestPV ), prt );
    if ( sc.isFailure() ) ++m_msg_noBestPV;
  }
  return vtx2prtRel;
}
