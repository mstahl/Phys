/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

#include "GaudiAlg/MergingTransformer.h"

// Event.
#include "Event/Particle.h"

// LoKi.
#include "LoKi/ParticleCuts.h"
#include "LoKi/PhysExtract.h"

using namespace std;
using namespace LHCb;

// Based on ~/work/public/stack//Gaudi/GaudiExamples/src/FunctionalAlgorithms/merging_transformer.cpp
/*

Receives a vector of LHCb::Particle containers, basic  or composite, outputs a list of particles
removing duplicities (no protoparticle is used more than once)

For each composite particle, if the protoparticles associated to their
daughters are not in the list of used protoparticles yet, they are added and the composite particle is saved in the
output list.

For each basic particle, if the protoparticle associated to it is not in the list of used protoparticles,
it is added there and the particle saved in the list of output particles.

The used protparticles are separated in neutral, positive ans negative charges containers, to speed up processing.

TODO: The test performed to avoid double counting is done by using the particle's address.
We plan to include LoKi::CheckOverlap for next version.

*/

using out_t  = LHCb::Particles;
using inputs = const Gaudi::Functional::vector_of_const_<LHCb::Particles>;

class HltParticleFlowRun3 : public Gaudi::Functional::MergingTransformer<out_t( inputs const& )> {

public:
  /// Constructor.
  HltParticleFlowRun3( const string& name, ISvcLocator* svc );

  // Main method
  LHCb::Particles operator()( inputs const& Inputs ) const override;

private:
  mutable Gaudi::Accumulators::Counter<>     m_count{this, "00: # Number of Events"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbInputChargedsCounter{this, "01: # Basic Charged Input Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbInputNeutralsCounter{this, "02: # Basic Neutral Input Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbInputCompositesCounter{this, "03: # Composite Input Particles"};

  mutable Gaudi::Accumulators::StatCounter<> m_nbChargedsCounter{this, "04: # Basic Charged Output Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbNeutralsCounter{this, "05: # Basic Neutral Output Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbCompositesCounter{this, "06: # Composite Output Particles"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbParticleCounter{this, "07: # Merged Output Particles"};

  mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedBasicsCounter{
      this, "08: # ProtoParticles from Basic Particles rejected"};
  mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedCompositesCounter{
      this, "09: # ProtoParticles from Composite Particles rejected"};
};

DECLARE_COMPONENT( HltParticleFlowRun3 )

/// Constructor.
HltParticleFlowRun3::HltParticleFlowRun3( const string& name, ISvcLocator* svc )
    : MergingTransformer( name, svc, {"Inputs", {}}, {"Output", "Phys/ParticleFlow/Particles"} ) {}

LHCb::Particles HltParticleFlowRun3::operator()( inputs const& Inputs ) const {

  ++m_count;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "=============== New event: " << m_count.nEntries() << "  ================" << endmsg;

    debug() << "Number of input vectors: " << Inputs.size() << endmsg;

    debug() << "Sizes of Input vectors: ";
    int total = 0;
    for ( auto& Input : Inputs ) {
      debug() << Input.size() << " ";
      total += Input.size();
    }
    debug() << " Total: " << total << endmsg;

    info() << "oooooooooooooooooooooooooooooooooooooooooooooooooooooo" << endmsg;
    for ( auto& Input : Inputs )
      for ( auto prt : Input ) {
        // if(!(prt->particleID().pid()==22||prt->particleID().pid()==111) ) continue;
        if ( prt->isBasicParticle() )
          info() << "Particle: " << prt << " PID: " << prt->particleID().pid() << " Proto: " << prt->proto() << endmsg;
        else {
          info() << "Particle: " << prt << " PID: " << prt->particleID().pid() << endmsg;
          Particle::ConstVector dtrs;
          LoKi::Extract::getParticles( prt, back_inserter( dtrs ), LoKi::Cuts::HASPROTO && LoKi::Cuts::ISBASIC );
          for ( auto dtr : dtrs )
            info() << "   Daughter: " << dtr << " "
                   << " PID: " << dtr->particleID().pid() << " Proto: " << dtr->proto() << endmsg;
        }
      }
    info() << "oooooooooooooooooooooooooooooooooooooooooooooooooooooo" << endmsg;

    std::map<const LHCb::ProtoParticle*, std::set<const LHCb::Particle*>> proto2partsMap;
    for ( auto& Input : Inputs ) {
      if ( Input.empty() ) continue;
      auto first = ( *Input.begin() );
      if ( first->isBasicParticle() )
        for ( auto prt : Input ) proto2partsMap[prt->proto()].insert( prt );
      else {
        for ( auto prt : Input ) {
          Particle::ConstVector dtrs;
          LoKi::Extract::getParticles( prt, back_inserter( dtrs ), LoKi::Cuts::HASPROTO && LoKi::Cuts::ISBASIC );
          for ( auto dtr : dtrs ) {
            proto2partsMap[( dtr )->proto()].insert( prt );
            proto2partsMap[( dtr )->proto()].insert( dtr );
          }
        }
      }
    }
    for ( auto proto : proto2partsMap ) {
      if ( proto.second.size() > 1 ) {
        debug() << proto.first << " ";
        for ( auto prt : proto.second )
          debug() << "(" << prt << ", " << prt->particleID().pid() << ", " << prt->p() << ") ";
        debug() << endmsg;
      }
    }
    debug() << "Number of protoparticles: " << proto2partsMap.size() << endmsg;
  }

  // Input can be in any order. Separate into categories (composites, basic_positives, basic_negatives and neutrals).
  // Already process particles whose daughter shoud be rejected

  std::vector<const LHCb::Particle*> composites;
  std::vector<const LHCb::Particle*> basics;

  std::set<const LHCb::ProtoParticle*> proto_neutrals;
  std::set<const LHCb::ProtoParticle*> proto_positives;
  std::set<const LHCb::ProtoParticle*> proto_negatives;

  int nInput = 0;
  for ( auto& Input : Inputs ) {
    ++nInput;
    if ( Input.empty() ) {
      debug() << "Input vector # " << nInput << " is empty. Skipping" << endmsg;
      continue;
    }
    auto first = ( *Input.begin() ); // Enough to interrogate the first particle in each Input list. Assumes same type
                                     // of Particles in each Input
    if ( first->isBasicParticle() )
      for ( auto prt : Input ) basics.emplace_back( prt );
    else
      for ( auto prt : Input ) composites.emplace_back( prt );
  }
  m_nbInputCompositesCounter += composites.size();

  LHCb::Particles prts;

  // Now process the composite particles
  int nbCompositesOut      = 0;
  int nbRejectedComposites = 0;
  for ( auto prt : composites ) {
    Particle::ConstVector dtrs;
    LoKi::Extract::getParticles( prt, back_inserter( dtrs ), LoKi::Cuts::HASPROTO && LoKi::Cuts::ISBASIC );
    bool used = std::any_of( dtrs.begin(), dtrs.end(), [&]( const auto& dtr ) {
      return ( dtr->charge() == 0 && proto_neutrals.find( dtr->proto() ) != proto_neutrals.end() ) ||
             ( dtr->charge() > 0 && proto_positives.find( dtr->proto() ) != proto_positives.end() ) ||
             ( proto_negatives.find( dtr->proto() ) != proto_negatives.end() );
    } );
    // A composite Particle is not added to the output list if at least one of their daughther protoparticles is already
    // used
    if ( used ) {
      debug() << "Composite rejected: " << prt << " PID: " << prt->particleID().pid() << " momentum: " << prt->p()
              << endmsg;
      ++nbRejectedComposites; // Some protoparticle from this composite particle was already used
    } else {
      for ( auto dtr : dtrs ) {
        if ( dtr->charge() == 0 )
          proto_neutrals.insert( dtr->proto() );
        else if ( dtr->charge() > 0 )
          proto_positives.insert( dtr->proto() );
        else
          proto_negatives.insert( dtr->proto() );
      }
      // Insert composite particles to the output list
      // **** To be reviewed: Using a vector of pointers
      prts.insert( prt->clone() );
      ++nbCompositesOut;
    }
  }

  // Now look at the basic particles ...
  int nbRejectedBasics = 0;
  int nbNeutralsIn     = 0;
  int nbChargedsIn     = 0;

  int nbNeutralsOut = 0;
  int nbChargedsOut = 0;

  for ( auto prt : basics ) {

    if ( prt->charge() == 0 ) {
      ++nbNeutralsIn;
      auto ret = proto_neutrals.insert( prt->proto() );
      if ( ret.second ) {
        prts.insert( prt->clone() );
        ++nbNeutralsOut;
      } else {
        debug() << "Neutral rejected: " << prt << " PID: " << prt->particleID().pid() << " momentum: " << prt->p()
                << endmsg;
        ++nbRejectedBasics;
      }
    }

    if ( prt->charge() > 0 ) {
      ++nbChargedsIn;
      auto ret = proto_positives.insert( prt->proto() );
      if ( ret.second ) {
        prts.insert( prt->clone() );
        ++nbChargedsOut;
      } else {
        debug() << "Positive rejected: " << prt << " PID: " << prt->particleID().pid() << " momentum: " << prt->p()
                << endmsg;
        ++nbRejectedBasics;
      }
    }

    if ( prt->charge() < 0 ) {
      ++nbChargedsIn;
      auto ret = proto_negatives.insert( prt->proto() );
      if ( ret.second ) {
        prts.insert( prt->clone() );
        ++nbChargedsOut;
      } else {
        debug() << "Negative rejected: " << prt << " PID: " << prt->particleID().pid() << " momentum: " << prt->p()
                << endmsg;
        ++nbRejectedBasics;
      }
    }
  }

  debug() << "Input: #composites: " << composites.size() << " #chargeds: " << nbChargedsIn
          << " #neutrals: " << nbNeutralsIn << endmsg;

  m_nbInputChargedsCounter += nbChargedsIn;
  m_nbInputNeutralsCounter += nbNeutralsIn;

  // Update counters
  m_nbNeutralsCounter += nbNeutralsOut;
  m_nbChargedsCounter += nbChargedsOut;
  m_nbCompositesCounter += nbCompositesOut;

  m_nbRejectedBasicsCounter += nbRejectedBasics;
  m_nbRejectedCompositesCounter += nbRejectedComposites;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Output: #composites: " << nbCompositesOut << " #chargeds: " << nbChargedsOut
            << " #neutrals: " << nbNeutralsOut << " Rejected composites: " << nbRejectedComposites
            << " Rejected basics: " << nbRejectedBasics << endmsg;
    debug() << "Number of output particles: " << prts.size() << endmsg;
  }

  m_nbParticleCounter += prts.size();
  return prts;
}
