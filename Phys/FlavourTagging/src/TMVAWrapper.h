/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TMVAWRAPPER_H
#define TMVAWRAPPER_H 1

// Include files
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

//#define SKIP_TMVA
//#define SKIP_CHARMBDT

/** @class TMVAWrapper TMVAWrapper.h
 *
 *  A wrapper for the Read<Method> classes in TMVAClassiification
 *  Each charm mode needs its own wrapper; they will inherit from this base class
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class TMVAWrapper {
public:
  virtual double GetMvaValue( std::vector<double> const& ) { return 0; };
  virtual ~TMVAWrapper() {}
};
#endif // TMVAWRAPPER_H
