/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/ILifetimeFitter.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IVertexFit.h"
#include <limits>

#include "TaggingHelpers.h"
#include "TaggingUtils.h"

#include "LoKi/ParticleProperties.h"
#include "LoKi/VertexCuts.h"

//--------------------------------------------------------------------
// Implementation file for class : TaggingUtils
//
// Author: Marco Musy
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

#include "LoKi/ParticleContextCuts.h"
#include "LoKi/ParticleCuts.h"

using namespace LoKi::Cuts;
using namespace LoKi::Types;
using namespace LoKi::Particles;

using CharmTaggerSpace::CharmMode;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggingUtils )

//====================================================================
TaggingUtils::TaggingUtils( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_dva( nullptr )
    , m_PVSelCriterion( "PVbyIPs" )
    , m_algNamePVReFitter( "LoKi::PVReFitter:PUBLIC" )
    , m_algNameLifetimeFitter( "LoKi::LifetimeFitter:PUBLIC" )
    , m_algNameVertexFitter( "LoKi::VertexFitter:PUBLIC" )
    , m_algNameDistanceCalculator( "LoKi::DistanceCalculator:PUBLIC" )
    , m_algNameParticleDescendants( "ParticleDescendants" )
    , m_PVReFitter( nullptr )
    , m_LifetimeFitter( nullptr )
    , m_VertexFitter( nullptr )
    , m_DistanceCalculator( nullptr )
    , m_ParticleDescendants( nullptr ) {
  declareInterface<ITaggingUtils>( this );

  declareProperty( "PVSelCriterion", m_PVSelCriterion = "PVbyIPs" );

  declareProperty( "PVReFitter", m_algNamePVReFitter );
  declareProperty( "LifetimeFitter", m_algNameLifetimeFitter );
  declareProperty( "VertexFitter", m_algNameVertexFitter );
  declareProperty( "DistanceCalculator", m_algNameDistanceCalculator );
  declareProperty( "ParticleDescendants", m_algNameParticleDescendants );
}

//=====================================================================
StatusCode TaggingUtils::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_dva = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this );
  if ( m_dva == nullptr ) {
    fatal() << "Coudn't get parent DVAlgorithm" << endmsg;
    return StatusCode::FAILURE;
  }

  m_PVReFitter = tool<IPVReFitter>( m_algNamePVReFitter, this );
  if ( m_PVReFitter == nullptr ) {
    fatal() << "Unable to retrieve IPVRefitter \'" << m_algNamePVReFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_LifetimeFitter = tool<ILifetimeFitter>( m_algNameLifetimeFitter, this );
  if ( m_LifetimeFitter == nullptr ) {
    fatal() << "Unable to retrieve ILifetimFitter \'" << m_algNameLifetimeFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_VertexFitter = tool<IVertexFit>( m_algNameVertexFitter, this );
  if ( m_VertexFitter == nullptr ) {
    fatal() << "Unable to retrieve IVertexFit \'" << m_algNameVertexFitter << "\'" << endmsg;
    return StatusCode::FAILURE;
  }

  m_DistanceCalculator = tool<IDistanceCalculator>( m_algNameDistanceCalculator, this ); // m_dva->distanceCalculator();
  if ( m_DistanceCalculator == nullptr ) {
    fatal() << "Unable to retrieve the IDistanceCalculator tool " << m_algNameDistanceCalculator << endmsg;
    return StatusCode::FAILURE;
  }

  m_ParticleDescendants = tool<IParticleDescendants>( m_algNameParticleDescendants, this );
  if ( !m_ParticleDescendants ) {
    fatal() << "Unable to retrieve ParticleDescendants tool " << m_algNameParticleDescendants << endmsg;
    return StatusCode::FAILURE;
  }

  // Particle IDs
  lambda_pid  = LoKi::Particles::pidFromName( "Lambda0" ).abspid();
  pi_pid      = LoKi::Particles::pidFromName( "pi+" ).abspid();
  pi0_pid     = LoKi::Particles::pidFromName( "pi0" ).abspid();
  k_pid       = LoKi::Particles::pidFromName( "K+" ).abspid();
  ks_pid      = 310; // LoKi::Particles::pidFromName("KS0").abspid();
  p_pid       = LoKi::Particles::pidFromName( "p+" ).abspid();
  e_pid       = LoKi::Particles::pidFromName( "e+" ).abspid();
  mu_pid      = LoKi::Particles::pidFromName( "mu+" ).abspid();
  d0_pid      = LoKi::Particles::pidFromName( "D0" ).abspid();
  d_pid       = LoKi::Particles::pidFromName( "D+" ).abspid();
  lambdaC_pid = LoKi::Particles::pidFromName( "Lambda_c+" ).abspid();

  return sc;
}
//==========================================================================
StatusCode TaggingUtils::calcDOCAmin( const Particle* axp, const Particle* p1, const Particle* p2, double& doca,
                                      double& docaerr ) {
  double           doca1( 0 ), doca2( 0 ), err1( 0 ), err2( 0 );
  const StatusCode sc1 = m_DistanceCalculator->distance( axp, p1, doca1, err1 );
  const StatusCode sc2 = m_DistanceCalculator->distance( axp, p2, doca2, err2 );

  doca = std::min( doca1, doca2 );
  if ( doca == doca1 ) {
    docaerr = err1;
  } else {
    docaerr = err2;
  }

  return StatusCode{sc1 && sc2};
}

//==========================================================================
StatusCode TaggingUtils::calcIP( const Particle* axp, const VertexBase* v, double& ip, double& iperr ) {
  ip                     = -100.0;
  iperr                  = 0.0;
  int              zsign = 0;
  double           ipC = 0, ipChi2 = 0;
  StatusCode       sc2 = m_DistanceCalculator->distance( axp, v, ipC, ipChi2 );
  Gaudi::XYZVector ipV;
  StatusCode       sc = m_DistanceCalculator->distance( axp, v, ipV );
  // if ( msgLevel(MSG::DEBUG) ) debug()<<"ipS: "<<ipC<<", ipV.R: "<<ipV.R()<<endmsg;
  if ( sc2 && ipChi2 != 0 ) {
    if ( sc ) zsign = ipV.z() > 0 ? 1 : -1;
    ip    = ipC * zsign; // IP with sign
    iperr = ipC / std::sqrt( ipChi2 );
  }
  // if ( msgLevel(MSG::DEBUG) )
  //   debug()<<"IP: "<<ipC<<", "<<ip<<", (sign = "<<zsign<<" )"<<endmsg;
  return sc2;
}

//=========================================================================
StatusCode TaggingUtils::calcIP( const Particle* axp, const RecVertex::ConstVector& PileUpVtx, double& ip,
                                 double& ipe ) {
  double     ipmin    = std::numeric_limits<double>::max();
  double     ipminerr = 0.0;
  StatusCode sc{StatusCode::SUCCESS}, lastsc{StatusCode::SUCCESS};

  for ( auto v : PileUpVtx ) {
    double ipx = 0, ipex = 0;
    double ipC = 0, ipChi2 = 0;
    sc = m_DistanceCalculator->distance( axp, v, ipC, ipChi2 );
    if ( ipChi2 ) {
      ipx  = ipC;
      ipex = ipC / sqrt( ipChi2 );
    }

    if ( sc ) {
      if ( ipx < ipmin ) {
        ipmin    = ipx;
        ipminerr = ipex;
      }
    } else
      lastsc = sc;
  }
  ip  = ipmin;
  ipe = ipminerr;

  return lastsc;
}

StatusCode TaggingUtils::GetVCHI2NDOF( const Particle* sigp, const Particle* tagp, double& VCHI2NDOF ) {
  VCHI2NDOF = 0.0;
  LHCb::Vertex Vtx;
  auto         statuscode = m_VertexFitter->fit( Vtx, *sigp, *tagp );
  if ( statuscode ) { VCHI2NDOF = Vtx.chi2() / Vtx.nDoF(); }
  return statuscode;
}

//=========================================================================
int TaggingUtils::countTracks( const LHCb::Particle::ConstVector& vtags ) { return vtags.size(); }

//=========================================================================
bool TaggingUtils::qualitySort( const LHCb::Particle* p, const LHCb::Particle* q ) {
  // if true is returned the second input particle is the "better" one
  if ( p->proto() && q->proto() ) {
    if ( p->proto()->track() && q->proto()->track() ) {
      const LHCb::Track& t1 = *p->proto()->track();
      const LHCb::Track& t2 = *q->proto()->track();
      // prefer tracks which have more subdetectors in
      // where TT counts as half a subdetector
      const unsigned nSub1 = ( t1.hasVelo() ? 2 : 0 ) + ( t1.hasTT() ? 1 : 0 ) + ( t1.hasT() ? 2 : 0 );
      const unsigned nSub2 = ( t2.hasVelo() ? 2 : 0 ) + ( t2.hasTT() ? 1 : 0 ) + ( t2.hasT() ? 2 : 0 );
      if ( nSub2 > nSub1 ) return true;
      if ( nSub1 > nSub2 ) return false;
      // if available, prefer lower ghost probability
      const double ghProb1 = t1.ghostProbability();
      const double ghProb2 = t2.ghostProbability();
      if ( -0. <= ghProb1 && ghProb1 <= 1. && -0. <= ghProb2 && ghProb2 <= 1. ) {
        if ( ghProb1 > ghProb2 ) return true;
        if ( ghProb2 > ghProb1 ) return false;
      }
      // prefer longer tracks
      if ( t1.nLHCbIDs() < t2.nLHCbIDs() ) return true;
      if ( t1.nLHCbIDs() > t2.nLHCbIDs() ) return false;
      // for same length tracks, have chi^2/ndf decide
      const double chi1 = t1.chi2() / double( t1.nDoF() );
      const double chi2 = t2.chi2() / double( t2.nDoF() );
      return ( chi1 > chi2 );
    }
  }
  // fall back on a pT comparison (higher is better) as last resort
  return p->pt() < q->pt();
}

//============================================================================
bool TaggingUtils::isInTree( const LHCb::Particle* axp, const LHCb::Particle::ConstVector& sons ) {

  for ( auto p : sons ) {
    using TaggingHelpers::dphi;
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::toString;

    const SameTrackStatus isSame = isSameTrack( *axp, *p );
    if ( isSame ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " particle is: " << toString( isSame ) << " isInTree part: " << axp->particleID().pid()
                  << " with p=" << axp->p() / Gaudi::Units::GeV << " pt=" << axp->pt() / Gaudi::Units::GeV
                  << " proto_axp,ip=" << axp->proto() << " " << p->proto() << endmsg;
      return true;
    }
  }
  return false;
}

//============================================================================
bool TaggingUtils::isInTree( const LHCb::Particle* axp, const LHCb::Particle::ConstVector& sons, double& dist_phi ) {
  dist_phi = std::numeric_limits<double>::max();
  for ( auto p : sons ) {
    using TaggingHelpers::dphi;
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::toString;
    const double deltaphi = fabs( dphi( axp->momentum().phi(), p->momentum().phi() ) );
    if ( dist_phi > deltaphi ) dist_phi = deltaphi;
    const SameTrackStatus isSame = isSameTrack( *axp, *p );
    if ( isSame ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " particle is: " << toString( isSame ) << " isinTree part: " << axp->particleID().pid()
                  << " with p=" << axp->p() / Gaudi::Units::GeV << " pt=" << axp->pt() / Gaudi::Units::GeV
                  << " proto_axp,ip=" << axp->proto() << " " << p->proto() << endmsg;
      return true;
    }
  }
  return false;
}

//=============================================================================
LHCb::Particle::ConstVector TaggingUtils::purgeCands( const LHCb::Particle::Range& cands, const LHCb::Particle& BS ) {
  // remove any charm cand that has descendents in common with the signal B
  LHCb::Particle::ConstVector purgedCands;

  Particle::ConstVector signalDaus = m_ParticleDescendants->descendants( &BS );

  for ( auto cand : cands ) {

    auto candDaus        = m_ParticleDescendants->descendants( cand );
    bool isUsedForSignal = false;

    for ( auto candDau : candDaus ) {

      const LHCb::ProtoParticle* proto_candDau = candDau->proto();

      for ( auto signalDau : signalDaus ) {
        if ( proto_candDau == signalDau->proto() ) isUsedForSignal = true;
        if ( isUsedForSignal ) break;
      }

      if ( isUsedForSignal ) break;
    }

    if ( !isUsedForSignal ) purgedCands.push_back( cand );
  }

  return purgedCands;
}

double TaggingUtils::TPVTAU( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) {
  double     ct = 0.0, ctErr = 0.0, ctChi2 = 0.0;
  StatusCode sc  = m_LifetimeFitter->fit( *vert, *cand, ct, ctErr, ctChi2 );
  double     tau = ( sc ) ? ct / picosecond : -1.0;
  return tau;
}

double TaggingUtils::TPVDIRA( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) {
  Fun fDIRA = DIRA( vert );
  return fDIRA( cand );
}

double TaggingUtils::TPVFD( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) {
  Fun fVD = VD( vert );
  return fVD( cand );
}

double TaggingUtils::TPVFDCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert ) {
  Fun fVDCHI2 = VDCHI2( vert );
  return fVDCHI2( cand );
}

double TaggingUtils::TPVIPCHI2( const LHCb::Particle* cand, const LHCb::RecVertex* vert, const char* id ) {
  LHCb::VertexBase::ConstVector verts;
  verts.push_back( vert );
  Fun ipPvChi2 = MIPCHI2( verts, this->getDistanceCalculator() );
  if ( id ) {
    Fun mip = MAXTREE( ipPvChi2, id == ABSID, -1 );
    return mip( cand );
  } else {
    return ipPvChi2( cand );
  }
}

bool TaggingUtils::isBestPV( const LHCb::Particle* cand, const RecVertex* vert ) {
  auto bpv  = (const RecVertex*)m_dva->bestVertex( cand );
  VFun dist = LoKi::Cuts::VVDCHI2( vert );
  return ( dist( bpv ) < 1 );
}

//=============================================================================
CharmMode TaggingUtils::getCharmDecayMode( const LHCb::Particle* cand, int candType ) {

  CharmMode mode = CharmMode::None;

  const auto& daus    = cand->daughters();
  int         numDaus = daus.size();

  switch ( candType ) {

  case 0: // full reco, exclusive

    if ( cand->particleID().abspid() == d0_pid ) {

      switch ( numDaus ) {

      case 2:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpi;
        break;

      case 3:
        // if (daus[0]->particleID().abspid() == ks_pid and
        //     daus[1]->particleID().abspid() == pi_pid and
        //     daus[2]->particleID().abspid() == pi_pid)
        //   mode = "D0_Kspipi";
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi0_pid )
          mode = CharmMode::Dz2kpipiz;
        break;

      case 4:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi_pid and daus[3]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpipipi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else if ( cand->particleID().abspid() == d_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == ks_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "Dp_Kspi";
        //   break;

      case 3:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid and
             daus[2]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dp2kpipi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }

    break;

  case 1: // part reco, inclusive

    if ( cand->particleID().abspid() == d0_pid ) {

      switch ( numDaus ) {

      case 2:
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == pi_pid )
          mode = CharmMode::Dz2kpiX;
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == e_pid )
          mode = CharmMode::Dz2keX;
        if ( daus[0]->particleID().abspid() == k_pid and daus[1]->particleID().abspid() == mu_pid )
          mode = CharmMode::Dz2kmuX;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else if ( cand->particleID().abspid() == d_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "Dp_KpiX";
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == e_pid)
        //     mode = "Dp_KeX";
        //   if (daus[0]->particleID().abspid() == k_pid and
        //       daus[1]->particleID().abspid() == mu_pid)
        //     mode = "Dp_KmuX";
        //   break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }

    break;

  case 2: // dstar reco

    switch ( numDaus ) {

      // case 3:
      //   if (daus[0]->particleID().abspid() == ks_pid and
      //       daus[1]->particleID().abspid() == pi_pid and
      //       daus[2]->particleID().abspid() == pi_pid)
      //     mode = "Dstar_D0_Kspipi";
      //   break;

    default:
      fatal() << "Invalid daus size: " << numDaus << " candtype: " << candType << endmsg;
    }
    break;

  case 3: // lambda reco

    if ( cand->particleID().abspid() == lambdaC_pid ) {

      switch ( numDaus ) {

        // case 2:
        //   if (daus[0]->particleID().abspid() == p_pid and
        //       daus[1]->particleID().abspid() == ks_pid)
        //     mode = "LambdaC_pKs";
        //   if (daus[0]->particleID().abspid() == lambda_pid and
        //       daus[1]->particleID().abspid() == pi_pid)
        //     mode = "LambdaC_LambdaPi";
        //   break;

      case 3:
        if ( daus[0]->particleID().abspid() == p_pid and daus[1]->particleID().abspid() == k_pid and
             daus[2]->particleID().abspid() == pi_pid )
          mode = CharmMode::LambdaC2pkpi;
        break;

      default:
        fatal() << "Invalid daus size: " << numDaus << " for candtype: " << candType << endmsg;
      }

    } else {
      fatal() << "Invalid charm type: " << cand->particleID().abspid() << " for candtype: " << candType << endmsg;
    }
    break;

  default:
    fatal() << "Invalid candtype: " << candType << endmsg;
  }

  return mode;
}
