/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSVtxChTagger.h"

// from Phys
#include "Kernel/ISecondaryVertexTool.h"
#include "Kernel/IVertexFit.h"

// from FlavourTagging project
#include "FlavourTagging/ITaggingUtils.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Utils/TaggingHelpers.h"

namespace Units = Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSVtxChTagger )

//==============================================================================
OSVtxChTagger::OSVtxChTagger( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );
}

//==============================================================================
StatusCode OSVtxChTagger::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_secVtxTool = tool<ISecondaryVertexTool>( "SVertexOneSeedTool", m_secVtxToolName, this );

  if ( m_secVtxTool == nullptr ) {
    error() << "Could not load the ISecondaryVertexTool " << m_secVtxToolName << endmsg;
    return StatusCode::FAILURE;
  }

  m_tagUtils = tool<ITaggingUtils>( "TaggingUtils", this );
  if ( m_tagUtils == nullptr ) {
    error() << "Could not load TaggingUtils." << endmsg;
    return StatusCode::FAILURE;
  }

  m_classifierFactory = tool<ITaggingClassifierFactory>( m_classifierFactoryName, this );
  if ( m_classifierFactory == nullptr ) {
    error() << "Could not load the TaggingClassifierFactory " << m_classifierFactoryName << endmsg;
    return StatusCode::FAILURE;
  } else {
    m_classifier = m_classifierFactory->taggingClassifier();
  }

  // check classifier ranges for positive/negative decisions
  if ( m_mvaValNegRange.value().first >= m_mvaValNegRange.value().second ) {
    error() << "MVA classifier value boundaries for negative decision are incorrectly ordered." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( m_mvaValPosRange.value().first >= m_mvaValPosRange.value().second ) {
    error() << "MVA classifier value boundaries for positive decision are incorrectly ordered." << endmsg;
    return StatusCode::FAILURE;
  }

  if ( m_mvaValNegRange.value().second > m_mvaValPosRange.value().first ) {
    error() << "MVA classifier ranges representing positive/negative decisions overlap." << endmsg;
    return StatusCode::FAILURE;
  }

  return sc;
}

//==============================================================================
StatusCode OSVtxChTagger::finalize() { return GaudiTool::finalize(); }

//==============================================================================
LHCb::Tagger OSVtxChTagger::tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int nPUVtxs,
                                 LHCb::Particle::ConstVector& tagParts ) {

  // if there is no associated vertex or no tagging particles retun "no tag"-tag
  if ( assocVtx == nullptr || tagParts.empty() ) return emptyTag();

  // build secondary vertices from tagging particles, if no vertices could be
  // built, return "no tag"-tag
  auto secVtxs = m_secVtxTool->buildVertex( *assocVtx, tagParts );
  if ( secVtxs.empty() ) return emptyTag();

  // use the best (=first) vertex in from vector of vertices
  const auto secVtx = secVtxs.at( 0 );

  // ensure that vertex has outgoing particles
  const auto secVtxOutParts = secVtx.outgoingParticlesVector();
  if ( secVtxOutParts.empty() ) return emptyTag();

  // Calculate vertex properties and features needed for the MVA
  // 1.) properties to be calculated using particles contributing to the secVtx
  Gaudi::LorentzVector secVtxMomentum;   // momentum vector of vertex
  double               secVtxCh    = 0.; // weighted vertex charge
  double               secVtxPt    = 0.; // average transverse momentum
  double               secVtxIPsig = 0.; // average impact parameter significance w.r.t. assocVtx
  double               secVtxDoca  = 0.; // DOCA of particles w.r.t. seed vertex

  // helper variables
  double weightSum = 0.; // sum of weights for weighted charge calculation

  // loop over all particles contributing to the secVtx
  for ( const auto outPart : secVtxOutParts ) {
    // sum of particle momenta vectors
    secVtxMomentum += outPart->momentum();

    // sum of particles' transverse momenta
    const double outPartPt = outPart->pt();
    secVtxPt += outPartPt / Units::GeV;

    // sum of weighted charges and sum of weights
    const double weight = std::pow( outPartPt / Units::GeV, m_vtxChWeightPowerK );
    secVtxCh += weight * outPart->charge();
    weightSum += weight;

    // sum of impact parameter significances w.r.t. assocVtx of B candidate
    double minIP_assocVtx( 0. ), minIPErr_assocVtx( 0. );
    m_tagUtils->calcIP( outPart, assocVtx, minIP_assocVtx, minIPErr_assocVtx ).ignore();
    secVtxIPsig += std::abs( minIP_assocVtx ) / minIPErr_assocVtx;

    // sum of DOCAs w.r.t. seed vertex (given by the first two entries of the outgoing particles)
    double doca_seedVtx( 0. ), docaErr_seedVtx( 0. );
    m_tagUtils->calcDOCAmin( outPart, secVtxOutParts.at( 0 ), secVtxOutParts.at( 1 ), doca_seedVtx, docaErr_seedVtx )
        .ignore();
    secVtxDoca += doca_seedVtx;
  }

  // normalise sums to averages if sanity checks succeed
  const int secVtxNumParts = secVtxOutParts.size(); // number of particles contributing to the vertex
  if ( weightSum > 0. || secVtxCh != 0. ) {
    secVtxCh /= weightSum;
    secVtxPt /= secVtxNumParts;
    secVtxIPsig /= secVtxNumParts;
    secVtxDoca /= secVtxNumParts;
  } else
    return emptyTag();

  // 2.) properties directly accessible or related to properties calculated in loop
  // const double seedLikelihood = secVtx.info(LHCb::Vertex::LastGlobal+1, 0.5); // seed likelihood
  const auto   secVtxRelPos = secVtx.position();                   // relative position of secVtx to assocVtx
  const double secVtxP      = secVtxMomentum.P() / Units::GeV;     // abs. 3-momentum of secVtx
  const double secVtxM      = secVtxMomentum.M() / Units::GeV;     // inv. mass of secVtx
  const double sevVtxGP     = secVtxP / ( 0.16 * secVtxM + 0.12 ); // corrected momentum, details
                                                                   // are unfortunately unknown (JW, 05/05/2017)

  const double constBMass        = 5.28;        // B mass constant
  const double constSpeedOfLight = 0.299792458; // c light constant;
  const double secVtxT           = std::sqrt( secVtxRelPos.Mag2() ) * constBMass /
                         ( sevVtxGP * constSpeedOfLight ); // decay time of the secVtx (assuming a B0 or B+)
  const double secVtxDphi = TaggingHelpers::dphi(
      secVtxMomentum.phi(), secVtxRelPos.Phi() ); // difference in phi between secVtx momentum and PV-SV direction

  // Apply requirements
  if ( secVtxCh < m_secVtxChMin || secVtxM < m_secVtxMMin || secVtxP < m_secVtxPMin || secVtxPt < m_secVtxPtMin ||
       secVtxPt * secVtxNumParts < m_secVtxPtSumMin || secVtxIPsig * secVtxNumParts < m_secVtxIPsigSumMin ||
       secVtxDoca * secVtxNumParts > m_secVtxDocaSumMax )
    return emptyTag();

  // create feature vector
  m_featureValues = {(double)m_tagUtils->countTracks( tagParts ),
                     (double)nPUVtxs,
                     (double)std::log( sigPart->pt() / Units::GeV ),
                     (double)secVtxNumParts,
                     (double)std::log( secVtxPt ),
                     (double)std::log( secVtxIPsig ),
                     (double)std::abs( secVtxCh ),
                     (double)std::log( secVtxM ),
                     (double)std::log( secVtxP ),
                     (double)secVtxDphi,
                     (double)std::log( secVtxT ),
                     (double)secVtxDoca};

  // get output of the MVA classifier
  const double mvaOutput = m_classifier->getClassifierValue( m_featureValues );

  // interpret classification decision from MVA
  int mvaDecision = 0;
  if ( m_mvaValNegRange.value().first <= mvaOutput && mvaOutput < m_mvaValNegRange.value().second )
    mvaDecision = -1;
  else if ( m_mvaValPosRange.value().first <= mvaOutput && mvaOutput < m_mvaValPosRange.value().second )
    mvaDecision = +1;
  else
    mvaDecision = 0;

  // combine classification decision and charge of the vertex charge into flavour tag
  const int tagDecision = -1 * mvaDecision * ( ( 0 < secVtxCh ) - ( secVtxCh < 0 ) );

  // TODO: perform calibration using a GLM implementation

  auto tag = emptyTag();
  tag.setDecision( tagDecision );
  tag.setOmega( -1. );
  tag.setCharge( secVtxCh );
  tag.setMvaValue( mvaOutput );

  return tag;
}

//==============================================================================
LHCb::Tagger OSVtxChTagger::tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                                 LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) {
  return tag( sigPart, assocVtx, puVtxs.size(), tagParts );
}

//==============================================================================
