/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

namespace MyDataSpace_reco14 {
  class ReadMLPBNN;
}

struct DataReaderCompileWrapper_reco14 {

  DataReaderCompileWrapper_reco14( std::vector<std::string>& );

  ~DataReaderCompileWrapper_reco14();

  double GetMvaValue( std::vector<double> const& );

private:
  MyDataSpace_reco14::ReadMLPBNN* datareader_reco14;
};
