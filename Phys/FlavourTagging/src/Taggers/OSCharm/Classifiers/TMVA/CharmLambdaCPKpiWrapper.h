/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMLAMBDACPKPIWRAPPER_H
#define CHARMLAMBDACPKPIWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyLambdaCPKpiSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyLambdaCPKpiSpace

/** @class CharmLambdaCPKpiWrapper CharmLambdaCPKpiWrapper.h
 *
 *  Wrapper for D0 -> Kpi classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmLambdaCPKpiWrapper : public TMVAWrapper {
public:
  CharmLambdaCPKpiWrapper( std::vector<std::string>& );
  ~CharmLambdaCPKpiWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyLambdaCPKpiSpace::ReadBDT*     mcreader;
  MyLambdaCPKpiSpace::PurityTable* purtable;
};
#endif // CHARMLAMBDACPKPIWRAPPER_H
