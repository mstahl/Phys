/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonSame_NN2.h"

// hack, otherwise: redefinitions...
//
namespace MyNN2Space {
#include "weights/NN2_SSK.dat/TMVAClassification_MLPBNN.class.C"
}

NN2ReaderCompileWrapper::NN2ReaderCompileWrapper( std::vector<std::string>& names )
    : nn2reader( new MyNN2Space::ReadMLPBNN( names ) ) {}

NN2ReaderCompileWrapper::~NN2ReaderCompileWrapper() { delete nn2reader; }

double NN2ReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) {
  return nn2reader->GetMvaValue( values );
}
