/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"

// from STL
#include <cmath>
#include <iostream>

OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0() {
  // initialize constants
  Initialize();

  // initialize transformation
  InitTransform();
}

void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::Initialize() { // build network structure
  fLayers       = 3;
  fLayerSize[0] = 10;
  fWeights[0]   = new double[10];
  fLayerSize[1] = 15;
  fWeights[1]   = new double[15];
  fLayerSize[2] = 1;
  fWeights[2]   = new double[1];
  // weight matrix from layer 0 to 1
  fWeightMatrix0to1[0][0]  = -0.506638876682555;
  fWeightMatrix0to1[1][0]  = 1.741490299234;
  fWeightMatrix0to1[2][0]  = 0.723586183379345;
  fWeightMatrix0to1[3][0]  = 1.48387962539483;
  fWeightMatrix0to1[4][0]  = -2.07991880195114;
  fWeightMatrix0to1[5][0]  = -1.21472073210566;
  fWeightMatrix0to1[6][0]  = -0.875104546338716;
  fWeightMatrix0to1[7][0]  = 1.90189007420191;
  fWeightMatrix0to1[8][0]  = -1.461546618571;
  fWeightMatrix0to1[9][0]  = -1.09342012770746;
  fWeightMatrix0to1[10][0] = -1.84706750224961;
  fWeightMatrix0to1[11][0] = -0.196281210796102;
  fWeightMatrix0to1[12][0] = -0.674263821048962;
  fWeightMatrix0to1[13][0] = -0.520334609495627;
  fWeightMatrix0to1[0][1]  = -0.934486961584865;
  fWeightMatrix0to1[1][1]  = 0.784207537368889;
  fWeightMatrix0to1[2][1]  = -0.26748590792973;
  fWeightMatrix0to1[3][1]  = 2.14645885175698;
  fWeightMatrix0to1[4][1]  = 0.319971437513358;
  fWeightMatrix0to1[5][1]  = 0.542897600591295;
  fWeightMatrix0to1[6][1]  = -0.329810557646291;
  fWeightMatrix0to1[7][1]  = -0.474509583020718;
  fWeightMatrix0to1[8][1]  = 0.636295788735072;
  fWeightMatrix0to1[9][1]  = 0.241122407578606;
  fWeightMatrix0to1[10][1] = -0.656521085222354;
  fWeightMatrix0to1[11][1] = -0.369896058323832;
  fWeightMatrix0to1[12][1] = 0.492564565189802;
  fWeightMatrix0to1[13][1] = -0.885554584037163;
  fWeightMatrix0to1[0][2]  = -1.84773351477761;
  fWeightMatrix0to1[1][2]  = -0.00441348027391859;
  fWeightMatrix0to1[2][2]  = 0.618252796282176;
  fWeightMatrix0to1[3][2]  = 1.43266145322658;
  fWeightMatrix0to1[4][2]  = -0.4934521407861;
  fWeightMatrix0to1[5][2]  = -0.0310845210975141;
  fWeightMatrix0to1[6][2]  = 0.163024517214146;
  fWeightMatrix0to1[7][2]  = -0.212615055658402;
  fWeightMatrix0to1[8][2]  = -1.37357537675655;
  fWeightMatrix0to1[9][2]  = 1.76992207111357;
  fWeightMatrix0to1[10][2] = -1.24179641126834;
  fWeightMatrix0to1[11][2] = 0.950246055827831;
  fWeightMatrix0to1[12][2] = 1.54855534197188;
  fWeightMatrix0to1[13][2] = 0.886972608169866;
  fWeightMatrix0to1[0][3]  = 1.93166711717247;
  fWeightMatrix0to1[1][3]  = 1.18736988903373;
  fWeightMatrix0to1[2][3]  = -0.72379688343581;
  fWeightMatrix0to1[3][3]  = -1.7482719876512;
  fWeightMatrix0to1[4][3]  = 0.497170205543102;
  fWeightMatrix0to1[5][3]  = 0.134581459544273;
  fWeightMatrix0to1[6][3]  = 1.60362156577097;
  fWeightMatrix0to1[7][3]  = 1.22738678051438;
  fWeightMatrix0to1[8][3]  = 1.61345527408643;
  fWeightMatrix0to1[9][3]  = 1.35156080586444;
  fWeightMatrix0to1[10][3] = -1.39808629865092;
  fWeightMatrix0to1[11][3] = 1.23253300575362;
  fWeightMatrix0to1[12][3] = -1.87149483631189;
  fWeightMatrix0to1[13][3] = -0.81942123056641;
  fWeightMatrix0to1[0][4]  = -1.43702060049025;
  fWeightMatrix0to1[1][4]  = -1.73808511551626;
  fWeightMatrix0to1[2][4]  = 1.41768008180603;
  fWeightMatrix0to1[3][4]  = 0.603261732671374;
  fWeightMatrix0to1[4][4]  = -1.18375639768958;
  fWeightMatrix0to1[5][4]  = 0.325565020087801;
  fWeightMatrix0to1[6][4]  = -0.272139995812486;
  fWeightMatrix0to1[7][4]  = 0.509397885905128;
  fWeightMatrix0to1[8][4]  = 1.66551517967125;
  fWeightMatrix0to1[9][4]  = -0.371353285141237;
  fWeightMatrix0to1[10][4] = 0.00379673395611909;
  fWeightMatrix0to1[11][4] = -1.07061361182052;
  fWeightMatrix0to1[12][4] = 0.67882591889466;
  fWeightMatrix0to1[13][4] = -0.823290718233407;
  fWeightMatrix0to1[0][5]  = -0.00463281793096571;
  fWeightMatrix0to1[1][5]  = -1.44394723883704;
  fWeightMatrix0to1[2][5]  = 0.537079752460074;
  fWeightMatrix0to1[3][5]  = 0.956381801391577;
  fWeightMatrix0to1[4][5]  = 0.49886784255207;
  fWeightMatrix0to1[5][5]  = -0.000738754443223478;
  fWeightMatrix0to1[6][5]  = -1.54618890015405;
  fWeightMatrix0to1[7][5]  = -1.74416502663541;
  fWeightMatrix0to1[8][5]  = 0.858051407902795;
  fWeightMatrix0to1[9][5]  = 0.619644313831286;
  fWeightMatrix0to1[10][5] = 1.64606173305424;
  fWeightMatrix0to1[11][5] = 0.697639213007172;
  fWeightMatrix0to1[12][5] = 0.989265658263505;
  fWeightMatrix0to1[13][5] = -0.798687255111615;
  fWeightMatrix0to1[0][6]  = -1.30174647362608;
  fWeightMatrix0to1[1][6]  = -0.185021518788717;
  fWeightMatrix0to1[2][6]  = 0.772543577098368;
  fWeightMatrix0to1[3][6]  = -0.882031280817199;
  fWeightMatrix0to1[4][6]  = -1.88035174715243;
  fWeightMatrix0to1[5][6]  = -1.65874640609167;
  fWeightMatrix0to1[6][6]  = -0.424016313342064;
  fWeightMatrix0to1[7][6]  = 0.273617271388934;
  fWeightMatrix0to1[8][6]  = 1.48334785090832;
  fWeightMatrix0to1[9][6]  = -1.34438149254547;
  fWeightMatrix0to1[10][6] = -1.02070565341051;
  fWeightMatrix0to1[11][6] = 1.66577008614846;
  fWeightMatrix0to1[12][6] = -0.257711222904228;
  fWeightMatrix0to1[13][6] = -1.62652257726686;
  fWeightMatrix0to1[0][7]  = -0.863158764626255;
  fWeightMatrix0to1[1][7]  = 0.252289715617323;
  fWeightMatrix0to1[2][7]  = -1.99335841826234;
  fWeightMatrix0to1[3][7]  = -2.30959170823728;
  fWeightMatrix0to1[4][7]  = 0.764414816510148;
  fWeightMatrix0to1[5][7]  = 1.9643787499166;
  fWeightMatrix0to1[6][7]  = -0.807241733528072;
  fWeightMatrix0to1[7][7]  = 1.48601112336269;
  fWeightMatrix0to1[8][7]  = -0.901103303114585;
  fWeightMatrix0to1[9][7]  = -0.821947862492942;
  fWeightMatrix0to1[10][7] = 0.0562344295332925;
  fWeightMatrix0to1[11][7] = 0.33703977437672;
  fWeightMatrix0to1[12][7] = -0.859995482170204;
  fWeightMatrix0to1[13][7] = 0.230437906551002;
  fWeightMatrix0to1[0][8]  = 0.585031312664584;
  fWeightMatrix0to1[1][8]  = 0.573828631245814;
  fWeightMatrix0to1[2][8]  = -1.16770823902412;
  fWeightMatrix0to1[3][8]  = 1.66096267018698;
  fWeightMatrix0to1[4][8]  = 0.377244664671141;
  fWeightMatrix0to1[5][8]  = -1.29148581253846;
  fWeightMatrix0to1[6][8]  = 0.712138353336451;
  fWeightMatrix0to1[7][8]  = -1.17297955470867;
  fWeightMatrix0to1[8][8]  = -1.35229282892145;
  fWeightMatrix0to1[9][8]  = -0.317765925330994;
  fWeightMatrix0to1[10][8] = -0.31040312736672;
  fWeightMatrix0to1[11][8] = 1.41116706183773;
  fWeightMatrix0to1[12][8] = 0.804515493497519;
  fWeightMatrix0to1[13][8] = 0.0965218112047634;
  fWeightMatrix0to1[0][9]  = 0.698425258001698;
  fWeightMatrix0to1[1][9]  = 0.368465010935769;
  fWeightMatrix0to1[2][9]  = -2.06075513894871;
  fWeightMatrix0to1[3][9]  = 0.783968428315031;
  fWeightMatrix0to1[4][9]  = 0.0345645954382788;
  fWeightMatrix0to1[5][9]  = -1.14076966139208;
  fWeightMatrix0to1[6][9]  = 1.3905016845792;
  fWeightMatrix0to1[7][9]  = -1.83838323898764;
  fWeightMatrix0to1[8][9]  = 0.106543658337066;
  fWeightMatrix0to1[9][9]  = 1.03870569021065;
  fWeightMatrix0to1[10][9] = 1.91842624688649;
  fWeightMatrix0to1[11][9] = -1.87256838232309;
  fWeightMatrix0to1[12][9] = 0.394658555391729;
  fWeightMatrix0to1[13][9] = 1.46921150798276;
  // weight matrix from layer 1 to 2
  fWeightMatrix1to2[0][0]  = 0.706570738765834;
  fWeightMatrix1to2[0][1]  = 0.429851026500724;
  fWeightMatrix1to2[0][2]  = -0.679345687057535;
  fWeightMatrix1to2[0][3]  = -0.518255007744718;
  fWeightMatrix1to2[0][4]  = -0.850186798486939;
  fWeightMatrix1to2[0][5]  = 0.00496076084517743;
  fWeightMatrix1to2[0][6]  = 0.402046333072796;
  fWeightMatrix1to2[0][7]  = -0.298381134090054;
  fWeightMatrix1to2[0][8]  = 0.0350812700394706;
  fWeightMatrix1to2[0][9]  = 0.632652178769795;
  fWeightMatrix1to2[0][10] = -0.157441667279293;
  fWeightMatrix1to2[0][11] = -0.359513014622246;
  fWeightMatrix1to2[0][12] = 0.93512951450038;
  fWeightMatrix1to2[0][13] = -0.480087071031808;
  fWeightMatrix1to2[0][14] = 0.0296764051962415;
}

double OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::GetMvaValue__( const std::vector<double>& inputValues ) const {
  if ( inputValues.size() != (unsigned int)fLayerSize[0] - 1 ) {
    std::cout << fClassName << ": Input vector needs to be of size " << fLayerSize[0] - 1 << std::endl;
    return 0;
  }

  for ( int l = 0; l < fLayers; l++ )
    for ( int i = 0; i < fLayerSize[l]; i++ ) fWeights[l][i] = 0;

  for ( int l = 0; l < fLayers - 1; l++ ) fWeights[l][fLayerSize[l] - 1] = 1;

  for ( int i = 0; i < fLayerSize[0] - 1; i++ ) fWeights[0][i] = inputValues[i];

  // layer 0 to 1
  for ( int o = 0; o < fLayerSize[1] - 1; o++ ) {
    for ( int i = 0; i < fLayerSize[0]; i++ ) {
      double inputVal = fWeightMatrix0to1[o][i] * fWeights[0][i];
      fWeights[1][o] += inputVal;
    }
    fWeights[1][o] = ActivationFnc( fWeights[1][o] );
  }
  // layer 1 to 2
  for ( int o = 0; o < fLayerSize[2]; o++ ) {
    for ( int i = 0; i < fLayerSize[1]; i++ ) {
      double inputVal = fWeightMatrix1to2[o][i] * fWeights[1][i];
      fWeights[2][o] += inputVal;
    }
    fWeights[2][o] = OutputActivationFnc( fWeights[2][o] );
  }

  return fWeights[2][0];
}

double OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::ActivationFnc( double x ) const {
  // sigmoid
  return 1.0 / ( 1.0 + exp( -x ) );
}
double OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::OutputActivationFnc( double x ) const {
  // identity
  return x;
}

// Clean up
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::Clear() {
  // nothing to clear
}

double OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::GetMvaValue( const std::vector<double>& inputValues ) const {
  // classifier response value
  double retval = 0;
  // classifier response, sanity check first
  if ( !IsStatusClean() ) {
    std::cout << "Problem in class \"" << fClassName << "\": cannot return classifier response"
              << " because status is dirty" << std::endl;
    retval = 0;
  } else {
    if ( IsNormalised() ) {
      // normalise variables
      std::vector<double> iV;
      int                 ivar = 0;
      for ( std::vector<double>::const_iterator varIt = inputValues.begin(); varIt != inputValues.end();
            varIt++, ivar++ ) {
        iV.push_back( NormVariable( *varIt, fVmin[ivar], fVmax[ivar] ) );
      }
      Transform( iV, -1 );
      retval = GetMvaValue__( iV );
    } else {
      std::vector<double> iV;
      int                 ivar = 0;
      for ( std::vector<double>::const_iterator varIt = inputValues.begin(); varIt != inputValues.end();
            varIt++, ivar++ ) {
        iV.push_back( *varIt );
      }
      Transform( iV, -1 );
      retval = GetMvaValue__( iV );
    }
  }
  return retval;
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::InitTransform_1() {
  // Decorrelation transformation, initialisation
  fDecTF_1[0][0][0] = 0.081400750629;
  fDecTF_1[0][0][1] = 0.00063576626319;
  fDecTF_1[0][0][2] = -0.00192352921832;
  fDecTF_1[0][0][3] = -0.0106218375593;
  fDecTF_1[0][0][4] = -0.00395333319651;
  fDecTF_1[0][0][5] = -0.00450857306138;
  fDecTF_1[0][0][6] = -0.00398778572105;
  fDecTF_1[0][0][7] = -0.00847890589286;
  fDecTF_1[0][0][8] = 0.00180913002742;
  fDecTF_1[0][1][0] = 0.00063576626319;
  fDecTF_1[0][1][1] = 1.70367121724;
  fDecTF_1[0][1][2] = -0.669131063135;
  fDecTF_1[0][1][3] = -0.0345752544484;
  fDecTF_1[0][1][4] = 0.0863441260388;
  fDecTF_1[0][1][5] = 0.430024339847;
  fDecTF_1[0][1][6] = 0.0551903430627;
  fDecTF_1[0][1][7] = -0.138753044296;
  fDecTF_1[0][1][8] = 0.0644524027687;
  fDecTF_1[0][2][0] = -0.00192352921832;
  fDecTF_1[0][2][1] = -0.669131063135;
  fDecTF_1[0][2][2] = 2.65385525302;
  fDecTF_1[0][2][3] = -0.070970501902;
  fDecTF_1[0][2][4] = -0.0496563041125;
  fDecTF_1[0][2][5] = -0.242299756365;
  fDecTF_1[0][2][6] = 0.0438945394611;
  fDecTF_1[0][2][7] = 0.150858846714;
  fDecTF_1[0][2][8] = -0.0726713620621;
  fDecTF_1[0][3][0] = -0.0106218375593;
  fDecTF_1[0][3][1] = -0.0345752544484;
  fDecTF_1[0][3][2] = -0.070970501902;
  fDecTF_1[0][3][3] = 1.37250240267;
  fDecTF_1[0][3][4] = -0.00336917287328;
  fDecTF_1[0][3][5] = 0.0232537172055;
  fDecTF_1[0][3][6] = -0.056576266228;
  fDecTF_1[0][3][7] = 0.00125959503065;
  fDecTF_1[0][3][8] = -0.0190061569506;
  fDecTF_1[0][4][0] = -0.00395333319651;
  fDecTF_1[0][4][1] = 0.0863441260388;
  fDecTF_1[0][4][2] = -0.0496563041125;
  fDecTF_1[0][4][3] = -0.00336917287328;
  fDecTF_1[0][4][4] = 1.02130987128;
  fDecTF_1[0][4][5] = 0.0269360096904;
  fDecTF_1[0][4][6] = 0.00889831468146;
  fDecTF_1[0][4][7] = -6.12674535042e-05;
  fDecTF_1[0][4][8] = 0.0174818570026;
  fDecTF_1[0][5][0] = -0.00450857306138;
  fDecTF_1[0][5][1] = 0.430024339847;
  fDecTF_1[0][5][2] = -0.242299756365;
  fDecTF_1[0][5][3] = 0.0232537172055;
  fDecTF_1[0][5][4] = 0.0269360096904;
  fDecTF_1[0][5][5] = 4.00206612233;
  fDecTF_1[0][5][6] = -0.137518797343;
  fDecTF_1[0][5][7] = -0.585304424988;
  fDecTF_1[0][5][8] = 0.0134456393262;
  fDecTF_1[0][6][0] = -0.00398778572105;
  fDecTF_1[0][6][1] = 0.0551903430627;
  fDecTF_1[0][6][2] = 0.0438945394611;
  fDecTF_1[0][6][3] = -0.056576266228;
  fDecTF_1[0][6][4] = 0.00889831468146;
  fDecTF_1[0][6][5] = -0.137518797343;
  fDecTF_1[0][6][6] = 7.50047940936;
  fDecTF_1[0][6][7] = 0.0447527588907;
  fDecTF_1[0][6][8] = 0.0249993837787;
  fDecTF_1[0][7][0] = -0.00847890589286;
  fDecTF_1[0][7][1] = -0.138753044296;
  fDecTF_1[0][7][2] = 0.150858846714;
  fDecTF_1[0][7][3] = 0.00125959503065;
  fDecTF_1[0][7][4] = -6.12674535042e-05;
  fDecTF_1[0][7][5] = -0.585304424988;
  fDecTF_1[0][7][6] = 0.0447527588907;
  fDecTF_1[0][7][7] = 0.878356096117;
  fDecTF_1[0][7][8] = 0.0214395715298;
  fDecTF_1[0][8][0] = 0.00180913002742;
  fDecTF_1[0][8][1] = 0.0644524027687;
  fDecTF_1[0][8][2] = -0.0726713620621;
  fDecTF_1[0][8][3] = -0.0190061569506;
  fDecTF_1[0][8][4] = 0.0174818570026;
  fDecTF_1[0][8][5] = 0.0134456393262;
  fDecTF_1[0][8][6] = 0.0249993837787;
  fDecTF_1[0][8][7] = 0.0214395715298;
  fDecTF_1[0][8][8] = 0.439187451568;
  fDecTF_1[1][0][0] = 0.0781524999799;
  fDecTF_1[1][0][1] = -0.000439840331061;
  fDecTF_1[1][0][2] = 0.00124819694967;
  fDecTF_1[1][0][3] = -0.000609188055104;
  fDecTF_1[1][0][4] = -0.0016101049832;
  fDecTF_1[1][0][5] = -0.00489327913171;
  fDecTF_1[1][0][6] = -0.00600330473594;
  fDecTF_1[1][0][7] = -0.0102585187888;
  fDecTF_1[1][0][8] = 0.000356085102501;
  fDecTF_1[1][1][0] = -0.000439840331061;
  fDecTF_1[1][1][1] = 1.64355422733;
  fDecTF_1[1][1][2] = -0.663900848111;
  fDecTF_1[1][1][3] = -0.0297545757588;
  fDecTF_1[1][1][4] = 0.107873927739;
  fDecTF_1[1][1][5] = 0.412347759151;
  fDecTF_1[1][1][6] = 0.0592416493117;
  fDecTF_1[1][1][7] = -0.124650340827;
  fDecTF_1[1][1][8] = 0.0601366020208;
  fDecTF_1[1][2][0] = 0.00124819694967;
  fDecTF_1[1][2][1] = -0.663900848111;
  fDecTF_1[1][2][2] = 2.74781597317;
  fDecTF_1[1][2][3] = -0.0736114262966;
  fDecTF_1[1][2][4] = -0.0853332138026;
  fDecTF_1[1][2][5] = -0.243366629472;
  fDecTF_1[1][2][6] = 0.0386971014702;
  fDecTF_1[1][2][7] = 0.133112579763;
  fDecTF_1[1][2][8] = -0.0648545926265;
  fDecTF_1[1][3][0] = -0.000609188055104;
  fDecTF_1[1][3][1] = -0.0297545757588;
  fDecTF_1[1][3][2] = -0.0736114262966;
  fDecTF_1[1][3][3] = 1.28497701085;
  fDecTF_1[1][3][4] = 0.0534856338113;
  fDecTF_1[1][3][5] = 7.95006267668e-05;
  fDecTF_1[1][3][6] = -0.0399094845956;
  fDecTF_1[1][3][7] = -0.000868016619155;
  fDecTF_1[1][3][8] = -0.0142991561983;
  fDecTF_1[1][4][0] = -0.0016101049832;
  fDecTF_1[1][4][1] = 0.107873927739;
  fDecTF_1[1][4][2] = -0.0853332138026;
  fDecTF_1[1][4][3] = 0.0534856338113;
  fDecTF_1[1][4][4] = 0.94986714079;
  fDecTF_1[1][4][5] = 0.0556695282228;
  fDecTF_1[1][4][6] = -0.0147849055754;
  fDecTF_1[1][4][7] = 0.00513251406217;
  fDecTF_1[1][4][8] = 0.0527323782666;
  fDecTF_1[1][5][0] = -0.00489327913171;
  fDecTF_1[1][5][1] = 0.412347759151;
  fDecTF_1[1][5][2] = -0.243366629472;
  fDecTF_1[1][5][3] = 7.95006267668e-05;
  fDecTF_1[1][5][4] = 0.0556695282228;
  fDecTF_1[1][5][5] = 3.82182338906;
  fDecTF_1[1][5][6] = -0.128542440669;
  fDecTF_1[1][5][7] = -0.594189737879;
  fDecTF_1[1][5][8] = 0.0248326340081;
  fDecTF_1[1][6][0] = -0.00600330473594;
  fDecTF_1[1][6][1] = 0.0592416493117;
  fDecTF_1[1][6][2] = 0.0386971014702;
  fDecTF_1[1][6][3] = -0.0399094845956;
  fDecTF_1[1][6][4] = -0.0147849055754;
  fDecTF_1[1][6][5] = -0.128542440669;
  fDecTF_1[1][6][6] = 6.51513265372;
  fDecTF_1[1][6][7] = -0.0293040278961;
  fDecTF_1[1][6][8] = 0.00985086355564;
  fDecTF_1[1][7][0] = -0.0102585187888;
  fDecTF_1[1][7][1] = -0.124650340827;
  fDecTF_1[1][7][2] = 0.133112579763;
  fDecTF_1[1][7][3] = -0.000868016619155;
  fDecTF_1[1][7][4] = 0.00513251406217;
  fDecTF_1[1][7][5] = -0.594189737879;
  fDecTF_1[1][7][6] = -0.0293040278961;
  fDecTF_1[1][7][7] = 0.864038221181;
  fDecTF_1[1][7][8] = 0.0472848088339;
  fDecTF_1[1][8][0] = 0.000356085102501;
  fDecTF_1[1][8][1] = 0.0601366020208;
  fDecTF_1[1][8][2] = -0.0648545926265;
  fDecTF_1[1][8][3] = -0.0142991561983;
  fDecTF_1[1][8][4] = 0.0527323782666;
  fDecTF_1[1][8][5] = 0.0248326340081;
  fDecTF_1[1][8][6] = 0.00985086355564;
  fDecTF_1[1][8][7] = 0.0472848088339;
  fDecTF_1[1][8][8] = 0.417476357613;
  fDecTF_1[2][0][0] = 0.0800178427379;
  fDecTF_1[2][0][1] = 0.000268288975577;
  fDecTF_1[2][0][2] = -0.000377967492396;
  fDecTF_1[2][0][3] = -0.0067587051067;
  fDecTF_1[2][0][4] = -0.00324313153313;
  fDecTF_1[2][0][5] = -0.00468880097891;
  fDecTF_1[2][0][6] = -0.00501023298315;
  fDecTF_1[2][0][7] = -0.0093868091084;
  fDecTF_1[2][0][8] = 0.00147495819304;
  fDecTF_1[2][1][0] = 0.000268288975577;
  fDecTF_1[2][1][1] = 1.68032375468;
  fDecTF_1[2][1][2] = -0.665576710605;
  fDecTF_1[2][1][3] = -0.0329450881438;
  fDecTF_1[2][1][4] = 0.0953504137787;
  fDecTF_1[2][1][5] = 0.423126907794;
  fDecTF_1[2][1][6] = 0.0571765913069;
  fDecTF_1[2][1][7] = -0.133054692424;
  fDecTF_1[2][1][8] = 0.0620955140521;
  fDecTF_1[2][2][0] = -0.000377967492396;
  fDecTF_1[2][2][1] = -0.665576710605;
  fDecTF_1[2][2][2] = 2.67513058599;
  fDecTF_1[2][2][3] = -0.0702918843133;
  fDecTF_1[2][2][4] = -0.0614968300212;
  fDecTF_1[2][2][5] = -0.24189377123;
  fDecTF_1[2][2][6] = 0.0445890189798;
  fDecTF_1[2][2][7] = 0.146784868687;
  fDecTF_1[2][2][8] = -0.0705762306763;
  fDecTF_1[2][3][0] = -0.0067587051067;
  fDecTF_1[2][3][1] = -0.0329450881438;
  fDecTF_1[2][3][2] = -0.0702918843133;
  fDecTF_1[2][3][3] = 1.33450661584;
  fDecTF_1[2][3][4] = 0.0190075247984;
  fDecTF_1[2][3][5] = 0.0131207603603;
  fDecTF_1[2][3][6] = -0.0481345606121;
  fDecTF_1[2][3][7] = 0.000208427813504;
  fDecTF_1[2][3][8] = -0.0172133557627;
  fDecTF_1[2][4][0] = -0.00324313153313;
  fDecTF_1[2][4][1] = 0.0953504137787;
  fDecTF_1[2][4][2] = -0.0614968300212;
  fDecTF_1[2][4][3] = 0.0190075247984;
  fDecTF_1[2][4][4] = 0.989850119353;
  fDecTF_1[2][4][5] = 0.0384746760637;
  fDecTF_1[2][4][6] = -0.00129904107994;
  fDecTF_1[2][4][7] = -0.000157799031389;
  fDecTF_1[2][4][8] = 0.0324168109114;
  fDecTF_1[2][5][0] = -0.00468880097891;
  fDecTF_1[2][5][1] = 0.423126907794;
  fDecTF_1[2][5][2] = -0.24189377123;
  fDecTF_1[2][5][3] = 0.0131207603603;
  fDecTF_1[2][5][4] = 0.0384746760637;
  fDecTF_1[2][5][5] = 3.9311820163;
  fDecTF_1[2][5][6] = -0.136859519495;
  fDecTF_1[2][5][7] = -0.589365258931;
  fDecTF_1[2][5][8] = 0.0182639819248;
  fDecTF_1[2][6][0] = -0.00501023298315;
  fDecTF_1[2][6][1] = 0.0571765913069;
  fDecTF_1[2][6][2] = 0.0445890189798;
  fDecTF_1[2][6][3] = -0.0481345606121;
  fDecTF_1[2][6][4] = -0.00129904107994;
  fDecTF_1[2][6][5] = -0.136859519495;
  fDecTF_1[2][6][6] = 7.08002774051;
  fDecTF_1[2][6][7] = 0.0139061389541;
  fDecTF_1[2][6][8] = 0.0204811236442;
  fDecTF_1[2][7][0] = -0.0093868091084;
  fDecTF_1[2][7][1] = -0.133054692424;
  fDecTF_1[2][7][2] = 0.146784868687;
  fDecTF_1[2][7][3] = 0.000208427813504;
  fDecTF_1[2][7][4] = -0.000157799031389;
  fDecTF_1[2][7][5] = -0.589365258931;
  fDecTF_1[2][7][6] = 0.0139061389541;
  fDecTF_1[2][7][7] = 0.870730579908;
  fDecTF_1[2][7][8] = 0.0323797632409;
  fDecTF_1[2][8][0] = 0.00147495819304;
  fDecTF_1[2][8][1] = 0.0620955140521;
  fDecTF_1[2][8][2] = -0.0705762306763;
  fDecTF_1[2][8][3] = -0.0172133557627;
  fDecTF_1[2][8][4] = 0.0324168109114;
  fDecTF_1[2][8][5] = 0.0182639819248;
  fDecTF_1[2][8][6] = 0.0204811236442;
  fDecTF_1[2][8][7] = 0.0323797632409;
  fDecTF_1[2][8][8] = 0.429390993755;
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::Transform_1( std::vector<double>& iv, int cls ) const {
  // Decorrelation transformation
  if ( cls < 0 || cls > 2 ) {
    if ( 2 > 1 )
      cls = 2;
    else
      cls = 2;
  }

  // define the indices of the variables which are transfor
  std::vector<int> indicesGet;
  std::vector<int> indicesPut;

  indicesGet.push_back( 0 );
  indicesGet.push_back( 1 );
  indicesGet.push_back( 2 );
  indicesGet.push_back( 3 );
  indicesGet.push_back( 4 );
  indicesGet.push_back( 5 );
  indicesGet.push_back( 6 );
  indicesGet.push_back( 7 );
  indicesGet.push_back( 8 );
  indicesPut.push_back( 0 );
  indicesPut.push_back( 1 );
  indicesPut.push_back( 2 );
  indicesPut.push_back( 3 );
  indicesPut.push_back( 4 );
  indicesPut.push_back( 5 );
  indicesPut.push_back( 6 );
  indicesPut.push_back( 7 );
  indicesPut.push_back( 8 );

  std::vector<double> tv;
  for ( int i = 0; i < 9; i++ ) {
    double v = 0;
    for ( int j = 0; j < 9; j++ ) v += iv[indicesGet.at( j )] * fDecTF_1[cls][i][j];
    tv.push_back( v );
  }
  for ( int i = 0; i < 9; i++ ) iv[indicesPut.at( i )] = tv[i];
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::InitTransform_2() {
  // Normalization transformation, initialisation
  fMin_2[0][0] = 0.076336696744;
  fMax_2[0][0] = 7.06789731979;
  fMin_2[1][0] = 0.284225195646;
  fMax_2[1][0] = 10.3993749619;
  fMin_2[2][0] = 0.076336696744;
  fMax_2[2][0] = 10.3993749619;
  fMin_2[0][1] = 3.30455565453;
  fMax_2[0][1] = 8.86950778961;
  fMin_2[1][1] = 3.16146588326;
  fMax_2[1][1] = 9.15793228149;
  fMin_2[2][1] = 3.16146588326;
  fMax_2[2][1] = 9.15793228149;
  fMin_2[0][2] = -4.10888385773;
  fMax_2[0][2] = 1.93104457855;
  fMin_2[1][2] = -3.9934296608;
  fMax_2[1][2] = 2.18026781082;
  fMin_2[2][2] = -4.10888385773;
  fMax_2[2][2] = 2.18026781082;
  fMin_2[0][3] = -3.68065237999;
  fMax_2[0][3] = 4.22020339966;
  fMin_2[1][3] = -5.98346042633;
  fMax_2[1][3] = 4.31200742722;
  fMin_2[2][3] = -5.98346042633;
  fMax_2[2][3] = 4.31200742722;
  fMin_2[0][4] = 1.41889894009;
  fMax_2[0][4] = 6.92700910568;
  fMin_2[1][4] = 1.40067100525;
  fMax_2[1][4] = 6.54305315018;
  fMin_2[2][4] = 1.40067100525;
  fMax_2[2][4] = 6.92700910568;
  fMin_2[0][5] = -0.0281277615577;
  fMax_2[0][5] = 7.92215633392;
  fMin_2[1][5] = -0.30606725812;
  fMax_2[1][5] = 7.99080324173;
  fMin_2[2][5] = -0.30606725812;
  fMax_2[2][5] = 7.99080324173;
  fMin_2[0][6] = 5.76580905914;
  fMax_2[0][6] = 17.0834236145;
  fMin_2[1][6] = 5.81704616547;
  fMax_2[1][6] = 17.3935546875;
  fMin_2[2][6] = 5.76580905914;
  fMax_2[2][6] = 17.3935546875;
  fMin_2[0][7] = -8.73332977295;
  fMax_2[0][7] = -1.25026834011;
  fMin_2[1][7] = -8.63602256775;
  fMax_2[1][7] = -1.43286752701;
  fMin_2[2][7] = -8.73332977295;
  fMax_2[2][7] = -1.25026834011;
  fMin_2[0][8] = 0.645963847637;
  fMax_2[0][8] = 4.34242296219;
  fMin_2[1][8] = 0.598184108734;
  fMax_2[1][8] = 4.31493234634;
  fMin_2[2][8] = 0.598184108734;
  fMax_2[2][8] = 4.34242296219;
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::Transform_2( std::vector<double>& iv, int cls ) const {
  // Normalization transformation
  if ( cls < 0 || cls > 2 ) {
    if ( 2 > 1 )
      cls = 2;
    else
      cls = 2;
  }
  const int nVar = 9;

  // get indices of used variables

  // define the indices of the variables which are transformed by this transformation
  std::vector<int> indicesGet;
  std::vector<int> indicesPut;

  indicesGet.push_back( 0 );
  indicesGet.push_back( 1 );
  indicesGet.push_back( 2 );
  indicesGet.push_back( 3 );
  indicesGet.push_back( 4 );
  indicesGet.push_back( 5 );
  indicesGet.push_back( 6 );
  indicesGet.push_back( 7 );
  indicesGet.push_back( 8 );
  indicesPut.push_back( 0 );
  indicesPut.push_back( 1 );
  indicesPut.push_back( 2 );
  indicesPut.push_back( 3 );
  indicesPut.push_back( 4 );
  indicesPut.push_back( 5 );
  indicesPut.push_back( 6 );
  indicesPut.push_back( 7 );
  indicesPut.push_back( 8 );

  std::vector<double> dv( nVar );
  for ( int ivar = 0; ivar < nVar; ivar++ ) dv[ivar] = iv[indicesGet.at( ivar )];
  for ( int ivar = 0; ivar < 9; ivar++ ) {
    double offset             = fMin_2[cls][ivar];
    double scale              = 1.0 / ( fMax_2[cls][ivar] - fMin_2[cls][ivar] );
    iv[indicesPut.at( ivar )] = ( dv[ivar] - offset ) * scale * 2 - 1;
  }
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::InitTransform() {
  InitTransform_1();
  InitTransform_2();
}

//_______________________________________________________________________
void OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0::Transform( std::vector<double>& iv, int sigOrBgd ) const {
  Transform_1( iv, sigOrBgd );
  Transform_2( iv, sigOrBgd );
}