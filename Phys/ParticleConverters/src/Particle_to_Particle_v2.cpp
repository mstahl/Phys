/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/Transformer.h"

#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/ProtoParticle.h"
#include "Event/SOATrackConversion.h"
#include "Event/Track_SOA.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IProtoParticleFilter.h"
#include "Kernel/ParticleProperty.h"
#include "SOAExtensions/ZipUtils.h"
#include "TrackInterfaces/ITrackSelector.h"

using namespace LHCb::v2;

namespace TrackTag = LHCb::v2::Event::Tag;
namespace MuonTag  = LHCb::Pr::Muon::Tag;

using output_t = std::tuple<ChargedBasics, std::unique_ptr<AssignedMasses>, std::unique_ptr<RichPIDs>,
                            std::unique_ptr<LHCb::Pr::Muon::PIDs>, std::unique_ptr<ParticleIDs>,
                            std::unique_ptr<LHCb::v2::Event::Tracks>, std::unique_ptr<CombDLLs>>;

class Proto2ChargedBasic : public Gaudi::Functional::MultiTransformer<output_t( LHCb::ProtoParticles const& )> {

public:
  Proto2ChargedBasic( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, {KeyValue{"InputProtoParticles", LHCb::ProtoParticleLocation::Charged}},
                          {
                              KeyValue{"Particles", ""},
                              KeyValue{"Masses", ""},
                              KeyValue{"RichPIDs", ""},
                              KeyValue{"MuonPIDs", ""},
                              KeyValue{"ParticleIDs", ""},
                              KeyValue{"Tracks", ""},
                              KeyValue{"CombDLLs", ""},
                          } ) {}

  StatusCode initialize() override {
    auto sc = MultiTransformer::initialize();
    if ( sc.isFailure() ) { return sc; }

    const std::map<std::string, std::string> id_to_descriptor = {
        {"pion", "pi+"}, {"kaon", "K+"}, {"muon", "mu+"}, {"electron", "e+"}, {"proton", "p+"}};
    const auto result = id_to_descriptor.find( m_particleid.value() );
    if ( result == id_to_descriptor.end() ) { return Error( "Unknown ParticleID value: " + m_particleid.value() ); }
    auto descriptor = ( *result ).second;

    m_particlePropertySvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
    m_particle_prop       = m_particlePropertySvc->find( descriptor );
    if ( !m_particle_prop ) { return Error( "Could not find ParticleProperty for " + m_particleid.value() ); }
    m_antiparticle_prop = m_particle_prop->antiParticle();

    return sc;
  }

  output_t operator()( LHCb::ProtoParticles const& protos ) const override {
    auto zn             = Zipping::generateZipIdentifier();
    auto masses         = std::make_unique<AssignedMasses>( zn );
    auto rich_pids      = std::make_unique<RichPIDs>( zn );
    auto muon_pids      = std::make_unique<LHCb::Pr::Muon::PIDs>( zn );
    auto particle_ids   = std::make_unique<ParticleIDs>( zn );
    auto tracks         = std::make_unique<LHCb::v2::Event::Tracks>( LHCb::v2::Event::TrackType::Long, zn, nullptr );
    auto combdlls       = std::make_unique<CombDLLs>( zn );
    int  nparticles     = 0;
    int  nantiparticles = 0;

    for ( const auto* proto : protos ) {
      const auto* track = proto->track();
      // Sanity checks
      if ( track == nullptr ) {
        Warning( "Charged ProtoParticle with no Track found. Ignoring." ).ignore();
        continue;
      }
      if ( track->states().empty() ) {
        Warning( "Track has empty states. This is likely to be a bug" ).ignore();
        continue;
      }

      const auto selected_track = m_track_selector->accept( *track );
      m_npassed_track_filter += selected_track;
      if ( !selected_track ) { continue; }

      const auto selected_proto = m_proto_selector->isSatisfied( proto );
      m_npassed_proto_filter += selected_proto;
      if ( !selected_proto ) { continue; }

      // When filling PID, run additional checks and selections
      if ( m_check_pid.value() ) { check_pid( proto ); }

      // Get the (anti)particle property corresponding to the charge we have
      const LHCb::ParticleProperty* prop = nullptr;
      if ( proto->charge() == m_particle_prop->charge() ) {
        prop = m_particle_prop;
      } else if ( proto->charge() == m_antiparticle_prop->charge() ) {
        prop = m_antiparticle_prop;
      }

      { // fill particles

        masses->m_masses.emplace_back( prop->mass() );
        particle_ids->m_ids.emplace_back( prop->particleID().pid() );

        // protos
        auto mu_pid = muon_pids->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        if ( auto* muonpid = proto->muonPID() ) {
          mu_pid.field<MuonTag::Status>().set( muonpid->IsMuon() );
          mu_pid.field<MuonTag::Chi2Corr>().set( muonpid->chi2Corr() );
        } else {
          mu_pid.field<MuonTag::Status>().set( 0 ); // TODO make a correct invalid entry
          mu_pid.field<MuonTag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );
        }
        if ( auto* richpid = proto->richPID() ) {
          rich_pids->m_pid_codes.emplace_back( richpid->pidResultCode() );
        } else {
          rich_pids->m_pid_codes.emplace_back( rich_pids->invalid_entry() );
        }

        { // tracks
          auto const* track    = proto->track();
          auto        outTrack = tracks->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          LHCb::v2::Event::conversion::convert_track( LHCb::v2::Event::TrackType::Long, outTrack, track );
        }

        // combdlls
        combdlls->m_CombDLLps.emplace_back(
            proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLp, std::numeric_limits<float>::lowest() ) );
        combdlls->m_CombDLLes.emplace_back(
            proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLe, std::numeric_limits<float>::lowest() ) );
        combdlls->m_CombDLLpis.emplace_back(
            proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLpi, std::numeric_limits<float>::lowest() ) );
        combdlls->m_CombDLLks.emplace_back(
            proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLk, std::numeric_limits<float>::lowest() ) );
        combdlls->m_CombDLLmus.emplace_back(
            proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLmu, std::numeric_limits<float>::lowest() ) );
      }
      if ( prop == m_particle_prop ) {
        ++nparticles;
      } else if ( prop == m_antiparticle_prop ) {
        ++nantiparticles;
      }
    }

    m_nparticles += nparticles;
    m_nantiparticles += nantiparticles;

    // align sizes of the containers to given the instruction_set
    masses->reserve( masses->size() );
    combdlls->reserve( combdlls->size() );
    rich_pids->reserve( rich_pids->size() );
    particle_ids->reserve( particle_ids->size() );
    muon_pids->reserve( muon_pids->size() );
    // tracks are already fine

    return {LHCb::Pr::make_zip( std::as_const( *tracks ), std::as_const( *rich_pids ), std::as_const( *muon_pids ),
                                std::as_const( *masses ), std::as_const( *particle_ids ), std::as_const( *combdlls ) ),
            std::move( masses ),
            std::move( rich_pids ),
            std::move( muon_pids ),
            std::move( particle_ids ),
            std::move( tracks ),
            std::move( combdlls )};
  }

private:
  /// Print Warnings if the RICH or Muon PID objects are mising from the protoparticle
  void check_pid( const LHCb::ProtoParticle* proto ) const {
    // RICH links
    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::RichPIDStatus ) ) {
      const LHCb::RichPID* rpid = proto->richPID();
      if ( !rpid ) { Error( "ProtoParticle has RICH information but NULL RichPID SmartRef!" ).ignore(); }
    }
    // Muon links
    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus ) ) {
      const LHCb::MuonPID* mpid = proto->muonPID();
      if ( !mpid ) { Error( "ProtoParticle has MUON information but NULL MuonPID SmartRef!" ).ignore(); }
    }
  }

  LHCb::IParticlePropertySvc* m_particlePropertySvc = nullptr;

  /// Tool for filling the particle's kinematics from a given state
  ToolHandle<IParticle2State> m_particle_from_state_tool = {this, "Particle2StateTool", "Particle2State"};
  /// Tool to filter tracks associated to input protoparticles
  ToolHandle<ITrackSelector> m_track_selector = {this, "TrackSelector", "LoKi::Hybrid::TrackSelector"};
  /// Tool to filter protoparticles
  ToolHandle<IProtoParticleFilter> m_proto_selector = {this, "ProtoParticleFilter",
                                                       "LoKi::Hybrid::ProtoParticleFilter"};

  /// Particle property information for matter particles to be created
  const LHCb::ParticleProperty* m_particle_prop = nullptr;
  /// Particle property information for antimatter particles to be created
  const LHCb::ParticleProperty* m_antiparticle_prop = nullptr;

  Gaudi::Property<std::string> m_particleid{this, "ParticleID", "UNDEFINED",
                                            "Particle species hypothesis to apply to each created object."};
  Gaudi::Property<bool>        m_check_pid{
      this, "CheckPID", true,
      "If true, print Warnings if the Particle ID information on the ProtoParticle is missing."};

  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{this, "Nb created particles"};
  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{this, "Nb created anti-particles"};
  /// Number of protoparticles passing the protoparticle filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_proto_filter{this, "# passed ProtoParticle filter"};
  /// Number of tracks passing the track filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_track_filter{this, "# passed Track filter"};
};

DECLARE_COMPONENT( Proto2ChargedBasic )
