/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DAVINCIINIT_H
#define DAVINCIINIT_H 1

// from LHCbKernel
#include "Kernel/LbAppInit.h"

class IGenericTool;

/** @class DaVinciInit DaVinciInit.h
 *
 *  Algorithm to initialize DaVinci
 *
 *  @author Patrick Koppenburg
 *  @date   2009-03-02
 */
class DaVinciInit : public LbAppInit {

public:
  /// Standard constructor
  DaVinciInit( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DaVinciInit() {} ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  IGenericTool* m_memoryTool = nullptr; ///< Pointer to (private) memory histogram tool
  bool          m_print;                ///< Print event and run
};

#endif // DAVINCIINIT_H
