/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "Kernel/FilterParticlesBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FilterParticlesBase
//
// 2007-07-20 : Juan Palacios
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FilterParticlesBase::FilterParticlesBase( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IFilterParticles>( this );
}
//=============================================================================
bool FilterParticlesBase::operator()( const LHCb::Particle::ConstVector& parts ) const { return isSatisfied( parts ); }
//=============================================================================
bool FilterParticlesBase::isSatisfied( const LHCb::Particle::ConstVector& /*parts*/ ) const { return true; }
//=============================================================================
// Destructor
//=============================================================================
FilterParticlesBase::~FilterParticlesBase() {}

//=============================================================================
