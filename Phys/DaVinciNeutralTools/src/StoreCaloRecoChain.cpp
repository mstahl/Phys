/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "StoreCaloRecoChain.h"
#include "CaloUtils/CaloAlgUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StoreCaloRecoChain
//
//
// 2016-12-06 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( StoreCaloRecoChain )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
StoreCaloRecoChain::StoreCaloRecoChain( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ) {
  declareProperty( "UseStatusMask", m_mask = true ); //  true : only store the masked digits
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode StoreCaloRecoChain::initialize() {
  const auto sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  m_edata     = tool<ICaloDataProvider>( "CaloDataProvider", "EcalDataProvider", this );
  m_pdata     = tool<ICaloDataProvider>( "CaloDataProvider", "PrsDataProvider", this );
  counterStat = tool<ICounterLevel>( "CounterLevel" );
  return sc;
}

//========================================
StatusCode StoreCaloRecoChain::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // Input particles
  const LHCb::Particle::Range parts = particles();

  // set true if we have any input particles
  setFilterPassed( !parts.empty() );

  if ( !parts.empty() ) {
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " === Found " << parts.size() << " particles in the input container(s) " << inputLocations()
                << endmsg;

    //== create the containers for the reduced calo-reco chain
    m_clusters = getOrCreate<LHCb::CaloClusters, LHCb::CaloClusters>( "CaloReco/" + LHCb::CaloClusterLocation::Ecal );
    m_splitClusters =
        getOrCreate<LHCb::CaloClusters, LHCb::CaloClusters>( "CaloReco/" + LHCb::CaloClusterLocation::EcalSplit );
    m_edigits = getOrCreate<LHCb::CaloDigits, LHCb::CaloDigits>( "CaloReco/" + LHCb::CaloDigitLocation::Ecal );
    m_pdigits = getOrCreate<LHCb::CaloDigits, LHCb::CaloDigits>( "CaloReco/" + LHCb::CaloDigitLocation::Prs );
    m_sdigits = getOrCreate<LHCb::CaloDigits, LHCb::CaloDigits>( "CaloReco/" + LHCb::CaloDigitLocation::Spd );
    m_eadcs   = getOrCreate<LHCb::CaloAdcs, LHCb::CaloAdcs>( "CaloReco/" + LHCb::CaloAdcLocation::Ecal );
    m_padcs   = getOrCreate<LHCb::CaloAdcs, LHCb::CaloAdcs>( "CaloReco/" + LHCb::CaloAdcLocation::Prs );

    const auto _nClus  = m_clusters->size();
    const auto _nSClus = m_splitClusters->size();
    const auto _neDig  = m_edigits->size();
    const auto _neAdc  = m_eadcs->size();
    const auto _npDig  = m_pdigits->size();
    const auto _npAdc  = m_padcs->size();
    const auto _nsDig  = m_sdigits->size();

    unsigned int ncalo = 0;
    for ( const auto part : parts ) {
      // collect calo particles in the tree
      auto caloParts = getCaloTree( part );
      ncalo += caloParts.size();
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " == Particle : " << part->particleID().pid() << " => found " << caloParts.size()
                  << " neutral calo object in the  descendants" << endmsg;
      for ( const auto calo : caloParts ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "  = Calo Particle : " << calo->particleID().pid() << endmsg;
        m_counter.set();
        storeCalo( calo ); // clone & store the reco chain for each particle
        if ( msgLevel( MSG::VERBOSE ) ) m_counter.print();
      }
    }

    // Monitoring :
    if ( msgLevel( MSG::VERBOSE ) ) {
      info() << " ==== SUMMARY ==== " << endmsg;
      info() << " === Found " << parts.size() << " particles in the input container " << endmsg;
      info() << "  == Found " << ncalo << " calo objects in the particles descendants " << endmsg;
      info() << "   = Stored : " << m_clusters->size() - _nClus << " new clusters " << endmsg;
      info() << "   = Stored : " << m_splitClusters->size() - _nSClus << " new split-clusters " << endmsg;
      info() << "   = Stored : " << m_edigits->size() - _neDig << " new Ecal digits " << endmsg;
      info() << "   = Stored : " << m_eadcs->size() - _neAdc << " new Ecal ADCs   " << endmsg;
      info() << "   = Stored : " << m_pdigits->size() - _npDig << " new Prs digits  " << endmsg;
      info() << "   = Stored : " << m_padcs->size() - _npAdc << " new Prs ADCs    " << endmsg;
      info() << "   = Stored : " << m_sdigits->size() - _nsDig << " new Spd digits  " << endmsg;
    }
    m_nbCaloRecoChainsCounter += ncalo;
    if ( counterStat->isQuiet() ) {
      m_nClusters += m_clusters->size() - _nClus;
      m_nSplitClusters += m_splitClusters->size() - _nSClus;
      m_nEcalDigits += m_edigits->size() - _neDig;
      m_nPrsDigits += m_pdigits->size() - _npDig;
      m_nEcalAdcs += m_eadcs->size() - _neAdc;
      m_nPrsAdcs += m_padcs->size() - _npAdc;
      m_nSpdDigits += m_sdigits->size() - _nsDig;
    }
  }

  return StatusCode::SUCCESS;
}

//========================================
std::vector<const LHCb::Particle*> StoreCaloRecoChain::getCaloTree( const LHCb::Particle* p ) {
  std::vector<const LHCb::Particle*> tree;
  if ( p->isBasicParticle() && isPureNeutralCalo( p ) ) {
    tree.push_back( p );
  } else if ( !p->isBasicParticle() ) {
    for ( const auto& d : p->daughters() ) {
      for ( const auto dd : getCaloTree( d ) ) { tree.push_back( dd ); }
    }
  }
  return tree;
}

//========================================
bool StoreCaloRecoChain::storeCalo( const LHCb::Particle* p ) {
  if ( !p ) return false;
  if ( !p->isBasicParticle() || !isPureNeutralCalo( p ) ) return false;
  // get (neutral) protoP
  const auto* proto = p->proto();
  if ( !proto ) return false;
  // get CaloHypo ( (neutral)proto->hypo one-to-one correspondance)
  return ( !proto->calo().empty() ? storeHypo( proto->calo().front() ) : false );
}

//========================================
bool StoreCaloRecoChain::storeHypo( const LHCb::CaloHypo* h ) {
  if ( !h ) return false;
  // 1-  store the related extra-digits (Prs/Spd)
  const auto&  digits       = h->digits();
  const auto   nExtraDig    = digits.size();
  unsigned int nAddExtraDig = 0;
  for ( const auto& d : digits ) {
    if ( storeDigit( d ) ) { ++nAddExtraDig; }
  }
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << " - Stored :  " << nAddExtraDig << "/" << nExtraDig << " new extra Digits" << endmsg;

  // 2- in case of merged pi0 -  store the associated split-clusters :
  if ( h->hypothesis() == LHCb::CaloHypo::Pi0Merged ) {
    for ( const auto& split : h->hypos() ) {
      storeCluster( LHCb::CaloAlgUtils::ClusterFromHypo( split ), m_splitClusters );
    }
  }

  // 3- store the main cluster
  const bool storeClus = storeCluster( LHCb::CaloAlgUtils::ClusterFromHypo( h ), m_clusters );
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << " - Stored " << storeClus << " new CaloCluster" << endmsg;
  return storeClus;
}

//========================================
bool StoreCaloRecoChain::storeCluster( const LHCb::CaloCluster* c, LHCb::CaloClusters* clusters ) {
  if ( !c || !clusters ) { return false; }
  const auto id = c->seed();
  // check (by cellID) whether the corresponding cluster is already stored or not
  for ( const auto cc : *clusters ) {
    if ( cc->seed() == id ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "  -- calocluster has already been stored" << endmsg;
      return false;
    }
  }
  if ( clusters->object( c->key() ) ) return false;
  // if not clone & store the cluster
  auto* pCluster = new LHCb::CaloCluster( *c );
  clusters->insert( pCluster, c->key() );
  ++m_clusterCounter;
  // ... and store the related Ecal digits
  const auto&                         entries = c->entries();
  unsigned int                        nMaskedDigits( 0 ), nStoredDigits( 0 );
  std::vector<LHCb::CaloClusterEntry> clonedEntries;
  clonedEntries.reserve( entries.size() );
  for ( const auto& e : entries ) {
    if ( m_mask &&
         e.status().noneOf( {LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForPosition,
                             LHCb::CaloDigitStatus::Mask::UseForCovariance} ) )
      continue;
    ++nMaskedDigits;
    auto* newDig = storeDigit( e.digit() );
    if ( newDig ) {
      ++nStoredDigits;
      clonedEntries.emplace_back( e );
      clonedEntries.back().setDigit( newDig );
    }
  }
  pCluster->entries().clear();
  pCluster->setEntries( clonedEntries );
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << " - Stored " << nStoredDigits << " new CaloDigits (#Masked =" << nMaskedDigits
              << "/#Entries=" << pCluster->entries().size() << ")" << endmsg;

  return true;
}

const LHCb::CaloDigit* StoreCaloRecoChain::storeDigit( const LHCb::CaloDigit* d ) {
  if ( !d ) return nullptr;
  const auto id = d->cellID();
  // assign the right container
  LHCb::CaloDigits* digits = nullptr;
  if ( m_ecal( d ) ) {
    digits = m_edigits;
  } else if ( m_prs( d ) ) {
    digits = m_pdigits;
  } else if ( m_spd( d ) ) {
    digits = m_sdigits;
  } else {
    return nullptr;
  }
  // check (by cellID) whether the corresponding digit is already stored or not
  auto* pDigit = digits->object( id );
  if ( !pDigit ) pDigit = digits->object( d->key() );
  if ( pDigit ) return pDigit;
  pDigit = new LHCb::CaloDigit( *d );
  digits->insert( pDigit, d->key() ); // clone & store the digit
  if ( m_ecal( d ) ) ++m_nbEcalDigitsCounter;
  if ( m_prs( d ) ) ++m_nbPrsDigitsCounter;
  if ( m_spd( d ) ) ++m_nbSpdDigitsCounter;
  // ... and store the corresponding ADC
  storeADC( d );
  return pDigit;
}

bool StoreCaloRecoChain::storeADC( const LHCb::CaloDigit* d ) {
  if ( !d ) return false;
  if ( m_spd( d ) ) return false; // no need to store ADC for the binary SPD
  const auto id = d->cellID();
  // assign the right container
  LHCb::CaloAdcs* adcs = nullptr;
  if ( m_ecal( d ) ) {
    adcs = m_eadcs;
  } else if ( m_prs( d ) ) {
    adcs = m_padcs;
  } else {
    return false;
  }
  // == check (by cellID) whether the corresponding ADC is already stored or not
  if ( adcs->object( id ) ) return false;
  const int adc = ( m_ecal( d ) ) ? m_edata->adc( id, -255 ) : m_pdata->adc( id, 0 );
  if ( m_ecal( d ) ) ++m_nbEcalADCCounter;
  if ( m_prs( d ) ) ++m_nbPrsADCCounter;
  adcs->insert( new LHCb::CaloAdc( id, adc ), id );
  return true;
}
