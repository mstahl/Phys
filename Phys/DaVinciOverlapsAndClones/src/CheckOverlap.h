/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHECKOVERLAP_H
#define CHECKOVERLAP_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// From PhysEvent
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"

// from DaVinci
#include "Kernel/ICheckOverlap.h"

namespace LHCb {
  class IParticlePropertySvc;
}

/** @class CheckOverlap CheckOverlap.h
 *
 *  Tool to check if more than one LHCb::Particle in the particle tree have the
 *  same underying LHCb::ProtoParticle. Also checks for the same underlying
 *  LHCb::Track for charged basic particles.
 *
 *  @author Jose' Helder Lopes
 *  @date   28/06/2002
 */
class CheckOverlap : public extends<GaudiTool, ICheckOverlap> {
  using base = extends<GaudiTool, ICheckOverlap>;

public:
  /// Standard constructor
  CheckOverlap( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override {
    auto sc = base::initialize();
    m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
    return sc and m_ppSvc ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }

  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Argument: parts is a vector of pointers to particles.
  ///  Create an empty vector of pointers to protoparticles.
  ///  Call the real check method.
  bool foundOverlap( const LHCb::Particle::ConstVector& parts ) const override;

  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Arguments: particle1 up to particle4 are pointers to particles.
  ///  Create a ParticleVector and fill it with the input particles.
  ///  Create an empty vector of pointers to protoparticles.
  ///  Call the real check method.

  bool foundOverlap( const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle*,
                     const LHCb::Particle* ) const override;

  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Continue a previous check using the contents of the vector of pointers
  /// to protoparticles.(Most intended for internal use by the other methods).
  /// Arguments: parts is a vector of pointer to particles.
  ///            proto is a vector of pointers to protoparticles.
  //  Real checking method. Scans the tree headed by parts. Add each
  //  protoparticle to proto if it is a new one. Returns true otherwise.
  //  If called directly by the user, it will continue a previous check,
  //  not start a new one!
  //===========================================================================
  bool foundOverlap( const LHCb::Particle::ConstVector& parts, std::vector<const LHCb::ProtoParticle*>& proto ) const;

  /// Check for duplicate use of a protoparticle to produce decay tree of
  /// any particle in vector. Removes found particles from vector.
  StatusCode removeOverlap( LHCb::Particle::ConstVector& ) const override;

  /// Check for duplicate use of a protoparticle to produce decay tree of
  /// any particle in vector. Removes found particles from vector.
  StatusCode removeOverlap( LHCb::Particle::Vector& ) const;

private:
  bool addOrigins( const LHCb::Particle::ConstVector&, std::vector<const LHCb::ProtoParticle*>& ) const;

  /// Only look at protoparticles
  bool searchOverlap( std::vector<const LHCb::ProtoParticle*>& proto ) const;

private:
  /// Accessor for ParticlePropertySvc
  LHCb::IParticlePropertySvc* m_ppSvc;

}; // End of class header.

#endif // CHECKOVERLAP_H
