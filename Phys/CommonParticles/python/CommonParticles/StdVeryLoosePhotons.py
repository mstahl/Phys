#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## @file  CommonParticles/StdVeryLoosePhotons.py
#  configuration file for 'Standard Very Loose Photons'
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2019-11-27
# =============================================================================
"""
Configuration file for 'Standard Very Loose Photons'
"""
from __future__ import print_function
__author__ = "Olivier Deschamps odescham@in2p3.fr"
# =============================================================================
__all__ = ('StdVeryLoosePhotons', 'locations')
# =============================================================================
from Gaudi.Configuration import *
from Configurables import PhotonMakerAlg
from Configurables import PhotonMaker
from GaudiKernel.SystemOfUnits import MeV
from CommonParticles.Utils import *

## create the algorithm
algorithm = PhotonMakerAlg('StdVeryLoosePhotons', DecayDescriptor='Gamma')

# configure desktop&particle maker:
algorithm.addTool(PhotonMaker, name='PhotonMaker')
photon = algorithm.PhotonMaker
photon.PtCut = 75 * MeV

## configure Data-On-Demand service
locations = updateDoD(algorithm)

## finally: define the symbol
StdVeryLoosePhotons = algorithm

## ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)
    print(locationsDoD(locations))

# =============================================================================
# The END
# =============================================================================
