#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## @file  CommonParticles/StandardV0.py
#  configuration file for 'Standard V0'
#  @author Patrick Koppenburg
#  @date 2009-02-28
# =============================================================================
"""
Configuration file for 'Standard V0'
"""
from __future__ import print_function
__author__ = "Patrick Koppenburg"
# =============================================================================

_locations = {}

from CommonParticles.StdLooseKs import *
_locations.update(locations)

from CommonParticles.StdVeryLooseKs import *
_locations.update(locations)

from CommonParticles.StdLooseLambda import *
_locations.update(locations)

from CommonParticles.StdVeryLooseLambda import *
_locations.update(locations)

from CommonParticles.StdV0FromBrunel import *
_locations.update(locations)

# redefine the locations
locations = _locations

## ============================================================================
if '__main__' == __name__:

    print(__doc__)
    print(__author__)

    from CommonParticles.Utils import locationsDoD
    print(locationsDoD(locations))

# =============================================================================
# The END
# =============================================================================
