/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include <boost/container/small_vector.hpp>

#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Track_v1.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/IFactory.h"
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/GetDecay.h"
#include "Kernel/ICheckOverlap.h"
#include "Kernel/IDecodeSimpleDecayString.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "PrKernel/PrSelection.h"

#include "Functors/with_functor_maps.h"
#include "Functors/with_functors.h"

// legacy stuff
#include "Kernel/IDistanceCalculator.h"

template <typename particle, std::size_t N>
struct ParticleCombination;

namespace Combiner {
  namespace constants {
    static constexpr auto max_particlecontainer_size    = 500;   // TODO optimize
    static constexpr auto max_outcontainer_size         = 200;   // TODO optimize
    static constexpr auto max_combinationcontainer_size = 10000; // TODO optimize

    constexpr auto make_combiner_map = []( auto i ) {
      if constexpr ( i == 12 )
        return "Comb12Cut";
      else if constexpr ( i == 13 )
        return "Comb13Cut";
      else if constexpr ( i == 14 )
        return "Comb14Cut";
      else if constexpr ( i == 123 )
        return "Comb123Cut";
      else
        return "Comb1234Cut";
    };

    template <std::size_t i>
    constexpr static auto combiner_name = make_combiner_map( std::integral_constant<int, i>{} );

  } // namespace constants

  template <typename T, std::size_t N, std::size_t i>
  struct CombTypes {
    using Combination             = ParticleCombination<T, N>;
    constexpr static auto Headers = std::array{"CombKernel/ParticleCombination.h"};
    struct Cut {
      using Signature                    = bool( Combination const& ); // TODO mask_v instead of bool
      constexpr static auto PropertyName = constants::combiner_name<i>;
      constexpr static auto ExtraHeaders = Headers;
    };
  };

  template <typename T>
  struct MotherTypes {
    constexpr static auto Headers = std::array{"Event/Particle.h"};
    struct Cut {
      using Out                          = bool;
      using Signature                    = Out( T const& ); // TODO mask_v instead of bool
      constexpr static auto PropertyName = "MotherCut";
      constexpr static auto ExtraHeaders = Headers;
    };
  };

  template <typename T>
  struct DauTypes {
    constexpr static auto Headers = std::array{"Event/Particle.h"};
    struct Cut {
      using Out                          = bool;
      using Signature                    = Out( T const& );
      constexpr static auto PropertyName = "DaughterCuts";
      constexpr static auto ExtraHeaders = Headers;
    };
  };

  namespace types {
    using Vertex_t       = LHCb::Vertex;
    using Particle_t     = LHCb::Particle;
    using Particle_ptr_t = LHCb::Particle const*;
    using Track_t        = LHCb::Event::v1::Track;
    using Combination4_t = ParticleCombination<LHCb::Particle, 4>;
    using Combination3_t = ParticleCombination<LHCb::Particle, 3>;
    using Combination2_t = ParticleCombination<LHCb::Particle, 2>;

    using ParticleCut_t     = Functors::Functor<bool( Particle_t const& )>;
    using DaughterCuts_t    = std::map<int, ParticleCut_t const*>;
    using Combination4Cut_t = Functors::Functor<bool( Combination4_t const& )>;
    using Combination3Cut_t = Functors::Functor<bool( Combination3_t const& )>;
    using Combination2Cut_t = Functors::Functor<bool( Combination2_t const& )>;

    using ParticleContainer_t =
        boost::container::small_vector<Particle_t const*, constants::max_particlecontainer_size>;
    using InputParticleContainer_t = Particle_t::Range;
    using In_t                     = Gaudi::Functional::details::vector_of_const_<InputParticleContainer_t>;
    using OutVertices_t            = std::vector<Vertex_t>;
    using OutParticles_t           = std::vector<Particle_t>;
    using Out_t                    = std::tuple<OutParticles_t, OutVertices_t>;
    using functional_base_t        = Gaudi::Functional::MergingMultiTransformer<Out_t( In_t const& )>;
    using DauType                  = DauTypes<LHCb::Particle>;
    using MotherType               = MotherTypes<LHCb::Particle>;
    template <std::size_t N, std::size_t i>
    using CombType = CombTypes<LHCb::Particle, N, i>;

    using base_t = with_functors<with_functor_maps<functional_base_t, DauType::Cut>, MotherType::Cut>;

  } // namespace types

} // namespace Combiner

class NBodyParticleCombinerBase : public Combiner::types::base_t {
public:
  NBodyParticleCombinerBase( const std::string& name, ISvcLocator* pSvcLocator )
      : Combiner::types::base_t( name, pSvcLocator, {"Inputs", {}},
                                 {KeyValue{"OutputParticles", ""}, KeyValue{"OutputVertices", ""}} ) {}

  using base = Combiner::types::base_t;

  StatusCode initialize() override {
    auto sc = base::initialize();

    m_decoder.retrieve().ignore(); // need this?
    m_overlapTool.retrieve().ignore();
    m_vertexFitter.retrieve().ignore();
    m_dist_calc.retrieve().ignore();
    m_decays = Decays::decays( m_decaydescriptors, &*m_decoder );

    auto const& daughter_cuts = this->template getFunctorMap<Combiner::types::DauType::Cut>();

    std::map<int, LHCb::ParticleProperty const*> children_pids;
    // TODO only works for one decay now!
    for ( auto const& decay : m_decays ) {
      for ( auto const& child : decay.children() ) { children_pids.emplace( child.pid().pid(), child.pp() ); }
    }

    // put daughter cut into the map of <pid, daughter cuts>

    for ( auto const& child : children_pids ) {
      auto  child_pid = std::get<0>( child );
      auto* partProp  = std::get<1>( child );
      // try particle
      auto iter = std::find_if( daughter_cuts.begin(), daughter_cuts.end(), [&]( auto const& dau ) {
        return this->m_particlePropSvc->find( dau.first )->pid().pid() == child_pid;
      } );
      // try antiparticle
      if ( iter == daughter_cuts.end() ) {
        iter = std::find_if( daughter_cuts.begin(), daughter_cuts.end(), [&]( auto const& dau ) {
          return this->m_particlePropSvc->find( dau.first )->anti()->pid().pid() == child_pid;
        } );
      }
      if ( iter == daughter_cuts.end() ) {
        // if its not there, emplace nullptr to indicate default
        m_daughter_cuts.emplace( child_pid, nullptr );
      } else {
        m_daughter_cuts.emplace( child_pid, &iter->second );
      }
      m_daughters_passed.emplace( std::piecewise_construct, std::tuple{child_pid},
                                  std::tuple{this, "# passed " + partProp->name() + " cut"} );
    }
    // prepare the cuts
    return sc;
  }

  Combiner::types::DaughterCuts_t                               m_daughter_cuts;
  mutable std::map<int, Gaudi::Accumulators::BinomialCounter<>> m_daughters_passed{};
  mutable Gaudi::Accumulators::BinomialCounter<>                m_mother_passed{this, "# passed mother cut"};

  // legacy stuff
  ToolHandle<IDistanceCalculator> m_dist_calc{this, "DistanceCalculator", "LoKi::DistanceCalculator"};
  // get particle pids from names:
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropSvc{this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc"};

  // decay descriptors
  ToolHandle<IDecodeSimpleDecayString> m_decoder{this, "DecayDecoder", "DecodeSimpleDecayString"};
  ToolHandle<ICheckOverlap>            m_overlapTool{this, "OverlapTool", "CheckOverlap"};
  ToolHandle<IParticleCombiner>        m_vertexFitter{this, "ParticleCombiner", "LoKi::VertexFitter"};

  Gaudi::Property<std::vector<std::string>> m_decaydescriptors{
      this, "DecayDescriptors", {}, "Please provide a list of decay descriptors"};
  std::vector<Decays::Decay> m_decays;
};

template <std::size_t N>
class NBodyParticleCombiner;

template <>
class NBodyParticleCombiner<2>
    : public with_functors<NBodyParticleCombinerBase, Combiner::types::CombType<2, 12>::Cut> {
public:
  using base = with_functors<NBodyParticleCombinerBase, Combiner::types::CombType<2, 12>::Cut>;
  using base::base;

  NBodyParticleCombiner( const std::string& name, ISvcLocator* pSvcLocator ) : base( name, pSvcLocator ) {}

  Combiner::types::Out_t operator()( Combiner::types::In_t const& particles ) const override;

protected:
  mutable Gaudi::Accumulators::BinomialCounter<> m_combination12_passed{this, "# passed comb12 cut"};
};

template <>
class NBodyParticleCombiner<3>
    : public with_functors<NBodyParticleCombiner<2>, typename Combiner::types::CombType<3, 123>::Cut,
                           typename Combiner::types::CombType<2, 13>::Cut> {
public:
  using base = with_functors<NBodyParticleCombiner<2>, typename Combiner::types::CombType<3, 123>::Cut,
                             typename Combiner::types::CombType<2, 13>::Cut>;
  using base::base;

  Combiner::types::Out_t operator()( Combiner::types::In_t const& particles ) const override;

protected:
  mutable Gaudi::Accumulators::BinomialCounter<> m_combination123_passed{this, "# passed comb123 cut"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_combination13_passed{this, "# passed comb13 cut"};
};

template <>
class NBodyParticleCombiner<4>
    : public with_functors<NBodyParticleCombiner<3>, typename Combiner::types::CombType<4, 1234>::Cut,
                           typename Combiner::types::CombType<2, 14>::Cut> {
public:
  using base = with_functors<NBodyParticleCombiner<3>, typename Combiner::types::CombType<4, 1234>::Cut,
                             typename Combiner::types::CombType<2, 14>::Cut>;
  using base::base;

  Combiner::types::Out_t operator()( Combiner::types::In_t const& particles ) const override;

protected:
  mutable Gaudi::Accumulators::BinomialCounter<> m_combination1234_passed{this, "# passed comb1234 cut"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_combination14_passed{this, "# passed comb14 cut"};
};
