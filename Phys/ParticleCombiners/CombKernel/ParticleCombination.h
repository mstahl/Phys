/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "ParticleCombiner.h"
#include <array>
#include <functional>
#include <limits>

namespace Combination::details {
  using fp = float;
}

template <typename Particle, std::size_t N>
struct ParticleCombination {

  using child_array = std::array<Particle const*, N>;
  child_array m_children{};

  NBodyParticleCombinerBase const* m_legacy_accessor;
  ParticleCombination( NBodyParticleCombinerBase const* legacy_accessor ) : m_legacy_accessor( legacy_accessor ) {}

  Combination::details::fp mom2() const {
    using Combination::details::fp;
    using std::sqrt;
    double px = std::numeric_limits<fp>::min();
    double py = std::numeric_limits<fp>::min();
    double pz = std::numeric_limits<fp>::min();
    for ( auto* i : m_children ) {
      px += i->momentum().px();
      py += i->momentum().py();
      pz += i->momentum().pz();
    }
    return px * px + py * py + pz * pz;
  }

  Combination::details::fp mass() const {
    using Combination::details::fp;
    using std::sqrt;
    double energy = std::numeric_limits<fp>::min();
    for ( auto* i : m_children ) { energy += sqrt( pow( i->p(), 2 ) + pow( i->measuredMass(), 2 ) ); }
    return sqrt( energy * energy - mom2() );
  }

  template <std::size_t I1, std::size_t I2, typename DistanceCalculator>
  auto doca( DistanceCalculator const& ) const {
    static_assert( I1 < N && I2 < N );
    double current_distance{};
    m_legacy_accessor->m_dist_calc->distance( m_children[I1], m_children[I2], current_distance ).ignore();
    return current_distance;
  }

  template <typename DistanceCalculator>
  bool maxdocacut( DistanceCalculator const&, Combination::details::fp thresh ) const {
    using Combination::details::fp;
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        double current_distance = std::numeric_limits<fp>::min();
        m_legacy_accessor->m_dist_calc->distance( *i1, *i2, current_distance );
        if ( current_distance > thresh ) { return false; };
      }
    }
    return true;
  }

  template <std::size_t I1, std::size_t I2, typename DistanceCalculator>
  auto docachi2( DistanceCalculator const& ) const {
    static_assert( I1 < N && I2 < N );
    double current_chi2{}, current_distance{};
    m_legacy_accessor->m_dist_calc->distance( m_children[I1], m_children[I2], current_distance, current_chi2 ).ignore();
    return current_chi2;
  }

  template <typename DistanceCalculator>
  Combination::details::fp maxdocachi2( DistanceCalculator const& ) const {
    using Combination::details::fp;
    double chi2 = std::numeric_limits<fp>::min();
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        double current_chi2     = std::numeric_limits<fp>::min();
        double current_distance = std::numeric_limits<fp>::min();
        m_legacy_accessor->m_dist_calc->distance( *i1, *i2, current_distance, current_chi2 );
        chi2 = std::max( chi2, current_chi2 );
      }
    }
    return chi2;
  }

  template <typename DistanceCalculator>
  bool maxdocachi2cut( DistanceCalculator const&, Combination::details::fp thresh ) const {
    using Combination::details::fp;
    for ( auto i1 = m_children.begin(); i1 != m_children.end(); ++i1 ) {
      for ( auto i2 = std::next( i1 ); i2 != m_children.end(); ++i2 ) {
        double current_chi2     = std::numeric_limits<fp>::min();
        double current_distance = std::numeric_limits<fp>::min();
        m_legacy_accessor->m_dist_calc->distance( *i1, *i2, current_distance, current_chi2 )
            .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
        if ( current_chi2 > thresh ) { return false; };
      }
    }
    return true;
  }
};

template <typename Particle, std::size_t N>
auto begin( ParticleCombination<Particle, N> const& combination ) {
  return combination.m_children.begin();
}

template <typename Particle, std::size_t N>
auto end( ParticleCombination<Particle, N> const& combination ) {
  return combination.m_children.end();
}
