/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ThOrCombiner.h"

#include "Event/Particle_v2.h"

using ThOrCombinerScalar__2ChargedBasics = ThOr::CombinerScalar<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;
using ThOrCombinerScalar__3ChargedBasics =
    ThOr::CombinerScalar<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;
using ThOrCombinerScalar__4ChargedBasics = ThOr::CombinerScalar<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics,
                                                                LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;

DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__2ChargedBasics, "ThOrCombinerScalar__2ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__3ChargedBasics, "ThOrCombinerScalar__3ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__4ChargedBasics, "ThOrCombinerScalar__4ChargedBasics" )