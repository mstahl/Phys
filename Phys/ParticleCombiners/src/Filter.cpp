/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/HeaderMapping.h"
#include "PrAlgorithms/PrFilter.h"
#include "PrKernel/PrSelection.h"

namespace LHCb {
  template <>
  struct header_map<LHCb::Particle> {
    constexpr static auto value = LHCb::string_array{"Event/Particle.h"};
  };

  struct VectorToSelectionParticle final
      : Gaudi::Functional::Transformer<::Pr::Selection<LHCb::Particle>( std::vector<LHCb::Particle> const& )> {

    VectorToSelectionParticle( const std::string& name, ISvcLocator* pSvc )
        : Transformer( name, pSvc, {KeyValue{"Input", {""}}}, {KeyValue{"Output", {""}}} ) {}

    ::Pr::Selection<LHCb::Particle> operator()( std::vector<LHCb::Particle> const& input ) const override {
      return ::Pr::Selection{input};
    }
  };

  DECLARE_COMPONENT( VectorToSelectionParticle )
} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( Pr::Filter<LHCb::v2::ChargedBasics>, "ChargedBasicsFilter" )

DECLARE_COMPONENT_WITH_ID( Pr::Filter<Pr::Selection<LHCb::Particle>>, "ParticleSelectionFilter" )