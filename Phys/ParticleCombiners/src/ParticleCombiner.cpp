/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <functional>

#include <boost/algorithm/string/join.hpp>

#include "CombKernel/ParticleCombination.h"
#include "CombKernel/ParticleCombiner.h"
#include "LoKi/Combiner.h"
#include "LoKi/ParticleProperties.h"
#include "PrKernel/PrSelection.h"
#include "TrackKernel/TrackStateVertex.h"
#include "TrackKernel/TrackVertexUtils.h"

/*
TODO features:
  1. Functors
  4. deduplicate children

TODO optimizations:
  1. the overlap tool asks about the several properties every time that are the same for entire containers (like
isBasicParticle)
  2. we make too many comparisons with the overlaptool, it would sometimes be better to call it with more than 2
particles
  2. the default combination cut is ALL, in which case selecting through entire containers just to get n* true is
useless.
  3. vectorize cuts/selection/fits
  4. check at what point in the loop we should invoke cuts (for instance: in 3 body combinations, upfront comb13 cut or
in the inner loop?)
  5. improve the vertex fit in docachi2
  6. if p2 and p3 are of the same type and the cut12/cut13 are the same, the selections need not be executed twice!
*/

DECLARE_COMPONENT_WITH_ID( NBodyParticleCombiner<2>, "TwoBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( NBodyParticleCombiner<3>, "ThreeBodyCombiner" )
DECLARE_COMPONENT_WITH_ID( NBodyParticleCombiner<4>, "FourBodyCombiner" )

using namespace Combiner::types;

namespace {

  auto prepare_daughter_cut = []( int pid, Combiner::types::ParticleCut_t const& cut, auto& ctr ) {
    return [dau_cut = cut.prepare(), _ctr = ctr.buffer(), pid]( auto const* p ) mutable {
      if ( not p ) return false; // TODO hopefully to be removed?
      if ( p->particleID().pid() != pid ) return false;
      bool passed = dau_cut( *p );
      _ctr += passed;
      return passed;
    };
  };

  auto prepare_mother_cut = []( Combiner::types::ParticleCut_t const& cut, auto& ctr ) {
    return [mother_cut = cut.prepare(), _ctr = ctr.buffer()]( auto const& p ) mutable {
      bool passed = mother_cut( p );
      _ctr += passed;
      return passed;
    };
  };

  auto prepare_combination_cut = []( auto& comb, auto const& cut, auto& ctr ) {
    return [comb_cut = cut.prepare(), _ctr = ctr.buffer(), &comb]( auto const*... ps ) mutable {
      comb.m_children = {ps...};
      bool passed     = comb_cut( comb );
      _ctr += passed;
      return passed;
    };
  };

  template <typename Pred_t>
  ParticleContainer_t select_particles( ParticleContainer_t const& pcontainer, Pred_t&& pred ) {
    ParticleContainer_t out{};
    // out.reserve( pcontainer.size() ); //need this only in case of normal vectors
    // TODO vectorize this pls, requires more soa like particle container
    std::copy_if( pcontainer.begin(), pcontainer.end(), std::back_inserter( out ), std::forward<Pred_t>( pred ) );
    return out;
  }

  auto select_daughters = []( auto const& particles, auto const& daughter_cuts, auto& cntrs ) {
    std::map<int, ParticleContainer_t> selected_daughters{};
    for ( auto const& daughter_cut : daughter_cuts ) {
      if ( daughter_cut.second == nullptr ) {
        selected_daughters.emplace( daughter_cut.first, select_particles( particles, [&]( Particle_ptr_t p ) {
                                      if ( not p ) return false; // TODO hopefully to be removed?
                                      bool passed = p->particleID().pid() == daughter_cut.first;
                                      cntrs[daughter_cut.first] += passed;
                                      return passed;
                                    } ) );
      } else {
        selected_daughters.emplace(
            daughter_cut.first,
            select_particles( particles, prepare_daughter_cut( daughter_cut.first, *daughter_cut.second,
                                                               cntrs[daughter_cut.first] ) ) );
      }
    }
    return selected_daughters;
  };

  auto fill_candidates = []( auto& candidates, auto const& decay, auto const& selected_daughters ) {
    const Decays::Decay::Items& items = decay.children();
    for ( auto i = 0u; i < candidates.size(); ++i ) {
      auto it_to_selected_daughter = selected_daughters.find( items[i].pid().pid() );
      if ( it_to_selected_daughter != selected_daughters.end() ) {
        candidates[i] = &it_to_selected_daughter->second;
      } else {
        throw std::runtime_error( "no entry in the daughter cut list for PID " +
                                  std::to_string( items[i].pid().pid() ) );
      }
    }
  };

  template <std::size_t n>
  using comb_t = std::array<Particle_ptr_t, n>;

  // check order if both particles are of same type
  template <bool same_type = false>
  bool check_order( Particle_ptr_t p1, Particle_ptr_t p2 ) {
    if constexpr ( not same_type ) {
      return true;
    } else {
      // if the pid is the same, we merged all and thus only accept 1 order to "deduplicate"
      auto const& p1mom = p1->momentum();
      auto const& p2mom = p2->momentum();
      if ( p1mom.perp2() < p2mom.perp2() ) {
        return true;
      } else if ( p2mom.perp2() < p1mom.perp2() ) {
        return false;
      } // RETURN
      // compare by E
      const double e1 = p1mom.E();
      const double e2 = p2mom.E();
      return e1 < e2;
    }
  }

  // transform a combination cut into a cut that only takes one particle as input (the other one is captured
  template <typename Cut, typename O>
  auto make_cut_predicate( Cut&& cut, O&& ordered, ICheckOverlap const* overlapCheck,
                           Combiner::types::Particle_ptr_t p1 ) { // a predicate for selection
    return [p1, &cut, ordered = std::forward<O>( ordered ), overlapCheck]( Particle_ptr_t p2 ) -> bool {
      return ordered( p1, p2 ) and not overlapCheck->foundOverlap( p1, p2 ) and cut( p1, p2 );
    };
  }

  // we compare pids from every particle in the container, to see whether we have to order in the combination
  //(see check_order)
  template <typename T, std::size_t n>
  auto compare_pids( std::array<T, n> const& container ) {
    auto                              get_pid = []( auto const& i ) { return i[0]->particleID().pid(); };
    std::array<bool, n*( n - 1 ) / 2> ret;
    int                               idx = 0;
    for ( auto i = 0u; i < n; ++i ) {
      for ( auto j = i + 1; j < n; ++j ) { ret[idx++] = get_pid( *container[i] ) == get_pid( *container[j] ); }
    }
    return ret;
  }

  template <typename Out_t, typename Candidates_t, typename cut12_t>
  Out_t combine_2( Candidates_t const& candidates, bool const pid_comparison, ICheckOverlap const* overlapChecker,
                   cut12_t&& cut12 ) {
    Out_t combinations;
    auto  is_ordered12 = pid_comparison ? check_order<true> : check_order<false>;

    for ( Particle_ptr_t p1 : *candidates[0] ) {
      for ( Particle_ptr_t p2 :
            select_particles( *candidates[1], make_cut_predicate( cut12, is_ordered12, overlapChecker, p1 ) ) ) {
        combinations.emplace_back( comb_t<2>{p1, p2} );
      }
    }
    return combinations;
  }

  template <typename Out_t, typename Candidates_t, typename cut12_t, typename cut13_t, typename cut123_t,
            typename pid_comparison_t>
  Out_t combine_3( Candidates_t const& candidates, pid_comparison_t const& pid_comparison,
                   ICheckOverlap const* overlapChecker, cut12_t&& cut12, cut13_t&& cut13, cut123_t&& cut123 ) {
    Out_t combinations;
    auto  is_ordered12 = pid_comparison[0] ? check_order<true> : check_order<false>;
    auto  is_ordered13 = pid_comparison[1] ? check_order<true> : check_order<false>;
    auto  is_ordered23 = pid_comparison[2] ? check_order<true> : check_order<false>;

    for ( Particle_ptr_t p1 : *( candidates[0] ) ) {
      auto p2s = select_particles( *( candidates[1] ), make_cut_predicate( cut12, is_ordered12, overlapChecker, p1 ) );
      if ( p2s.empty() ) continue;
      auto p3s = select_particles( *( candidates[2] ), make_cut_predicate( cut13, is_ordered13, overlapChecker, p1 ) );
      if ( p3s.empty() ) continue;
      for ( Particle_ptr_t p2 : p2s ) {
        for ( Particle_ptr_t p3 : p3s ) {
          if ( is_ordered23( p2, p3 ) and not overlapChecker->foundOverlap( p2, p3 ) and cut123( p1, p2, p3 ) ) {
            combinations.emplace_back( comb_t<3>{p1, p2, p3} );
          }
        }
      }
    }
    return combinations;
  }

  template <typename Out_t, typename Candidates_t, typename cut12_t, typename cut13_t, typename cut14_t,
            typename cut123_t, typename cut1234_t, typename pid_comparison_t>
  Out_t combine_4( Candidates_t const& candidates, pid_comparison_t const& pid_comparison,
                   ICheckOverlap const* overlapChecker, cut12_t&& cut12, cut13_t&& cut13, cut14_t&& cut14,
                   cut123_t&& cut123, cut1234_t&& cut1234 ) {
    Out_t combinations{};
    auto  is_ordered12 = pid_comparison[0] ? check_order<true> : check_order<false>;
    auto  is_ordered13 = pid_comparison[1] ? check_order<true> : check_order<false>;
    auto  is_ordered14 = pid_comparison[2] ? check_order<true> : check_order<false>;
    auto  is_ordered23 = pid_comparison[3] ? check_order<true> : check_order<false>;
    auto  is_ordered24 = pid_comparison[4] ? check_order<true> : check_order<false>;
    auto  is_ordered34 = pid_comparison[5] ? check_order<true> : check_order<false>;

    for ( Particle_ptr_t p1 : *( candidates[0] ) ) {
      auto p2s = select_particles( *( candidates[1] ), make_cut_predicate( cut12, is_ordered12, overlapChecker, p1 ) );
      if ( p2s.empty() ) continue;
      auto p3s = select_particles( *( candidates[2] ), make_cut_predicate( cut13, is_ordered13, overlapChecker, p1 ) );
      if ( p3s.empty() ) continue;
      auto p4s = select_particles( *( candidates[3] ), make_cut_predicate( cut14, is_ordered14, overlapChecker, p1 ) );
      if ( p4s.empty() ) continue;
      for ( Particle_ptr_t p2 : p2s ) {
        for ( Particle_ptr_t p3 : p3s ) {
          if ( is_ordered23( p2, p3 ) and cut123( p1, p2, p3 ) ) {
            for ( Particle_ptr_t p4 : p4s ) {
              if ( is_ordered24( p2, p4 ) and is_ordered34( p3, p4 ) and
                   not overlapChecker->foundOverlap( p2, p3, p4 ) and cut1234( p1, p2, p3, p4 ) ) {
                combinations.emplace_back( comb_t<4>{p1, p2, p3, p4} );
              }
            }
          }
        }
      }
    }
    return combinations;
  }

  template <typename Cut_t, typename Comb_t>
  void fit_and_append_mothers( Combiner::types::Out_t& ret, const LHCb::ParticleID& pid, const Comb_t& combinations,
                               const IParticleCombiner* combiner, Cut_t&& mother_cut ) {
    auto& [mothers, vertices] = ret;
    for ( auto const& combination : combinations ) {
      mothers.emplace_back( pid );
      vertices.emplace_back();
      auto& mother = mothers.back();
      auto& vertex = vertices.back();
      // use default particle combiner to create the composed particle
      // TODO remove this hack, and in general a better combiner
      LHCb::Particle::ConstVector c;
      c.reserve( combination.size() );
      std::transform( combination.begin(), combination.end(), std::back_inserter( c ),
                      []( auto const* p ) { return p; } );
      if ( ( combiner->combine( c, mother, vertex ) ).isFailure() or not mother_cut( mother ) ) {
        mothers.pop_back(); // TODO remove this ugly design, rather only build the mother if we mothercut passes
        vertices.pop_back();
      } else {
        // TODO relate bestpv // legacy: const LHCb::Particle* particle = this->markTree( &mother ) ;
      }
    }
  }

} // end of namespace

Combiner::types::Out_t NBodyParticleCombiner<2>::operator()( Combiner::types::In_t const& all_particles_soc ) const {

  // create the output type (particles and vertices)
  Out_t out;
  std::get<0>( out ).reserve( Combiner::constants::max_outcontainer_size );
  std::get<1>( out ).reserve( Combiner::constants::max_outcontainer_size );

  using namespace Combiner::types;
  constexpr int nbodies = 2;

  ParticleContainer_t particles{};
  auto                all_sizes = 0u;
  for ( auto const& container : all_particles_soc ) all_sizes += container.size();
  particles.reserve( all_sizes );

  for ( auto const& container : all_particles_soc )
    for ( Particle_ptr_t particle : container ) particles.push_back( particle );

  auto selected_daughters = select_daughters( particles, m_daughter_cuts, m_daughters_passed );

  Combination2_t two_combination{this}; // this not reinstantiated every time, only children are
                                        // changed
  auto cut12 = prepare_combination_cut( two_combination, this->getFunctor<Combiner::types::CombType<2, 12>::Cut>(),
                                        m_combination12_passed );

  std::array<ParticleContainer_t const*, nbodies> candidates;

  for ( auto const& decay : m_decays ) {
    fill_candidates( candidates, decay, selected_daughters );

    if ( std::any_of( candidates.begin(), candidates.end(), []( auto const* c ) { return c->empty(); } ) ) continue;

    auto same_pid = compare_pids( candidates );

    auto combinations =
        combine_2<boost::container::small_vector<comb_t<nbodies>, Combiner::constants::max_combinationcontainer_size>>(
            candidates, same_pid[0], m_overlapTool.get(), cut12 );
    fit_and_append_mothers(
        out, decay.mother().pid(), combinations, m_vertexFitter.get(),
        prepare_mother_cut( static_cast<base::base const*>( this )->getFunctor<Combiner::types::MotherType::Cut>(),
                            m_mother_passed ) );
  }

  return out;
}

Combiner::types::Out_t NBodyParticleCombiner<3>::operator()( Combiner::types::In_t const& all_particles_soc ) const {

  // create the output type (particles and vertices)
  Out_t out;
  std::get<0>( out ).reserve( Combiner::constants::max_outcontainer_size );
  std::get<1>( out ).reserve( Combiner::constants::max_outcontainer_size );

  using namespace Combiner::types;
  constexpr int nbodies = 3;

  ParticleContainer_t particles{};
  auto                all_sizes = 0u;
  for ( auto const& container : all_particles_soc ) all_sizes += container.size();
  particles.reserve( all_sizes );

  for ( Combiner::types::InputParticleContainer_t const& container : all_particles_soc )
    for ( Particle_ptr_t particle : container ) particles.push_back( particle );

  auto selected_daughters = select_daughters( particles, m_daughter_cuts, m_daughters_passed );

  Combination3_t three_combination{this}; // this not reinstantiated every time, only children are
                                          // changed
  Combination2_t two_combination{this};   // this not reinstantiated every time, only children are
                                          // changed
  auto cut12 = prepare_combination_cut(
      two_combination, static_cast<base::base const*>( this )->getFunctor<Combiner::types::CombType<2, 12>::Cut>(),
      m_combination12_passed );
  auto cut13  = prepare_combination_cut( two_combination, this->getFunctor<Combiner::types::CombType<2, 13>::Cut>(),
                                        m_combination13_passed );
  auto cut123 = prepare_combination_cut( three_combination, this->getFunctor<Combiner::types::CombType<3, 123>::Cut>(),
                                         m_combination123_passed );

  std::array<ParticleContainer_t const*, nbodies> candidates;

  for ( auto const& decay : m_decays ) {
    fill_candidates( candidates, decay, selected_daughters );

    if ( std::any_of( candidates.begin(), candidates.end(), []( auto const* c ) { return c->empty(); } ) ) continue;

    auto pid_comparison = compare_pids( candidates );

    auto combinations =
        combine_3<boost::container::small_vector<comb_t<nbodies>, Combiner::constants::max_combinationcontainer_size>>(
            candidates, pid_comparison, m_overlapTool.get(), cut12, cut13, cut123 );

    fit_and_append_mothers(
        out, decay.mother().pid(), combinations, m_vertexFitter.get(),
        prepare_mother_cut(
            static_cast<base::base::base const*>( this )->getFunctor<Combiner::types::MotherType::Cut>(),
            m_mother_passed ) );
  }

  return out;
}

Combiner::types::Out_t NBodyParticleCombiner<4>::operator()( Combiner::types::In_t const& all_particles_soc ) const {

  // create the output type (particles and vertices)
  Out_t out;
  std::get<0>( out ).reserve( Combiner::constants::max_outcontainer_size );
  std::get<1>( out ).reserve( Combiner::constants::max_outcontainer_size );

  using namespace Combiner::types;
  constexpr int nbodies = 4;

  ParticleContainer_t particles{};
  auto                all_sizes = 0u;
  for ( auto const& container : all_particles_soc ) all_sizes += container.size();
  particles.reserve( all_sizes );

  for ( auto const& container : all_particles_soc )
    for ( Particle_ptr_t particle : container ) particles.push_back( particle );

  auto selected_daughters = select_daughters( particles, m_daughter_cuts, m_daughters_passed );

  Combination4_t four_combination{this};  // this not reinstantiated every time, only children are
                                          // changed
  Combination3_t three_combination{this}; // this not reinstantiated every time, only children are
                                          // changed
  Combination2_t two_combination{this};   // this not reinstantiated every time, only children are
                                          // changed
  auto cut12 = prepare_combination_cut(
      two_combination,
      static_cast<base::base::base const*>( this )->getFunctor<Combiner::types::CombType<2, 12>::Cut>(),
      m_combination12_passed );
  auto cut13 = prepare_combination_cut(
      two_combination, static_cast<base::base const*>( this )->getFunctor<Combiner::types::CombType<2, 13>::Cut>(),
      m_combination13_passed );
  auto cut14  = prepare_combination_cut( two_combination, this->getFunctor<Combiner::types::CombType<2, 14>::Cut>(),
                                        m_combination14_passed );
  auto cut123 = prepare_combination_cut(
      three_combination, static_cast<base::base const*>( this )->getFunctor<Combiner::types::CombType<3, 123>::Cut>(),
      m_combination123_passed );
  auto cut1234 = prepare_combination_cut( four_combination, this->getFunctor<Combiner::types::CombType<4, 1234>::Cut>(),
                                          m_combination1234_passed );

  std::array<ParticleContainer_t const*, nbodies> candidates;

  for ( auto const& decay : m_decays ) {
    fill_candidates( candidates, decay, selected_daughters );

    if ( std::any_of( candidates.begin(), candidates.end(), []( auto const* c ) { return c->empty(); } ) ) continue;

    auto pid_comparison = compare_pids( candidates );

    auto combinations =
        combine_4<boost::container::small_vector<comb_t<nbodies>, Combiner::constants::max_combinationcontainer_size>>(
            candidates, pid_comparison, m_overlapTool.get(), cut12, cut13, cut14, cut123, cut1234 );

    fit_and_append_mothers(
        out, decay.mother().pid(), combinations, m_vertexFitter.get(),
        prepare_mother_cut(
            static_cast<base::base::base::base const*>( this )->getFunctor<Combiner::types::MotherType::Cut>(),
            m_mother_passed ) );
  }
  return out;
}
