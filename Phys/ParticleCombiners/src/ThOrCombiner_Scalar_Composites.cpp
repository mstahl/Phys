/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ThOrCombiner.h"

#include "Event/Particle_v2.h"

using ThOrCombinerScalar__CompositesChargedBasics = ThOr::CombinerScalar<LHCb::v2::Composites, LHCb::v2::ChargedBasics>;
using ThOrCombinerScalar__Composites2ChargedBasics =
    ThOr::CombinerScalar<LHCb::v2::Composites, LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;
using ThOrCombinerScalar__Composites3ChargedBasics =
    ThOr::CombinerScalar<LHCb::v2::Composites, LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics,
                         LHCb::v2::ChargedBasics>;

DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__CompositesChargedBasics, "ThOrCombinerScalar__CompositesChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__Composites2ChargedBasics,
                           "ThOrCombinerScalar__Composites2ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerScalar__Composites3ChargedBasics,
                           "ThOrCombinerScalar__Composites3ChargedBasics" )
