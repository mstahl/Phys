/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ThOrCombiner.h"

#include "Event/Particle_v2.h"

using ThOrCombinerSSE__2ChargedBasics = ThOr::CombinerSSE<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;
using ThOrCombinerSSE__3ChargedBasics =
    ThOr::CombinerSSE<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;
using ThOrCombinerSSE__4ChargedBasics = ThOr::CombinerSSE<LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics,
                                                          LHCb::v2::ChargedBasics, LHCb::v2::ChargedBasics>;

DECLARE_COMPONENT_WITH_ID( ThOrCombinerSSE__2ChargedBasics, "ThOrCombinerSSE__2ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerSSE__3ChargedBasics, "ThOrCombinerSSE__3ChargedBasics" )
DECLARE_COMPONENT_WITH_ID( ThOrCombinerSSE__4ChargedBasics, "ThOrCombinerSSE__4ChargedBasics" )