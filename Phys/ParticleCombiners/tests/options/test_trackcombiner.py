from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from builtins import range
from Configurables import (
    ApplicationMgr, LHCb__Converters__Track__v1__fromLHCbTrackVector, LHCbApp,
    MakeSelection__Track_v1, TrackCombiner, UnpackTrack)
import GaudiPython as GP
from PRConfig import TestFileDB


def remove_davinci_hybrid_modules():
    """Remove Hybrid modules that require a DVAlgorithm.

    Some modules import functors that depend on the DVAlgorithm context being
    available. The LoKi::Hybrid::Tool tool loads these modules by default, but
    we don't want nor need them, so remove them from the default list.
    """
    # List of modules we will delete from the default list
    dv_modules = ["LoKiPhys.decorators", "LoKiArrayFunctors.decorators"]

    from Configurables import LoKi__Hybrid__Tool
    # Fetch the public tool instance used by the TrackCombiner algorithm
    factory = LoKi__Hybrid__Tool("HybridFactory")
    for m in dv_modules:
        del factory.Modules[factory.Modules.index(m)]


# Unpack pRec/Track/Best
unpacker = UnpackTrack()

# Convert the KeyedContainer to a std::vector
converter = LHCb__Converters__Track__v1__fromLHCbTrackVector(
    "ConvertTrackVector")
converter.InputTracksName = unpacker.getDefaultProperty("OutputName")
converter.OutputTracksName = str(converter.InputTracksName) + "Vector"

# Apply a dummy selection (selects all tracks)
selection = MakeSelection__Track_v1("SelectionMaker")
selection.Input = converter.OutputTracksName
selection.Output = str(selection.Input) + "Selected"

combiner = TrackCombiner("TwoBodyCombiner")
combiner.NBodies = 2
combiner.Preamble = [
    "from GaudiKernel.SystemOfUnits import MeV, GeV",
    "from LoKiPhys.decorators import RV_MASS, RV_PT, RV_TrNUM, VCHI2PDOF",
    "from LoKiTrack.decorators import TrLONG"
]
# Form high-pT phi(1020) candidates
combiner.CombinationCut = "(RV_TrNUM(TrLONG) == 2) & (RV_PT > 3*GeV) & (RV_MASS('K-', 'K+') < 1040*MeV)"
combiner.VertexCut = "(VCHI2PDOF < 8)"
combiner.InputTracks = selection.Output
combiner.OutputVertices = 'Rec/Vertex/Phi'
remove_davinci_hybrid_modules()

# Define the sequence
ApplicationMgr().TopAlg = [unpacker, converter, selection, combiner]

app = LHCbApp()
app.EvtMax = 100

# Pick a file that has the reconstruction available
f = TestFileDB.test_file_db[
    "2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed"]
f.setqualifiers(configurable=app)
f.run(configurable=app, withDB=True)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# FIXME
# without this calling `vertex.chi2PerDoF()` causes a warning on stderr
# input_line_495:10:7: warning: ignoring return value of function declared with 'nodiscard' attribute [-Wunused-result]
#       ((const LHCb::VertexBase*)obj)->chi2PerDoF();
# This is due to a bug in ROOT, now reported. Whenever it's fixed, this should be removed
import cppyy
cppyy.gbl.gInterpreter.Declare(
    '''#pragma clang diagnostic ignored "-Wunused-result"''')

event = 0

while event < app.EvtMax:
    anydata = TES[str(combiner.OutputVertices)]
    if not anydata:
        event += 1
        appMgr.run(1)
        continue

    container = anydata.getData()
    for i in range(container.size()):
        vertex = container.at(i)
        tracks = vertex.tracks()
        assert len(tracks) == 2, (i, vertex, len(tracks))

        chi2pdof = vertex.chi2PerDoF()
        pt = sum(t.pT().value() for t in tracks)
        all_long = all(t.type() == t.Long for t in tracks)

        # Check that our cuts have been applied
        assert chi2pdof < 8, (i, vertex, chi2pdof)
        assert pt > 3000, (i, vertex, pt)

    event += 1
    appMgr.run(1)
