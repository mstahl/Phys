/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// =============================================================================
// STD& STL
// =============================================================================
#include <map>
#include <memory>
#include <string>
// =============================================================================
// from Gaudi
// =============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Chrono.h"
#include "Kernel/IDecayTreeFit.h"
#include "Kernel/IParticleDictTool.h" // Interface used by this tool

#include "Kernel/Escape.h"
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ISubstitutePID.h"
#include "Kernel/PVSentry.h"
#include "Kernel/ParticleProperty.h"

#include "LoKi/AuxDTFBase.h"
#include "LoKi/GetTools.h"
#include "LoKi/PhysTypes.h"

// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class DTFDict
     *  Implementation of a Particle->Particle Transformation on
     *  a dictionary of Functors
     *  The particle for which the DictOfFunctors should be evaluated
     *  is transformed by applying the DecayTreeFitter
     *
     *  @author Sebastian Neubert
     *  @date   2013-07-10
     */
    class DTFDict final : public GaudiTool, virtual public IParticleDictTool {
    public:
      // ======================================================================
      /// Standard constructor
      DTFDict( const std::string& type, const std::string& name, const IInterface* parent )
          : GaudiTool( type, name, parent ) {
        declareInterface<IParticleDictTool>( this );

        declareProperty( "Source", m_sourcename, "Type/Name for Dictionary Tool" );

        declareProperty( "daughtersToConstrain", m_constraints, "list of particles to contrain" );

        declareProperty( "constrainToOriginVertex", m_usePV, "flag to switch on PV contraint" );

        declareProperty( "Substitutions", m_map, "PID-substitutions :  { 'decay-component' : 'new-pid' }" );
      }

      // ======================================================================
      StatusCode finalize() override {
        m_dtf.reset();
        return GaudiTool::finalize();
      }
      // ======================================================================
      StatusCode initialize() override {
        StatusCode sc = GaudiTool::initialize();
        if ( sc.isFailure() ) { return sc; } // RETURN
        // acquire the DictTool containing all the functors
        // that should be evaluated on the transformed particle
        m_source = tool<IParticleDictTool>( m_sourcename, this );
        if ( !m_source ) { return this->Error( "Unable to find the source DictTool " + m_sourcename, sc ); }

        if ( !m_map.empty() ) {
          m_substitute = tool<ISubstitutePID>( "SubstitutePIDTool", this );
          sc           = m_substitute->decodeCode( m_map );
        }

        m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

        /// get IDVAlgorithm  and ensure that the tool is private tool
        auto dv = Gaudi::Utils::getIDVAlgorithm( contextSvc(), this );
        if ( !dv ) { return Error( "Can't locate proper IDVAlgorithm!" ); }

        m_dtf = std::make_unique<LoKi::AuxDTFBase>( dv, m_constraints );

        debug() << "Successfully initialized DTFDict" << endmsg;

        return sc;
      }

      // ======================================================================

      StatusCode fill( const LHCb::Particle* p, IParticleDictTool::DICT& dict ) const override {

        Chrono chrono( chronoSvc(), name() + "::fill()" );
        if ( !p ) { return Error( "LHCb::Particle* points to NULL, dictionary will not be filled" ); }
        // 2. get primary vertex (if required)
        const LHCb::VertexBase* vertex = nullptr;
        if ( m_usePV ) {
          vertex = m_dtf->bestVertex( p );
          if ( !vertex ) { Warning( "``Best vertex'' points to null, constraits will be disabled!" ).ignore(); }
        }
        //

        LHCb::DecayTree tree( *p );

        // substitute
        if ( m_substitute && !substitute( tree ) ) return StatusCode::FAILURE;

        m_dtf->applyConstraints(); // you need to do this for each individual fit!
        // printConstraints(std::cout);

        auto            myfitter  = m_dtf->fitter(); // from AuxDTFBase
        auto            sc        = myfitter->fit( tree.head(), vertex );
        LHCb::Particle* prefitted = nullptr;
        if ( sc.isFailure() ) {
          // Hand an empty pointer to source and let it decide how to fill the
          // dictionary. This is needed because have too little information
          // on the structure of the dict at this point.
          this->Warning( "Error from IDecayTreeFit", sc ).ignore();
          dict.insert( "DTF_CHI2", -1 );
          dict.insert( "DTF_NDOF", 0 );
          /// request the dictionary from the source DictTool
          m_source->fill( prefitted, dict ).ignore();
        } else // everything went well
        {
          dict.insert( "DTF_CHI2", myfitter->chi2() );
          dict.insert( "DTF_NDOF", myfitter->nDoF() );
          auto tree = myfitter->fittedTree();
          // evaluate the dict of fucntors on the head of the refitted tree
          prefitted = tree.head();
          // lock the relation table Particle -> Primary Vertex
          // need to call getDesktop, since algorithm doesn't inherit from IDVAlgorithm
          // 3rd argument ensures that relations of daughters are locked as well
          DaVinci::PVSentry sentry( m_dtf->getDesktop(), prefitted, true );
          /// request the dictionary from the source DictTool
          m_source->fill( prefitted, dict ).ignore();
        } // sentry needs to go out of scope, so that destructor is called
        // tree.release();
        return StatusCode::SUCCESS;
      }

      std::string getName( const int id ) const {
        const auto* prop = m_ppSvc->find( LHCb::ParticleID( id ) );
        Assert( prop, "DTFDict: Unknown PID" );
        return Decays::escape( prop->name() );
      }

      StatusCode substitute( LHCb::DecayTree& tree ) const {

        if ( msgLevel( MSG::DEBUG ) ) { debug() << "Calling substitute" << endmsg; }

        const auto substituted = m_substitute->substitute( tree.head() );

        // debugging
        if ( msgLevel( MSG::VERBOSE ) || !substituted ) {
          const auto mp = tree.cloneMap();

          for ( const auto& i : mp ) {

            if ( i.first->particleID().pid() == i.second->particleID().pid() ) {
              info() << "A " << getName( i.first->particleID().pid() ) << " remains unchanged" << endmsg;
            } else {
              info() << "A " << getName( i.first->particleID().pid() ) << " is substituted by a "
                     << getName( i.second->particleID().pid() ) << endmsg;
            }
          }
        }

        if ( !substituted ) { return Error( "No particles have been substituted. Check your substitution options." ); }

        return StatusCode::SUCCESS;
      }

      // ======================================================================

    protected:
      IParticleDictTool* m_source = nullptr;
      std::string        m_sourcename;

      std::vector<std::string> m_constraints;

      bool m_usePV = true;

      ISubstitutePID::SubstitutionMap m_map; // mapping : { 'decay-component' : "new-pid" }

      ISubstitutePID* m_substitute = nullptr;

      LHCb::IParticlePropertySvc* m_ppSvc = nullptr;

      std::unique_ptr<LoKi::AuxDTFBase> m_dtf{nullptr};
    };
    // ========================================================================
  } // namespace Hybrid
  // ==========================================================================
} // namespace LoKi
// ============================================================================
DECLARE_COMPONENT( LoKi::Hybrid::DTFDict )
// ============================================================================
//                                                                      The END
// ============================================================================
