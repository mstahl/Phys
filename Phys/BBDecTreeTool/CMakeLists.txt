###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: BBDecTreeTool
################################################################################
gaudi_subdir(BBDecTreeTool)

gaudi_depends_on_subdirs(GaudiAlg
                         GaudiKernel
                         Phys/DaVinciKernel
                         Phys/LoKi
                         Phys/LoKiArrayFunctors)

find_package(Boost COMPONENTS system filesystem)

find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(BBDecTreeTool
                 src/*.cpp
                 INCLUDE_DIRS Boost Phys/LoKi
                 LINK_LIBRARIES Boost GaudiAlgLib GaudiKernel DaVinciKernelLib LoKiArrayFunctorsLib)

gaudi_install_python_modules()

