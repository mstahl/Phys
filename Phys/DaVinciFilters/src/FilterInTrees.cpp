/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/PhysExtract.h"
// ============================================================================
// local
// ============================================================================
#include "FilterDesktop.h"
// ============================================================================
/** @class FilterInTrees
 *  Simple variation of FilterDesktop algorithm, that allows to
 *  select/filter particles in decay trees, e.g. daughter muons
 *  @see FilterDesktop
 *  @author Vanya BELYAEV   Ivan.Belyaev@itep.ru
 *  @date 2010-04-05
 */
class FilterInTrees : public FilterDesktop {
public:
  // ==========================================================================
  /** standard constructor
   *  @see DVAlgorithm
   *  @see GaudiTupleAlg
   *  @see GaudiHistoAlg
   *  @see GaudiAlgorithm
   *  @see Algorithm
   *  @param name the algorithm instance name
   *  @param pSvc pointer to Service Locator
   */
  FilterInTrees                  // standard contructor
      ( const std::string& name, // the algorithm instance name
        ISvcLocator*       pSvc )      // pointer to Service Locator
      : FilterDesktop( name, pSvc ) {}
  // ==========================================================================
  /** the major method for filter input particles
   *  @param input    (INPUT) the input  container of particles
   *  @param filtered (OUPUT) the output container of particles
   *  @return Status code
   */
  StatusCode filter( const LHCb::Particle::ConstVector& input, LHCb::Particle::ConstVector& filtered ) override {
    //
    LoKi::Extract::particles( input.begin(), input.end(), std::back_inserter( filtered ), predicate() );
    //
    // mark & store filtered particles in DVAlgorithm local container
    //
    markParticles( filtered );
    //
    return StatusCode::SUCCESS;
  }
  // ==========================================================================
};
// ============================================================================
/// the factory
DECLARE_COMPONENT( FilterInTrees )
// ============================================================================
// The END
// ============================================================================
