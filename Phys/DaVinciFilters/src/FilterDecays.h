/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/ToStream.h"

#include "LoKi/Decays.h"
#include "LoKi/IDecay.h"

#include "FilterDesktop.h"

/** @class FilterDecays
 *  Simple algorithm to filter certain decays or decay components
 *
 *  The algorithm selects certain decays or decay components
 *
 *  @code
 *
 *  from Configurables import FilterDecays
 *
 *  myAlg1 = FilterDecays (
 *     ...                                                 ,
 *     Code          = " [B_s0 -> J/psi(1S) phi(1020)]CC"  ,
 *     ...                                                 ,
 *     )
 *
 *  myAlg2 = FilterDecays (
 *     ...                                                 ,
 *     Code          = " [B_s0 -> ^J/psi(1S) phi(1020)]CC" ,
 *     ...                                                 ,
 *     )
 *
 *  @endcode
 *
 *  @author Vanya BELYAEV   Ivan.Belyaev@cern.ch
 *  @date 2011-05-13
 */
class FilterDecays : public FilterDesktop {
public:
  /// finalize  the algorithm
  StatusCode finalize() override;
  /** standard constructor
   *  @see DVAlgorithm
   *  @see GaudiTupleAlg
   *  @see GaudiHistoAlg
   *  @see GaudiAlgorithm
   *  @see Algorithm
   *  @param name the algorithm instance name
   *  @param pSvc pointer to Service Locator
   */
  FilterDecays                   //        standard constructor
      ( const std::string& name, // the algorithm instance name
        ISvcLocator*       pSvc );     //  pointer to Service Locator

  /** the major method for filter input particles
   *  @param input    (INPUT) the input  container of particles
   *  @param filtered (OUPUT) the output container of particles
   *  @return Status code
   */
  StatusCode filter( const LHCb::Particle::ConstVector& input, LHCb::Particle::ConstVector& filtered ) override;
  /// decode the code
  StatusCode decodeCode() override;

protected:
  const Decays::IDecay::Finder& finder() const { return m_finder; }

private:
  /// the decay finder
  Decays::IDecay::Finder m_finder; //                    the decay finder

  mutable Gaudi::Accumulators::StatCounter<> m_nDecays{this, "#decays"};
};
