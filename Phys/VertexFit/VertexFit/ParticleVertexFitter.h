/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "SelKernel/ParticleAccessors.h"
#include "SelKernel/State4.h"
#include "SelKernel/Utilities.h"
#include "SelTools/Utilities.h"

#include "Event/Particle_v2.h"
#include "Event/Track_SOA.h"
#include "Kernel/IParticlePropertySvc.h"
#include "LHCbMath/MatVec.h"

#include "Gaudi/Algorithm.h"

namespace Sel::Fitters {
  /** @class Sel::Fitters::ParticleVertex
   *
   *  This is a templated adaptation of ParticleVertexFitter.cpp by Wouter
   *  Hulsbergen that aims to be compatible with explicit vectorisation and Run
   *  3 event model types.
   *  See: https://surfdrive.surf.nl/files/index.php/s/Z0mVdKL9PFfnXnR
   */
  using LHCb::v2::Event::StateLocation;
  using Sel::Utils::has_tracklike_API;
  struct ParticleVertex {
    ParticleVertex( Gaudi::Algorithm* owning_algorithm )
        : m_pp_svc{owning_algorithm, "ParticlePropertyService", "LHCb::ParticlePropertySvc"} {}

    /** Interface for use by particle combiner algorithms.
     */
    template <typename mask_v, typename int_v, typename... child_ts>
    mask_v fitComposite( LHCb::v2::Composites& storage, mask_v const& mask, int_v const& new_particle_ids,
                         Sel::ParticleCombination<child_ts...> children ) const {
      return fitComposite( storage, mask, new_particle_ids, children, std::index_sequence_for<child_ts...>{} );
    }

  private:
    template <std::size_t N, typename T>
    using VecN = LHCb::LinAlg::Vec<T, N>;

    template <std::size_t N, typename T>
    using SymNxN = LHCb::LinAlg::MatSym<T, N>;

    template <std::size_t N, std::size_t M, typename T>
    using Matrix = LHCb::LinAlg::Mat<T, N, M>;

    template <std::size_t N, typename T>
    using MatrixNxN = LHCb::LinAlg::Mat<T, N>;

    /* TODO look at merging this with the stuff in State4.h
     */
    template <typename F>
    struct MiniState {
      SymNxN<5, F> cov{};
      auto&        x() { return m_x; }
      auto const&  x() const { return m_x; }
      auto&        y() { return m_y; }
      auto const&  y() const { return m_y; }
      auto&        z() { return m_z; }
      auto const&  z() const { return m_z; }
      auto&        qOverP() { return m_qOverP; }
      auto const&  qOverP() const { return m_qOverP; }
      auto&        tx() { return m_tx; }
      auto const&  tx() const { return m_tx; }
      auto&        ty() { return m_ty; }
      auto const&  ty() const { return m_ty; }

      template <typename T, std::enable_if_t<Sel::Utils::is_state_like<T>, int> = 0>
      MiniState( T const& other )
          : m_x{other.x()}
          , m_y{other.y()}
          , m_tx{other.tx()}
          , m_ty{other.ty()}
          , m_z{other.z()}
          , m_qOverP{other.qOverP()} {
        auto const& other_cov = other.covariance();
        LHCb::Utils::unwind<0, 5>( [this, &other_cov]( auto i ) {
          LHCb::Utils::unwind<i, 5>( [this, i, &other_cov]( auto j ) { cov( i, j ) = other_cov( i, j ); } );
        } );
      }

      void linearTransportTo( F const& new_z ) {
        const auto dz  = new_z - m_z;
        const auto dz2 = dz * dz;
        m_x += dz * m_tx;
        m_y += dz * m_ty;
        cov( 0, 0 ) += dz2 * cov( 2, 2 ) + 2 * dz * cov( 2, 0 );
        cov( 1, 0 ) += dz2 * cov( 3, 2 ) + dz * ( cov( 3, 0 ) + cov( 2, 1 ) );
        cov( 2, 0 ) += dz * cov( 2, 2 );
        cov( 3, 0 ) += dz * cov( 3, 2 );
        cov( 4, 0 ) += dz * cov( 4, 2 );
        cov( 1, 1 ) += dz2 * cov( 3, 3 ) + 2 * dz * cov( 3, 1 );
        cov( 2, 1 ) += dz * cov( 3, 2 );
        cov( 3, 1 ) += dz * cov( 3, 3 );
        cov( 4, 1 ) += dz * cov( 4, 3 );
        m_z = new_z;
      }

      SymNxN<2, F>    covXX() const { return cov.template sub<SymNxN<2, F>, 0, 0>(); }
      MatrixNxN<2, F> covXT() const { return cov.template sub<MatrixNxN<2, F>, 2, 0>(); }

    private:
      F m_x{}, m_y{}, m_tx{}, m_ty{}, m_z{}, m_qOverP{};
    };

    // Different daughter types
    // TODO look at using the meta enum thing
    enum struct ChildType : unsigned char {
      TrackWithVelo = 0,
      TrackWithoutVelo,
      Composite,
      Resonance,
      Other,
      NumTypes
    };

    /** Indirectly stolen from TrackVertexUtils::poca
     */
    template <typename float_v, typename mask_v>
    std::tuple<VecN<3, float_v>, mask_v> poca( StateVector4<float_v> const& s0,
                                               StateVector4<float_v> const& s1 ) const {
      // STEP 1: Define an initial position with the first two states
      // define d^2 = ( x0 + mu0*tx0 - x1 - mu1*tx1)^2 +  ( y0 + mu0*ty0 - x1 - mu1*ty1)^2 + (z0 + mu0 - z1 - mu1)^2
      // compute (half) 2nd derivative to mu0 and mu1 in point mu0=mu1=0
      auto const second00 = s0.tx() * s0.tx() + s0.ty() * s0.ty() + 1.f;
      auto const second11 = s1.tx() * s1.tx() + s1.ty() * s1.ty() + 1.f;
      auto const second01 = -s0.tx() * s1.tx() - s0.ty() * s1.ty() - 1.f;
      // compute inverse matrix, but stop if determinant not positive
      auto const det = second00 * second11 - second01 * second01;
      // OL: NN wrote 'abs( det )' but the comment above suggests it should be 'det'
      // Either way, calculate a mask of "valid" entries
      auto const mask = abs( det ) > 0.f;

      auto const secondinv00 = second11 / det;
      auto const secondinv11 = second00 / det;
      auto const secondinv01 = -second01 / det;

      // compute (half) first derivative
      auto first0 = s0.tx() * ( s0.x() - s1.x() ) + s0.ty() * ( s0.y() - s1.y() ) + ( s0.z() - s1.z() );
      auto first1 = -s1.tx() * ( s0.x() - s1.x() ) - s1.ty() * ( s0.y() - s1.y() ) - ( s0.z() - s1.z() );

      // compute mu0 and mu1 with delta-mu = - secondderivative^-1 * firstderivative
      auto const mu0 = -( secondinv00 * first0 + secondinv01 * first1 );
      auto const mu1 = -( secondinv11 * first1 + secondinv01 * first0 );

      // the initial point, this one will be updated in the fit loop
      return {{{0.5f * ( s0.x() + mu0 * s0.tx() + s1.x() + mu1 * s1.tx() ),
                0.5f * ( s0.y() + mu0 * s0.ty() + s1.y() + mu1 * s1.ty() ), 0.5f * ( s0.z() + mu0 + s1.z() + mu1 )}},
              mask};
    }

    GaudiException exception( std::string_view message ) const {
      // TODO also output to fatal() here to make it easier to find the errors
      return {std::string{message}, "Sel::Fitters::ParticleVertex", StatusCode::FAILURE};
    }

    // Fast utility function for inverting a 2x2 matrix. This just comes
    // from ROOT/Math/Dinv, but I took out the test on the determinant
    // since we do not need it.
    template <typename float_v>
    static SymNxN<2, float_v> Invert2x2( SymNxN<2, float_v> const& rhs ) {
      SymNxN<2, float_v> ret;
      auto const         det    = rhs( 0, 0 ) * rhs( 1, 1 ) - rhs( 0, 1 ) * rhs( 0, 1 );
      auto const         invdet = 1.f / det;
      // if (det == T(0.)) { return false; }
      ret( 0, 0 ) = rhs( 1, 1 ) * invdet;
      ret( 1, 1 ) = rhs( 0, 0 ) * invdet;
      ret( 0, 1 ) = -1.f * rhs( 0, 1 ) * invdet;
      return ret;
    }

    // Class for daughters with a straight-line trajectory
    template <typename particle_t, bool represents_composite>
    struct VertexTraj final {
      using simd_t  = typename particle_t::simd_t;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;
      using state_t = std::conditional_t<represents_composite, State4<float_v>, MiniState<float_v>>;

    private:
      particle_t            m_particle; // TODO maybe just cache some information from this?
      state_t               m_state;    // TODO evaluate what is really needed
      VecN<2, float_v>      m_q;        // predicted/fitted slope (tx,ty)
      SymNxN<2, float_v>    m_G;        // weight matrix of (x,y) of state
      Matrix<2, 3, float_v> m_A;        // projection matrix for vertex position

    public:
      VertexTraj( particle_t p, state_t state )
          : m_particle{std::move( p )}, m_state{std::move( state )}, m_q{m_state.tx(), m_state.ty()} {}

      void project( VecN<3, float_v> const& vertexpos, VecN<3, float_v>& halfDChisqDX,
                    SymNxN<3, float_v>& halfD2ChisqDX2, float_v& chi2, int_v& ndof ) {
        // move the state (not sure we should do it this way: maybe better to just cache the z.)
        m_state.linearTransportTo( vertexpos( 2 ) );

        // compute the weight matrix
        // TODO Just use invChol?
        m_G = Invert2x2( m_state.covXX() );

        // compute residual
        VecN<2, float_v> residual{vertexpos( 0 ) - m_state.x(), vertexpos( 1 ) - m_state.y()};

        // fill the projection matrix: use the fitted momentum!
        m_A( 0, 0 ) = m_A( 1, 1 ) = 1.f;
        m_A( 0, 2 )               = -1.f * m_q( 0 );
        m_A( 1, 2 )               = -1.f * m_q( 1 );

        // I tried to make this faster by writing it out, but H does
        // not contain sufficiently many zeroes. Better to
        // parallelise.

        // I am not sure that the compiler realizes that A^T*G is used
        // more than once here, so I'll substitute the code from
        // similarity. Note that similarity does not return an expression.
        // halfD2ChisqDX2 += ROOT::Math::Similarity( ROOT::Math::Transpose(m_A), m_G ) ;
        // halfDChisqDX   += ( ROOT::Math::Transpose(m_A) * m_G) * res ;
        auto const ATG = m_A.transpose() * m_G.cast_to_mat();
        // updating relevant fit results
        halfD2ChisqDX2 = halfD2ChisqDX2 + ( ATG * m_A ).cast_to_sym();
        halfDChisqDX   = halfDChisqDX + ATG * residual;
        chi2           = chi2 + LHCb::LinAlg::similarity( residual, m_G );
        ndof           = ndof + 2;
      }

      void updateSlopes( VecN<3, float_v> const& vertexpos ) {
        // first update the residual. (note the subtle difference with that in project!)
        float_v const          dz = vertexpos( 2 ) - m_state.z();
        VecN<2, float_v> const residual{vertexpos( 0 ) - ( m_state.x() + m_state.tx() * dz ),
                                        vertexpos( 1 ) - ( m_state.y() + m_state.ty() * dz )};

        // get the matrix that is the correlation of (x,y) and (tx,ty,qop)
        // ROOT::Math::SMatrix<double,2,2> Vba = m_state.covariance().template
        // Sub<ROOT::Math::SMatrix<double,2,2>>(2,0); compute the corresponding gain matrix (this is WBG in the BFR fit,
        // but here it is Vba * Vaa^-1) ROOT::Math::SMatrix<double,2,2> K = Vba*m_G ; compute the momentum vector
        m_q =
            VecN<2, float_v>{{m_state.tx(), m_state.ty()}} + m_state.covXT().transpose() * m_G.cast_to_mat() * residual;
      }

      void addToFourVector( VecN<3, float_v> const& vertexpos, VecN<4, float_v>& p4, SymNxN<4, float_v>& p4cov,
                            Matrix<4, 3, float_v>& gainmatrix ) const {
        // Despatch is a bit crude, but it'll do...in the original
        // ParticleVertexFitter this was done with a full specialisation, but
        // now it would have to be a partial specialisation and it all gets a
        // bit messy.
        if constexpr ( represents_composite ) {
          addToFourVector_Composite( vertexpos, p4, p4cov, gainmatrix );
        } else {
          addToFourVector_Track( vertexpos, p4, p4cov, gainmatrix );
        }
      }

      /** @todo FIXME implement logic for bremsstrahlung photons.
       */
      void addToFourVector_Track( VecN<3, float_v> const& vertexpos, VecN<4, float_v>& p4, SymNxN<4, float_v>& p4cov,
                                  Matrix<4, 3, float_v>& gain_matrix ) const {
        // This specialisation is for TrackWithVelo and TrackWithoutVelo
        // we first need to update q/p. since we also need the full gain matrix, we could as well redo that part.
        float_v const          dz = vertexpos( 2 ) - m_state.z();
        VecN<2, float_v> const residual{vertexpos( 0 ) - ( m_state.x() + m_q( 0 ) * dz ),
                                        vertexpos( 1 ) - ( m_state.y() + m_q( 1 ) * dz )};

        auto const Vba    = m_state.cov.template sub<Matrix<3, 2, float_v>, 2, 0>();
        auto       K      = Vba * m_G.cast_to_mat();
        auto       tx     = m_state.tx() + ( K * residual )( 0 );
        auto       ty     = m_state.ty() + ( K * residual )( 1 );
        auto       qOverP = m_state.qOverP() + ( K * residual )( 2 );
        // update 4-momentum
        // TODO can we just store this value in the constructor? what else is needed? bremsstrahlung info?
        auto const mass = m_particle.mass();
        auto const p    = 1.f / abs( qOverP );
        auto const n2   = 1.f + tx * tx + ty * ty;
        auto const n    = sqrt( n2 );
        auto const pz   = p / n;
        auto const px   = tx * pz;
        auto const py   = ty * pz;
        auto const E    = sqrt( p * p + mass * mass );
        p4              = p4 + VecN<4, float_v>{{px, py, pz, E}}; // TODO check whether this gives right results
                                                                  // it was E px py pz before

        Matrix<4, 3, float_v> dp4dmom{}; // jacobi matrix of del(p4)/del(tx,ty,qOverP)
        auto const            n3    = n2 * n;
        auto const            invn3 = 1.f / n3;
        dp4dmom( 0, 0 )             = p * ( 1.f + ty * ty ) * invn3; // dpx/dtx
        dp4dmom( 0, 1 )             = p * tx * -ty * invn3;          // dpx/dty
        dp4dmom( 0, 2 )             = -px / qOverP;                  // dpx/dqop

        dp4dmom( 1, 0 ) = p * ty * -tx * invn3;        // dpy/dtx
        dp4dmom( 1, 1 ) = p * ( 1 + tx * tx ) * invn3; // dpy/dty
        dp4dmom( 1, 2 ) = -py / qOverP;                // dpy/dqop

        dp4dmom( 2, 0 ) = pz * -tx / n2; // dpz/dtx
        dp4dmom( 2, 1 ) = pz * -ty / n2; // dpz/dtx
        dp4dmom( 2, 2 ) = -pz / qOverP;  // dpz/dqop

        dp4dmom( 3, 0 ) = 0.0;                 // dE/dtx
        dp4dmom( 3, 1 ) = 0.0;                 // dE/dty
        dp4dmom( 3, 2 ) = p / E * -p / qOverP; // dE/dqop

        auto FK = dp4dmom * K;

        p4cov = p4cov + LHCb::LinAlg::similarity( dp4dmom, m_state.cov.template sub<SymNxN<3, float_v>, 2, 2>() ) -
                LHCb::LinAlg::similarity( FK, m_state.cov.template sub<SymNxN<2, float_v>, 0, 0>() );

        gain_matrix = gain_matrix + FK * m_A;
        // // For brem-recovered electrons, we need to do something
        // // special. So, electrons are an ugly exception here. We could
        // // also add to q/p instead, which is cheaper, but perhaps even
        // // more ugly.
        // if ( particle().particleID().abspid() == 11 ) {
        //   const double absqop               = std::abs( m_state.qOverP() );
        //   const double momentumFromParticle = particle().momentum().P();
        //   // is 1% a reasonable threshold?
        //   if ( momentumFromParticle * absqop > 1.01 ) {
        //     const double momentumFromTrack       = 1 / absqop;
        //     const double momentumError2FromTrack = m_state.covariance()( 4, 4 ) * std::pow(
        //     momentumFromTrack, 4 ); const double momentumError2FromParticle =
        //         particle().momCovMatrix()( 3, 3 ) * std::pow( particle().momentum().E() /
        //         momentumFromParticle, 2 );
        //     const double bremEnergyCov = momentumError2FromParticle - momentumError2FromTrack;
        //     const double bremEnergy    = momentumFromParticle - momentumFromTrack;
        //     // if the correction is unphysical, ignore it.
        //     if ( bremEnergyCov > 0 ) {
        //       const auto tx = q( 0 );
        //       const auto ty = q( 1 );
        //       auto       t  = std::sqrt( 1 + tx * tx + ty * ty );
        //       // we could also 'scale' the momentum, but we anyway need the components for the
        //       jacobian Gaudi::LorentzVector p4brem{bremEnergy * tx / t, bremEnergy * ty / t,
        //       bremEnergy / t, bremEnergy}; p4 += p4brem; ROOT::Math::SMatrix<double, 4, 1> J;
        //       J( 0, 0 ) = tx / t;
        //       J( 1, 0 ) = ty / t;
        //       J( 2, 0 ) = 1 / t;
        //       J( 3, 0 ) = 1;
        //       p4cov += ROOT::Math::Similarity( J, Gaudi::SymMatrix1x1{bremEnergyCov} );
        //     }
        //   }
        // }
      }

      void addToFourVector_Composite( VecN<3, float_v> const& vertexpos, VecN<4, float_v>& p4,
                                      SymNxN<4, float_v>& p4cov, Matrix<4, 3, float_v>& gainmatrix ) const {
        // first need to 'update' the momentum vector. but for that we
        // first need to 'transport'.
        VecN<4, float_v> mom = m_particle.momentum(); // 4-momentum
        float_v const    pz  = Sel::get::pz( mom );
        float_v const    tx  = m_q( 0 );
        float_v const    ty  = m_q( 1 );

        // first update the residual
        float_v const               dz = Sel::get::z( vertexpos ) - Sel::get::z( m_particle );
        VecN<2, float_v>            res{Sel::get::x( vertexpos ) - ( Sel::get::x( m_particle ) + tx * dz ),
                             Sel::get::y( vertexpos ) - ( Sel::get::y( m_particle ) + ty * dz )};
        auto const&                 R         = m_state.covXX();
        auto const&                 Rinv      = m_G;
        SymNxN<4, float_v> const    momCov    = m_particle.momCovMatrix();
        Matrix<4, 3, float_v> const momPosCov = m_particle.momPosCovMatrix();

        // To do this right we need THREE projection matrices for the residual:
        //    r = A*x_vertex + B*mom_dau + C*x_dau
        // Note that C=-A.
        // Lets call the matrix F^T = (B^T C^T). then the uncertainty on the residual is
        //    R = F V77 F^T where V77 is the 7x7 covariance matrix of the daughter
        // We will need R every iteration
        // The correlation matrix between residual and the 7 parameters of the daughter is
        //    Vr_xmom = V77 F^T
        // Consequently, the gain matrix for the momentum-decayvertex is
        //    K72 = V77 F^T R^-1
        // Now, we do not need the updated parameters for the decay
        // vertex. If we just need the momentum part, then this reduces to
        //    K42 = ( V43 * C^T + V44 * B^T ) R^-1

        // The easiest implementation is this ...
        //  Gaudi::Matrix3x2 CT{ ROOT::Math::Transpose(m_A) } ;
        //  ROOT::Math::SMatrix<double,4,2> BT ;
        //  BT(0,0) = +dz/pz;
        //  BT(2,0) = -dz/pz*tx ;
        //  BT(1,1) = +dz/pz ;
        //  BT(2,1) = -dz/pz*ty ;
        //  ROOT::Math::SMatrix<double,4,2> K42 = ( momPosCov * CT + momCov * BT ) * Rinv ;
        // but since B and C are largely empty, we better write it out:
        float_v const         dzopz = dz / pz;
        Matrix<4, 2, float_v> K42part; // uninitialised here
        for ( int i = 0; i < 4; ++i ) {
          K42part( i, 0 ) =
              momPosCov( i, 0 ) - momPosCov( i, 2 ) * tx + momCov( i, 0 ) * dzopz - momCov( i, 2 ) * ( dzopz * tx );
          K42part( i, 1 ) =
              momPosCov( i, 1 ) - momPosCov( i, 2 ) * ty + momCov( i, 1 ) * dzopz - momCov( i, 2 ) * ( dzopz * ty );
        }
        Matrix<4, 2, float_v> const K42 = K42part * Rinv.cast_to_mat();

        // update the fourvector
        auto const deltap4 = K42 * res;
        p4                 = p4 + mom + deltap4;
        // to understand this, compare to what is done for the track
        p4cov      = p4cov + momCov - LHCb::LinAlg::similarity( K42, R );
        gainmatrix = gainmatrix + K42 * m_A;
      }
    };

    /** Helper type for identifying and handling TrackWithVelo and
     *  TrackWithoutVelo children.
     *  @todo Handle TrackWithoutVelo.
     */
    template <typename child_t>
    struct TrackChildHelper {
      /** Determine if the given `child_t` type represents a [chunk of] tracks.
       *  @todo Make this more sophisticated and follow changes in the event
       *        model.
       */
      constexpr static bool value = Sel::Utils::has_tracklike_API<child_t>;

      /** Given that `value` was `true`, derive (at runtime) the `ChildType` of
       *  `child_t`. This basically means "is it a long or downstream track".
       *  @todo Implement check and maybe return `ChildType::TrackWithoutVelo`.
       *  @todo Clearly state what is supported when `child` is a vector type.
       *        For example, if it is a proxy into a container that supports
       *        heterogeneous row types (e.g. some rows are long, some rows are
       *        downstream) then there is not a single correct return value we
       *        can give here. Is that a runtime error? Should the return type
       *        be changed so that can be supported?
       */
      static ChildType deriveType( LHCb::IParticlePropertySvc const&, [[maybe_unused]] child_t const& child ) {
        return ChildType::TrackWithVelo;
      }

      /** Given that `value` was `true`, and that `type` was returned by our
       *  `deriveType` function, return the narrowest-possible std::variant of
       *  types implementing the API used in the vertex fit. For tracks there
       *  should just be one return type (i.e. a variant of size 1), but there
       *  will be some runtime branching on the `type` to figure out how to get
       *  the relevant state.
       *  @todo Use `vertex_position` and its validity mask
       *        `vertex_position_mask` to determine if a sophisticated
       *        extrapolation is needed.
       *  @todo Implement this logic, don't just blindly call the
       *        `state(StateLocation::ClosestToBeam)` accessor. Remove [[maybe_unused]].
       */
      template <typename mask_v, typename float_v>
      static auto prepareChild( [[maybe_unused]] VecN<3, float_v> const& vertex_position,
                                [[maybe_unused]] mask_v const& vertex_position_mask, [[maybe_unused]] ChildType type,
                                child_t const& child ) {
        assert( type == ChildType::TrackWithVelo || type == ChildType::TrackWithoutVelo );
        using VertexTrack =
            VertexTraj<child_t, false /* flag this represents a track not a weakly-decaying composite */>;
        return VertexTrack{child, child.state( StateLocation::ClosestToBeam )};
      }
    };

    /** Helper type for identifying and handling Composite and Resonance
     *  children.
     *  @todo Handle Resonance, adding a runtime check on PID and another type
     *        to go in the variant returned by prepareChild.
     */
    template <typename child_t>
    struct CompositeChildHelper {
      /** Determine if the given `child_t` represents a [chunk of] composite
       *  particles.
       *  @todo This list looks a bit redundant. And it would be better to use
       *        e.g. Sel::get::x( particle ) in place of particle.x(), and to
       *        use here something that checks whether Sel::get::x( particle )
       *        is well-formed, rather than particle.x().
       */
      constexpr static bool value =
          Sel::type_traits::has_x_v<child_t> && Sel::type_traits::has_y_v<child_t> &&
          Sel::type_traits::has_z_v<child_t> && Sel::type_traits::has_px_v<child_t> &&
          Sel::type_traits::has_py_v<child_t> && Sel::type_traits::has_pz_v<child_t> &&
          Sel::type_traits::has_momentum_v<child_t> && Sel::type_traits::has_momCovMatrix_v<child_t> &&
          Sel::type_traits::has_momPosCovMatrix_v<child_t> && Sel::type_traits::has_posCovMatrix_v<child_t> &&
          Sel::type_traits::has_threeMomCovMatrix_v<child_t> && Sel::type_traits::has_threeMomPosCovMatrix_v<child_t>;

      /** Given that `value` was `true`, derive (at runtime) the `ChildType` of
       *  `child_t`. This basically means "is it a weakly decaying particle".
       *  @todo Implement check and maybe return `ChildType::Resonance`.
       *  @todo Can use `pp_svc` to get the "ctau" of the given `child`. Remove
       *        [[maybe_unused]] once we do that.
       */
      static ChildType deriveType( [[maybe_unused]] LHCb::IParticlePropertySvc const& pp_svc,
                                   [[maybe_unused]] child_t const&                    child ) {
        return ChildType::Composite;
      }
      /** Given that `value` was `true`, and that `type` was returned by our
       *  `deriveType` function, return the narrowest-possible std::variant of
       *  types implementing the API used in the vertex fit. Because weakly
       *  decaying particles are handled differently in the fit, the return
       *  type here will have to be a variant of two different types.
       *
       *  The original ParticleVertexFitter didn't use the estimated vertex
       *  position for `Resonance` and `Composite` children, so we don't
       *  either.
       *  @todo Add logic for the `ChildType::Resonance` value of `type` and
       *        add another member to the return type; remove [[maybe_unused]]
       */
      template <typename mask_v, typename float_v>
      static auto prepareChild( VecN<3, float_v> const&, mask_v const&, [[maybe_unused]] ChildType type,
                                child_t const& child ) {
        assert( type == ChildType::Composite || type == ChildType::Resonance );
        using VertexComposite =
            VertexTraj<child_t, true /* flag this represents a weakly-decaying composite and not a track */>;
        return VertexComposite{child, stateVectorFromComposite( child )};
      }
    };

    /** Helper type for the default `Other` case.
     */
    template <typename child_t>
    struct LeftoverHelper {
      /** This helper just tries generic logic, it should be tried last and it
       *  accepts everything. If it doesn't work, a compile error is the
       *  correct outcome.
       */
      constexpr static bool value = true;
      /** Catch-all, don't expect other logic here.
       */
      static ChildType deriveType( LHCb::IParticlePropertySvc const&, child_t const& ) { return ChildType::Other; }
      /** @todo Need to implement the helper class that handles generic
       *        children and return an instance of it. As a 1-member variant.
       */
      template <typename mask_v, typename float_v>
      static auto prepareChild( [[maybe_unused]] VecN<3, float_v> const& vertex_position,
                                [[maybe_unused]] mask_v const& vertex_position_mask, [[maybe_unused]] ChildType type,
                                [[maybe_unused]] child_t const& child ) {
        assert( type == ChildType::Other );
        return bool{};
      }
    };

    /** Master list of helper types. The order matters; the **first** matching
     *  helper is preferred.
     */
    template <typename child_t>
    using all_helper_types =
        boost::mp11::mp_list<TrackChildHelper<child_t>, CompositeChildHelper<child_t>, LeftoverHelper<child_t>>;

    /** Helper types that match (::value == true) for a given child type.
     */
    template <typename child_t>
    using matching_helper_types = boost::mp11::mp_copy_if<all_helper_types<child_t>, boost::mp11::mp_to_bool>;

    /** The first matching helper type for a given child type.
     */
    template <typename child_t>
    using first_matching_helper_type = boost::mp11::mp_front<matching_helper_types<child_t>>;

    /** Take a [SIMD vector worth of] particles and assign a single "type"
     *  value that will steer the fit algorithm below. It is an error
     *  (unfortunately only a runtime one...) to give children that are of
     *  heterogeneous `ChildType`, but it should be supported that there is
     *  a mix of actual PDG IDs (e.g. `child` could be a mix of D0 and D~0, but
     *  there should be a single return type (ChildType::Composite, in that
     *  case).
     */
    template <typename child_t>
    ChildType deriveType( child_t const& child ) const {
      assert( m_pp_svc );
      using helper_t = first_matching_helper_type<child_t>;
      return helper_t::deriveType( *m_pp_svc, child );
    }

    /** Helper for getting the return type of the prepareChild method of a
     *  helper type.
     */
    template <typename mask_v, typename float_v>
    struct prepareChild_helper {
      template <typename helper_t, typename variant_member_t>
      using fn = decltype( helper_t::prepareChild( std::declval<VecN<3, float_v>>(), std::declval<mask_v>(),
                                                   std::declval<ChildType>(), std::declval<variant_member_t>() ) );
    };

    /** Choose the first match for the given child type in `all_helper_types`
     *  and call its `prepareChild` method. All the extra complexity is to cope
     *  with `child_t` being a variant type whose members might match different
     *  helpers in `all_helper_types` and have different return types.
     *
     *  @todo Include the `vertexinsidefoil` logic from ParticleVertexFitter.cpp
     */
    template <typename mask_v, typename float_v, typename child_t>
    auto prepareChild( VecN<3, float_v> const& vertex_position, mask_v const& vertex_position_mask, ChildType type,
                       child_t const& possibly_variant_child ) const {
      // child_t might be variant<Ts...>, or it might be a value type itself.
      // if the former, this yields tuple<Ts...>, otherwise tuple<child_t>
      using variant_types = typename Sel::Utils::is_variant<child_t>::tuple_type;
      // get the [first] matching helper for each element in `variant_types`
      using matched_helpers = boost::mp11::mp_transform<first_matching_helper_type, variant_types>;
      // get the return types of the `prepareChild` methods of all of these
      // helpers when they are invoked with the corresponding element of
      // `variant_types`. e.g. this might be tuple<variant<A, B>, variant<C>>
      using return_types =
          boost::mp11::mp_transform_q<prepareChild_helper<mask_v, float_v>, matched_helpers, variant_types>;
      // Turn this into a variant<...> type that can accommodate all relevant
      // types, in the example just above `ret_t` would be variant<A, B, C>
      using ret_t = boost::mp11::mp_rename<return_types, std::variant>;
      static_assert( boost::mp11::mp_is_set<ret_t>::value,
                     "Got duplicate entries in derived variant type. Looks like the code needs an mp_unique<...>." );
      // Now we know what return type to impose, invoke/visit on `child`
      return Sel::Utils::invoke_or_visit(
          [&]( auto const& child ) -> ret_t {
            using helper_t = first_matching_helper_type<std::decay_t<decltype( child )>>;
            return helper_t::prepareChild( vertex_position, vertex_position_mask, type, child );
          },
          possibly_variant_child );
    }

    /** Obtain an estimate of the vertex position for initialization:
     *  1. if there are Resonance daughters, take the vertex position of the first.
     *  2. if not, use the states of the first two TrackWithVelo or Composite
     *  3. if not, try with TrackWithoutVelo and trajpoca
     */
    template <typename simd_t, std::size_t N, typename Combination>
    std::tuple<VecN<3, typename simd_t::float_v>, typename simd_t::mask_v>
    calculateInitialPosition( std::array<ChildType, N> const& types, Combination const& children ) const {
      // For convenience
      using mask_v  = typename simd_t::mask_v;
      using float_v = typename simd_t::float_v;

      // Count how many children we got of each type.
      auto const toInt = []( ChildType t ) { return static_cast<std::underlying_type_t<ChildType>>( t ); };
      std::array<unsigned char, toInt( ChildType::NumTypes )> counttypes{};
      static_assert( N <= std::numeric_limits<unsigned char>::max() );
      LHCb::Utils::unwind<0, N>( [&]( auto i ) { ++counttypes[toInt( types[i] )]; } );

      // Helper to access the count of a given type
      auto const count = [&]( ChildType type ) { return counttypes[toInt( type )]; };

      // Branch off to the different ways we can calculate a seed vertex
      if ( count( ChildType::Resonance ) > 0 ) {
        // Case 1
        std::optional<std::tuple<VecN<3, float_v>, mask_v>> tmp{std::nullopt};
        LHCb::Utils::unwind<0, N>( [&]( auto i ) {
          // Find the first `Resonance` child
          if ( !tmp.has_value() && types[i] == ChildType::Resonance ) {
            // Use invoke_or_visit in case the relevant entry in `children` is
            // a variant type.
            Sel::Utils::invoke_or_visit(
                [&]( auto const& child ) {
                  // TODO iron out API consistency, in the old model this would
                  //      have been child->endVertex()->position()
                  if constexpr ( Sel::type_traits::has_endVertex_v<std::decay_t<decltype( child )>> ) {
                    tmp.emplace( child.endVertex(), mask_v{true} );
                  } else {
                    throw exception( "Resonance type didn't have an endVertex()" );
                  }
                },
                children.template get<i>() );
          }
        } );
        assert( tmp.has_value() );
        return *tmp; // no validity check, but we have the assert just above
      } else if ( count( ChildType::TrackWithVelo ) + count( ChildType::Composite ) >= 2 ) {
        // Case 2
        // Use the POCA of TrackWithVelo + closestToBeamState or Composite + position/momentum
        std::array<StateVector4<float_v>, 2> velotrajs;
        unsigned char                        nvelotrajs{};
        LHCb::Utils::unwind<0, N>( [&]( auto i ) {
          if ( nvelotrajs < velotrajs.size() ) {
            // Use invoke_or_visit in case the relevant entry in `children` is
            // a variant type.
            Sel::Utils::invoke_or_visit(
                [&]( auto const& child ) {
                  using child_t = std::decay_t<decltype( child )>;
                  if ( types[i] == ChildType::TrackWithVelo ) {
                    if constexpr ( Sel::Utils::has_tracklike_API<child_t> ) {
                      velotrajs[nvelotrajs++] = StateVector4<float_v>{child.state( StateLocation::ClosestToBeam )};
                    } else {
                      throw exception( "TrackWithVelo type didn't have a state(StateLocation::ClosestToBeam)" );
                    }
                  } else if ( types[i] == ChildType::Composite ) {
                    if constexpr ( Sel::type_traits::has_endVertex_v<child_t> &&
                                   Sel::type_traits::has_threeMomentum_v<child_t> ) {
                      velotrajs[nvelotrajs++] = StateVector4<float_v>{child.endVertex(), child.threeMomentum()};
                    } else {
                      throw exception( "Composite type didn't one/both of endVertex() and threeMomentum()" );
                    }
                  }
                },
                children.template get<i>() );
          }
        } );
        assert( nvelotrajs == velotrajs.size() );
        // Return the POCA of the two states
        return poca<float_v, mask_v>( velotrajs[0], velotrajs[1] );
      } else if ( count( ChildType::TrackWithVelo ) + count( ChildType::Composite ) +
                      count( ChildType::TrackWithoutVelo ) >=
                  2 ) {
        // Case 3 -- TODO, see implementation in ParticleVertexFitter.cpp
        // Wouldn't be **that** hard to generate a scalar loop that converts
        // to old-style types and uses a state provider...
        // OL wonders if there could be a faster/more approximate case
        //    covering the "everything is downstream" case before falling
        //    back to this.
        throw exception( "Sel::Fitters::ParticleVertex does not implement ParticleVertexFitter's third "
                         "method for finding a starting position." );
      } else {
        std::ostringstream oss;
        oss << "Could not initialise Sel::Fitters::ParticleVertex: #TrackWithVelo = "
            << count( ChildType::TrackWithVelo ) << ", #TrackWithoutVelo = " << count( ChildType::TrackWithoutVelo )
            << ", #Composite = " << count( ChildType::Composite ) << ", #Resonance = " << count( ChildType::Resonance )
            << ", #Other = " << count( ChildType::Other );
        throw exception( oss.str() );
      }
    }

    template <typename T>
    using get_simd_t = typename T::simd_t;

    /** @todo Consider whether to support the even more complicated case when
     *        the children are are vector-yielding proxies but not all elements
     *        of the vectors need to be treated the same way, e.g. {long,
     *        downstream, long, downstream}...(OL thinks this is not worth the
     *        effort -- the scalar backend could cover that case)
     *  @todo Check with Wouter about the disabled `vertexinsidefoil` case
     *  @todo Revisit the condition for fit "success"
     *  @todo Consider whether we should emplace to `storage` if all fits fail
     */
    template <typename mask_v, typename int_v, typename... child_ts, std::size_t... IChild>
    mask_v fitComposite( LHCb::v2::Composites& storage, mask_v const& mask, int_v const& new_particle_ids,
                         Sel::ParticleCombination<child_ts...> children, std::index_sequence<IChild...> ) const {
      // Some boilerplate/sanity checking
      using simd_ts = boost::mp11::mp_unique<boost::mp11::mp_transform<
          get_simd_t, boost::mp11::mp_append<typename Sel::Utils::is_variant<child_ts>::tuple_type...>>>;
      static_assert( boost::mp11::mp_size<simd_ts>::value == 1 );
      using simd_t  = boost::mp11::mp_front<simd_ts>;
      using float_v = typename simd_t::float_v;
      static_assert( sizeof...( child_ts ) == sizeof...( IChild ) );
      static_assert( std::is_same_v<int_v, typename simd_t::int_v> );
      static_assert( std::is_same_v<mask_v, typename simd_t::mask_v> );

      // Classify the children we received according to the way they should be
      // handled in the fit. There are five cases:
      // - TrackWithVelo: tracks with velo hits (long, upstream, velo-only)
      // - TrackWithoutVelo: tracks without velo hits (only downstream, hopefully)
      // - Resonance: composites with ctau < 1micron
      // - Composite: other composites
      // - Other: everything else, e.g. photons, pi0, jets
      // Not all of these classifications can be made at compile time based on
      // the template pack `child_ts...`. If the input children are themselves
      // variants then `deriveType` is used as a visitor, increasing the amount
      // of runtime dispatch.
      std::array const types{Sel::Utils::invoke_or_visit(
          [this]( auto const& child ) { return this->deriveType( child ); }, children.template get<IChild>() )...};

      // Obtain an estimate of the vertex position for initialization
      auto [vertex_position, vertex_position_mask] = calculateInitialPosition<simd_t>( types, children );

      // Now we have a preliminary position `vertex_position` and its validity
      // is given by `prelim_fit_mask`
      auto const prelim_fit_mask = mask && vertex_position_mask;

      // the last trick: as long as the current estimate tells us
      // that we are within the beampipe, we could as well just use
      // the first state.
      // TODO implement this, e.g. R < 8mm, and pass it to `prepareChild`
      [[maybe_unused]] bool const vertexinsidefoil{true};

      // Now estimate everything that goes into the vertex, this array will be
      // filled with variants; the different variant types encode the different
      // handling required for TrackWith[out]Velo/Composite/Resonance/Other.
      std::tuple extrapolated_children{
          prepareChild( vertex_position, prelim_fit_mask, types[IChild], children.template get<IChild>() )...};

      // These are required for populating child relations at the end, but the
      // API for dealing with them should be agnostic to the child types
      std::array const current_child_indices{Sel::Utils::invoke_or_visit(
          []( auto const& child ) { return int_v{child.indices()}; }, children.template get<IChild>() )...};
      std::array const current_zip_families{Sel::Utils::invoke_or_visit(
          []( auto const& child ) { return int_v{int{child.zipIdentifier()}}; }, children.template get<IChild>() )...};

      // Now run the vertex fit iterations
      // Reminder: `vertex_position` holds the initial estimate of the vertex
      //           position and its validity is given by `prelim_fit_mask`
      SymNxN<3, float_v>       poscov{};
      float_v                  chi2{};
      int_v                    ndof{};
      mask_v                   converged{!prelim_fit_mask}; // mark invalid entries as already converged
      constexpr unsigned short max_iter       = 5;          // TODO make configurable
      constexpr auto           max_delta_chi2 = 0.01f;      // TODO make configurable
      for ( unsigned short iter = 0; iter < max_iter && !all( converged ); ++iter ) {
        chi2                = 0.;
        ndof                = -3; // TODO do we really need to count this every time?
        auto halfD2ChisqDX2 = LHCb::LinAlg::initialize_with_zeros<SymNxN<3, float_v>>();
        auto halfDChisqDX   = LHCb::LinAlg::initialize_with_zeros<VecN<3, float_v>>();

        // add all particles that contribute to the vertex chi2
        ( std::visit( [&, &vertex_position = vertex_position](
                          auto& child ) { child.project( vertex_position, halfDChisqDX, halfD2ChisqDX2, chi2, ndof ); },
                      std::get<IChild>( extrapolated_children ) ),
          ... );

        // calculate the covariance and the change in the position
        poscov = halfD2ChisqDX2.invChol(); // FIXME what if this fails?

        auto delta_pos = poscov * halfDChisqDX * float_v{-1.f};
        // update the position
        vertex_position = vertex_position + delta_pos;

        // add to chi2
        auto delta_chi2 = delta_pos.dot( halfDChisqDX );
        chi2            = chi2 + delta_chi2;
        converged       = converged || ( ( -1.f * delta_chi2 ) < max_delta_chi2 );

        // update the slopes
        // TODO maybe this should be updated to mean precisely "are we looping
        //      again"? Right now then if we hit max_iters we might update the
        //      slopes but not the chi2
        if ( !all( converged ) ) {
          ( std::visit( [& vertex_position = vertex_position]( auto& child ) { child.updateSlopes( vertex_position ); },
                        std::get<IChild>( extrapolated_children ) ),
            ... );
        }
      }

      // Fit iteration completed
      // TODO we should consider what behaviour in the fit loop should
      //      consistute failure, requiring convergence is perhaps too much,
      //      this (which also discards nan chi2...) seems OK-ish (it discards
      //      chi2 of exactly zero). Other positive-definiteness checks could
      //      also be considered...
      auto const fit_mask = prelim_fit_mask && ( chi2 > 0.f );

      // compute the total fourvector and all other parts of the covariance matrix
      auto p4          = LHCb::LinAlg::initialize_with_zeros<VecN<4, float_v>>();
      auto p4cov       = LHCb::LinAlg::initialize_with_zeros<SymNxN<4, float_v>>();
      auto gain_matrix = LHCb::LinAlg::initialize_with_zeros<Matrix<4, 3, float_v>>();
      ( std::visit( [&, &vertex_position = vertex_position](
                        auto& child ) { child.addToFourVector( vertex_position, p4, p4cov, gain_matrix ); },
                    std::get<IChild>( extrapolated_children ) ),
        ... );
      p4cov                                 = p4cov + LHCb::LinAlg::similarity( gain_matrix, poscov );
      Matrix<4, 3, float_v> const momposcov = gain_matrix * poscov.cast_to_mat();
      // Actually fill the output storage
      storage.emplace_back( vertex_position, p4, new_particle_ids, chi2, ndof, poscov, p4cov, momposcov,
                            current_child_indices, current_zip_families );

      return fit_mask;
    }

    // Service used to figure out if a given PDG ID decays weakly or not
    ServiceHandle<LHCb::IParticlePropertySvc> m_pp_svc;
  };
} // namespace Sel::Fitters
